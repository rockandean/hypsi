// file: zonearray.cc

#include <iostream>
#include <cmath>

#include "mpi.h"

#include "exception.h"
#include "zonearray.h"
#include "tags.h"

namespace HYPSI {

using namespace std;
//<><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>

// E grid interpolation - with fatal if index error

void Zone::calculate_eweights( const double* xyzp, InterpWeights& w )
{
  int ix0, ix1, iy0, iy1, iz0, iz1;
  double wx0, wx1, wy0, wy1, wz0, wz1; //! position in the region, as cell offset.

  wx0 = ( xyzp[0] - region.x0 )/ dx + 0.5;
  ix0 = static_cast<int>( wx0 );
  ix1 = ix0 + 1;
  wx1 = wx0 - ix0;
  wx0 = 1.0 - wx1;
  wy0 = ( xyzp[1] - region.y0 )/ dy + 0.5;
  iy0 = static_cast<int>( wy0 );
  iy1 = iy0 + 1;
  wy1 = wy0 - iy0;
  wy0 = 1.0 - wy1;
  wz0 = ( xyzp[2] - region.z0 )/ dz + 0.5;
  iz0 = static_cast<int>( wz0 );
  iz1 = iz0 + 1;
  wz1 = wz0 - iz0;
  wz0 = 1.0 - wz1;

  w.ew000 = wx0 * wy0 * wz0;
  w.ew100 = wx1 * wy0 * wz0;
  w.ew010 = wx0 * wy1 * wz0;
  w.ew110 = wx1 * wy1 * wz0;
  w.ew001 = wx0 * wy0 * wz1;
  w.ew101 = wx1 * wy0 * wz1;
  w.ew011 = wx0 * wy1 * wz1;
  w.ew111 = wx1 * wy1 * wz1;

  w.ei000 = idx( ix0, iy0, iz0 );
  w.ei100 = w.ei000 + nyz2;
  w.ei010 = w.ei000 + nz2;
  w.ei110 = w.ei010 + nyz2;
  w.ei001 = w.ei000 + 1;
  w.ei101 = w.ei100 + 1;
  w.ei011 = w.ei010 + 1;
  w.ei111 = w.ei110 + 1;

  if(    w.ei000 < 0 || w.ei000 >= nxyz2 
     ||  w.ei100 < 0 || w.ei100 >= nxyz2
     ||  w.ei010 < 0 || w.ei010 >= nxyz2
     ||  w.ei110 < 0 || w.ei110 >= nxyz2
     ||  w.ei001 < 0 || w.ei001 >= nxyz2
     ||  w.ei101 < 0 || w.ei101 >= nxyz2
     ||  w.ei011 < 0 || w.ei011 >= nxyz2
     ||  w.ei111 < 0 || w.ei111 >= nxyz2 )
  {
    HypsiException e(
      "E Cell index out of bounds", "Zone::calculate_eweights" );
    stringstream s;
    s << "Cell: ix0, iy0, iz0: " << ix0 << ", " << iy0 << ", " << iz0 
      << " x,y,z: " << xyzp[0] << ", " << xyzp[1] << ", "<< xyzp[2];
    e.push_err_msg( s.str() );
 
    throw e;
  }

}  // end of Zone::calculate_eweights

// --------------------------------------------------------------------------

// E grid interpolation - with no checks for index error

void Zone::calculate_eweights_nocheck( const double* xyzp, InterpWeights& w )
{
  int ix0, ix1, iy0, iy1, iz0, iz1;
  double wx0, wx1, wy0, wy1, wz0, wz1;

  wx0 = ( xyzp[0] - region.x0 )/ dx + 0.5;
  ix0 = static_cast<int>( wx0 );
  ix1 = ix0 + 1;
  wx1 = wx0 - ix0;
  wx0 = 1.0 - wx1;
  wy0 = ( xyzp[1] - region.y0 )/ dy + 0.5;
  iy0 = static_cast<int>( wy0 );
  iy1 = iy0 + 1;
  wy1 = wy0 - iy0;
  wy0 = 1.0 - wy1;
  wz0 = ( xyzp[2] - region.z0 )/ dz + 0.5;
  iz0 = static_cast<int>( wz0 );
  iz1 = iz0 + 1;
  wz1 = wz0 - iz0;
  wz0 = 1.0 - wz1;

  w.ew000 = wx0 * wy0 * wz0;
  w.ew100 = wx1 * wy0 * wz0;
  w.ew010 = wx0 * wy1 * wz0;
  w.ew110 = wx1 * wy1 * wz0;
  w.ew001 = wx0 * wy0 * wz1;
  w.ew101 = wx1 * wy0 * wz1;
  w.ew011 = wx0 * wy1 * wz1;
  w.ew111 = wx1 * wy1 * wz1;

  w.ei000 = idx( ix0, iy0, iz0 );
  w.ei100 = w.ei000 + nyz2;
  w.ei010 = w.ei000 + nz2;
  w.ei110 = w.ei010 + nyz2;
  w.ei001 = w.ei000 + 1;
  w.ei101 = w.ei100 + 1;
  w.ei011 = w.ei010 + 1;
  w.ei111 = w.ei110 + 1;

  w.check = false;

}  // end of Zone::calculate_eweights_nocheck

// --------------------------------------------------------------------------

// E grid interpolation - with no checks for index error

void Zone::calculate_eweights_check( const double* xyzp, InterpWeights& w )
{
  int ix0, ix1, iy0, iy1, iz0, iz1;
  double wx0, wx1, wy0, wy1, wz0, wz1;

  wx0 = ( xyzp[0] - region.x0 )/ dx + 0.5;
  ix0 = static_cast<int>( wx0 );
  ix1 = ix0 + 1;
  wx1 = wx0 - ix0;
  wx0 = 1.0 - wx1;
  wy0 = ( xyzp[1] - region.y0 )/ dy + 0.5;
  iy0 = static_cast<int>( wy0 );
  iy1 = iy0 + 1;
  wy1 = wy0 - iy0;
  wy0 = 1.0 - wy1;
  wz0 = ( xyzp[2] - region.z0 )/ dz + 0.5;
  iz0 = static_cast<int>( wz0 );
  iz1 = iz0 + 1;
  wz1 = wz0 - iz0;
  wz0 = 1.0 - wz1;

  w.ew000 = wx0 * wy0 * wz0;
  w.ew100 = wx1 * wy0 * wz0;
  w.ew010 = wx0 * wy1 * wz0;
  w.ew110 = wx1 * wy1 * wz0;
  w.ew001 = wx0 * wy0 * wz1;
  w.ew101 = wx1 * wy0 * wz1;
  w.ew011 = wx0 * wy1 * wz1;
  w.ew111 = wx1 * wy1 * wz1;

  w.ei000 = idx( ix0, iy0, iz0 );
  w.ei100 = w.ei000 + nyz2;
  w.ei010 = w.ei000 + nz2;
  w.ei110 = w.ei010 + nyz2;
  w.ei001 = w.ei000 + 1;
  w.ei101 = w.ei100 + 1;
  w.ei011 = w.ei010 + 1;
  w.ei111 = w.ei110 + 1;

  w.check = true;
  
  w.valid[0] = w.ei000 >= 0 || w.ei000 < nxyz2;
  w.valid[1] = w.ei001 >= 0 || w.ei001 < nxyz2;
  w.valid[2] = w.ei010 >= 0 || w.ei010 < nxyz2;
  w.valid[3] = w.ei011 >= 0 || w.ei011 < nxyz2;
  w.valid[4] = w.ei100 >= 0 || w.ei100 < nxyz2;
  w.valid[5] = w.ei101 >= 0 || w.ei101 < nxyz2;
  w.valid[6] = w.ei110 >= 0 || w.ei110 < nxyz2;
  w.valid[7] = w.ei111 >= 0 || w.ei111 < nxyz2; 

}  // end of Zone::calculate_eweights



// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

ScalarGcellBuffer::ScalarGcellBuffer( const Zone& zone )
{
  initialize( zone );
}

void ScalarGcellBuffer::initialize( const Zone& zone )
{

  nx = zone.nx; nx1 = nx+1; nx2 = nx+2;
  ny = zone.ny; ny1 = ny+1; ny2 = ny+2;
  nz = zone.nz; nz1 = nz+1; nz2 = nz+2;
  nyz2 = ny2*nz2; nxyz2 = nx2 * nyz2;
  for(int i=0;i<27;i++)
  { znneighbours[i] = zone.znneighbours[i];
    isend_complete_arr[i] = true;
    irecv_complete_arr[i] = false;
  }

// allocate ghost cells buffer space

  int buffsiz = 8 + 4*( nx + ny + nz ) + 2*( nx*ny + nx*nz + ny*nz );
  buffp = new double[ buffsiz ];

//cout<<"Z"<<zone.izone<<": ScalarGcellBuffer::initialize buffp:"<<buffp<<"\n";
  
  buffers[0] = buffp;                       // corner 000
  buffssiz[0] = 1;
  buffers[1] = buffers[0] + buffssiz[0];    // edge 00z
  buffssiz[1] = nz;
  buffers[2] = buffers[1] + buffssiz[1];    // corner 001
  buffssiz[2] = 1;
  buffers[3] = buffers[2] + buffssiz[2];    // edge 0y0
  buffssiz[3] = ny;
  buffers[4] = buffers[3] + buffssiz[3];    // face 0yz
  buffssiz[4] = ny*nz;
  buffers[5] = buffers[4] + buffssiz[4];    // edge 0y1
  buffssiz[5] = ny;
  buffers[6] = buffers[5] + buffssiz[5];    // corner 010
  buffssiz[6] = 1;
  buffers[7] = buffers[6] + buffssiz[6];    // edge 01z
  buffssiz[7] = nz;
  buffers[8] = buffers[7] + buffssiz[7];    // corner 011
  buffssiz[8] = 1;

  buffers[9] = buffers[8] + buffssiz[8];    // edge x00
  buffssiz[9] = nx;
  buffers[10] = buffers[9] + buffssiz[9];   // face x0z
  buffssiz[10] = nx*nz;
  buffers[11] = buffers[10] + buffssiz[10];  // edge x01
  buffssiz[11] = nx;
  buffers[12] = buffers[11] + buffssiz[11];  // face xy0
  buffssiz[12] = nx*ny;
  buffers[13] = 0;          // cube xyz  (SIZE ZERO!)
  buffssiz[13] = 0;
  buffers[14] = buffers[12] + buffssiz[12];  // face xy1
  buffssiz[14] = nx*ny;
  buffers[15] = buffers[14] + buffssiz[14];  // edge x10
  buffssiz[15] = nx;
  buffers[16] = buffers[15] + buffssiz[15];  // face x1z
  buffssiz[16] = nx*nz;
  buffers[17] = buffers[16] + buffssiz[16];  // edge x11
  buffssiz[17] = nx;

  buffers[18] = buffers[17] + buffssiz[17];  // corner 100
  buffssiz[18] = 1;
  buffers[19] = buffers[18] + buffssiz[18];  // edge 10z
  buffssiz[19] = nz;
  buffers[20] = buffers[19] + buffssiz[19];  // corner 101
  buffssiz[20] = 1;
  buffers[21] = buffers[20] + buffssiz[20];  // edge 1y0
  buffssiz[21] = ny;
  buffers[22] = buffers[21] + buffssiz[21];  // face 1yz
  buffssiz[22] = ny*nz;
  buffers[23] = buffers[22] + buffssiz[22];  // edge 1y1
  buffssiz[23] = ny;
  buffers[24] = buffers[23] + buffssiz[23];  // corner 110
  buffssiz[24] = 1;
  buffers[25] = buffers[24] + buffssiz[24];  // edge 11z
  buffssiz[25] = nz;
  buffers[26] = buffers[25] + buffssiz[25];  // corner 111
  buffssiz[26] = 1;
}

// <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>

void ScalarGcellBuffer::isend_to_neighbours( void )
{

  if( buffp == 0 )
    throw HypsiException(
      "Buffer pointer zero", "ScalarGcellBuffer::isend_to_neighbours");

  int mpierr;
  for( int i=0; i<27; i++ )
  {
    isend_complete_arr[i] = false;
    if( i == 13 )
    {
      isend_complete_arr[i] = true;
      continue;
    }
    mpierr = MPI_Isend( buffers[i], buffssiz[i], MPI_DOUBLE,
             znneighbours[i],
             TAG_NNEIGHBOURS_SEND+i,
             MPI_COMM_WORLD,
             &send_requests[i] );       
    if( mpierr != MPI_SUCCESS )
      throw HypsiMPIException(
      "MPI_Isend failed", "ScalarGcellBuffer::isend_to_neighbours", mpierr);
  }
}

// <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>

bool ScalarGcellBuffer::isend_complete( void )
{
  int mpi_test_flag;
  MPI_Status mpi_test_status;
  bool all_complete = true;
  
  for( int i=0; i<27; i++ )
  {
    if( !isend_complete_arr[i] )
    {
      MPI_Test( &send_requests[i], &mpi_test_flag, &mpi_test_status );
      isend_complete_arr[i] = mpi_test_flag;
      if( all_complete && !isend_complete_arr[i] )
        all_complete = false;
    }
  }
  return all_complete;
}

// <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>

void ScalarGcellBuffer::irecv_from_neighbours( void )
{
  if( buffp == 0 )
    throw HypsiException(
      "Buffer pointer zero", "ScalarGcellBuffer::irecv_from_neighbours");

  int mpierr;
  for( int i=0; i<27; i++ )
  {
    irecv_complete_arr[i] = false;
    if( i == 13 )
    {
      irecv_complete_arr[i] = true;
      continue;
    }
    mpierr = MPI_Irecv( buffers[i], buffssiz[i], MPI_DOUBLE,
             znneighbours[i],
             TAG_NNEIGHBOURS_SEND + 26 - i,
             MPI_COMM_WORLD,
             &recv_requests[i] );       
    if( mpierr != MPI_SUCCESS )
      throw HypsiMPIException(
      "MPI_Irecv failed", "ScalarGcellBuffer::irecv_from_neighbours", mpierr);
  }
}

// <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>

bool ScalarGcellBuffer::irecv_complete( void )
{
  int mpi_test_flag;
  MPI_Status mpi_test_status;
  bool all_complete = true;
  
  for( int i=0; i<27; i++ )
  {
    if( !irecv_complete_arr[i] )
    {
      MPI_Test( &recv_requests[i], &mpi_test_flag, &mpi_test_status );
      irecv_complete_arr[i] = mpi_test_flag;
      if( all_complete && !irecv_complete_arr[i] )
        all_complete = false;
    }
  }
  return all_complete;
}

// <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>
/* \brief copy from interior edges/faces to Gcell buffers

*/
void ScalarGcellBuffer::copy_in_from_array( const ScalarZarray& a )
{
  if( buffp == 0 )
    throw HypsiException(
      "Buffer pointer zero", "ScalarGcellBuffer::copy_in_from_array");

  int ioffset, ibuff;
  
// corner 000
  ioffset = idx(1,1,1);
  *buffers[0] = a.V[ ioffset ];

// edge 00z
  ioffset = idx(1,1,1);
  for( int i=0; i<nz; i++ ) buffers[1][i] = a.V[ ioffset + i ];

// corner 001
  *buffers[2] = a.V[ idx( 1, 1, nz ) ];

// edge 0y0
  ioffset = idx(1,1,1);
  for( int i=0; i<ny; i++ )
  {
    buffers[3][i] = a.V[ ioffset ];
    ioffset += nz2;
  }

// face 0yz
  ibuff=0;
  ioffset = idx(1,1,1);
  for( int i=0; i<ny; i++ )
  {
    for( int j=0; j<nz; j++ )
      buffers[4][ibuff++] = a.V[ ioffset + j ];
    ioffset += nz2;
  }

// edge 0y1
  ioffset = idx(1,1,nz);
  for( int i=0; i<ny; i++ )
  {
    buffers[5][i] = a.V[ ioffset ];
    ioffset += nz2;
  }

// corner 010
  ioffset = idx(1,ny,1);
  *buffers[6] = a.V[ ioffset ];

// edge 01z
  ioffset = idx(1,ny,1);
  for( int i=0; i<nz; i++ )
    buffers[7][i] = a.V[ ioffset + i ];

// corner 011
  *buffers[8] = a.V[ idx( 1, ny, nz ) ];

// edge x00
  ioffset = idx(1,1,1);
  for( int i=0; i<nx; i++ )
  {
    buffers[9][i] = a.V[ ioffset ];
    ioffset += nyz2;
  }

// face x0z
  ibuff=0;
  ioffset = idx(1,1,1); 
  for( int i=0; i<nx; i++ )
  {
    for( int j=0; j<nz; j++ )
      buffers[10][ibuff++] = a.V[ ioffset + j ];
    ioffset += nyz2;
  }

// edge x01
  ioffset = idx(1,1,nz);
  for( int i=0; i<nx; i++ )
  {
    buffers[11][i] = a.V[ ioffset ];
    ioffset += nyz2;
  }

// face xy0
  ibuff=0;
  ioffset = idx(1,1,1); 
  for( int i=0; i<nx; i++ )
  {
    int joffset;
    joffset = 0;
    for( int j=0; j<ny; j++ )
    { buffers[12][ibuff++] = a.V[ ioffset + joffset];  joffset += nz2; }
    ioffset += nyz2;
  }

// cube xyz

//  nothing here - no buffer

// face xy1
  ibuff=0;
  ioffset = idx(1,1,nz); 
  for( int i=0; i<nx; i++ )
  {
    int joffset;
    joffset = 0;
    for( int j=0; j<ny; j++ )
    { buffers[14][ibuff++] = a.V[ ioffset + joffset];  joffset += nz2; }
    ioffset += nyz2;
  }
  
// edge x10
  ioffset = idx(1,ny,1);
  for( int i=0; i<nx; i++ )
  {
    buffers[15][i] = a.V[ ioffset ];
    ioffset += nyz2;
  }

// face x1z
  ibuff=0;
  ioffset = idx(1,ny,1); 
  for( int i=0; i<nx; i++ )
  {
    for( int j=0; j<nz; j++ )
      buffers[16][ibuff++] = a.V[ ioffset + j ];
    ioffset += nyz2;
  }

// edge x11
  ioffset = idx(1,ny,nz);
  for( int i=0; i<nx; i++ )
  {
    buffers[17][i] = a.V[ ioffset ];
    ioffset += nyz2;
  }

// corner 100
  ioffset = idx(nx,1,1);
  *buffers[18] = a.V[ ioffset ];

// edge 10z
  ioffset = idx(nx,1,1);
  for( int i=0; i<nz; i++ )
    buffers[19][i] = a.V[ ioffset + i ];

// corner 101
  *buffers[20] = a.V[ idx( nx, 1, nz ) ];

// edge 1y0
  ioffset = idx(nx,1,1);
  for( int i=0; i<ny; i++ )
  {
    buffers[21][i] = a.V[ ioffset ];
    ioffset += nz2;
  }

// face 1yz
  ibuff=0;
  ioffset = idx(nx,1,1);
  for( int i=0; i<ny; i++ )
  {
    for( int j=0; j<nz; j++ )
      buffers[22][ibuff++] = a.V[ ioffset + j ];
    ioffset += nz2;
  }

// edge 1y1
  ioffset = idx(nx,1,nz);
  for( int i=0; i<ny; i++ )
  {
    buffers[23][i] = a.V[ ioffset ];
    ioffset += nz2;
  }

// corner 110
  ioffset = idx(nx,ny,1);
  *buffers[24] = a.V[ ioffset ];

// edge 11z
  ioffset = idx(nx,ny,1);
  for( int i=0; i<nz; i++ ) buffers[25][i] = a.V[ ioffset + i ];

// corner 111
  *buffers[26] = a.V[ idx( nx, ny, nz ) ];

} //end ScalarGcellBuffer::copy_in_from_array

// <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>

/* \brief copy contents of array ghost cells to Gcell buffers


*/

void ScalarGcellBuffer::copy_in_from_array_gcells( const ScalarZarray& a )
{
  int ioffset, ibuff;
  
// corner 000
  ioffset = idx(0,0,0);
  *buffers[0] = a.V[ ioffset ];

// edge 00z
  ioffset = idx(0,0,1);
  for( int i=0; i<nz; i++ ) buffers[1][i] = a.V[ ioffset + i ];

// corner 001
  *buffers[2] = a.V[ idx( 0, 0, nz1 ) ];

// edge 0y0
  ioffset = idx(0,1,0);
  for( int i=0; i<ny; i++ )
  {
    buffers[3][i] = a.V[ ioffset ];
    ioffset += nz2;
  }

// face 0yz
  ibuff=0;
  ioffset = idx(0,1,1);
  for( int i=0; i<ny; i++ )
  {
    for( int j=0; j<nz; j++ )
      buffers[4][ibuff++] = a.V[ ioffset + j ];
    ioffset += nz2;
  }

// edge 0y1
  ioffset = idx(0,1,nz1);
  for( int i=0; i<ny; i++ )
  {
    buffers[5][i] = a.V[ ioffset ];
    ioffset += nz2;
  }

// corner 010
  ioffset = idx(0,ny1,0);
  *buffers[6] = a.V[ ioffset ];

// edge 01z
  ioffset = idx(0,ny1,1);
  for( int i=0; i<nz; i++ )
    buffers[7][i] = a.V[ ioffset + i ];

// corner 011
  *buffers[8] = a.V[ idx( 0, ny1, nz1 ) ];

// edge x00
  ioffset = idx(1,0,0);
  for( int i=0; i<nx; i++ )
  {
    buffers[9][i] = a.V[ ioffset ];
    ioffset += nyz2;
  }

// face x0z
  ibuff=0;
  ioffset = idx(1,0,1); 
  for( int i=0; i<nx; i++ )
  {
    for( int j=0; j<nz; j++ )
      buffers[10][ibuff++] = a.V[ ioffset + j ];
    ioffset += nyz2;
  }

// edge x01
  ioffset = idx(1,0,nz1);
  for( int i=0; i<nx; i++ )
  {
    buffers[11][i] = a.V[ ioffset ];
    ioffset += nyz2;
  }

// face xy0
  ibuff=0;
  ioffset = idx(1,1,0); 
  for( int i=0; i<nx; i++ )
  {
    int joffset;
    joffset = 0;
    for( int j=0; j<ny; j++ )
    { buffers[12][ibuff++] = a.V[ ioffset + joffset]; joffset += nz2; }
    ioffset += nyz2;
  }

// cube xyz

//  nothing here - no buffer

// face xy1
  ibuff=0;
  ioffset = idx(1,1,nz1); 
  for( int i=0; i<nx; i++ )
  {
    int joffset;
    joffset = 0;
    for( int j=0; j<ny; j++ )
    { buffers[14][ibuff++] = a.V[ ioffset + joffset]; joffset += nz2; }
    ioffset += nyz2;
  }
  
// edge x10
  ioffset = idx(1,ny1,0);
  for( int i=0; i<nx; i++ )
  {
    buffers[15][i] = a.V[ ioffset ];
    ioffset += nyz2;
  }

// face x1z
  ibuff=0;
  ioffset = idx(1,ny1,1); 
  for( int i=0; i<nx; i++ )
  {
    for( int j=0; j<nz; j++ )
      buffers[16][ibuff++] = a.V[ ioffset + j ];
    ioffset += nyz2;
  }

// edge x11
  ioffset = idx(1,ny1,nz1);
  for( int i=0; i<nx; i++ )
  {
    buffers[17][i] = a.V[ ioffset ];
    ioffset += nyz2;
  }

// corner 100
  ioffset = idx(nx1,0,0);
  *buffers[18] = a.V[ ioffset ];

// edge 10z
  ioffset = idx(nx1,0,1);
  for( int i=0; i<nz; i++ )
    buffers[19][i] = a.V[ ioffset + i ];

// corner 101
  *buffers[20] = a.V[ idx( nx1, 0, nz1 ) ];

// edge 1y0
  ioffset = idx(nx1,1,0);
  for( int i=0; i<ny; i++ )
  {
    buffers[21][i] = a.V[ ioffset ];
    ioffset += nz2;
  }

// face 1yz
  ibuff=0;
  ioffset = idx(nx1,1,1);
  for( int i=0; i<ny; i++ )
  {
    for( int j=0; j<nz; j++ )
      buffers[22][ibuff++] = a.V[ ioffset + j ];
    ioffset += nz2;
  }

// edge 1y1
  ioffset = idx(nx1,1,nz1);
  for( int i=0; i<ny; i++ )
  {
    buffers[23][i] = a.V[ ioffset ];
    ioffset += nz2;
  }

// corner 110
  ioffset = idx(nx1,ny1,0);
  *buffers[24] = a.V[ ioffset ];

// edge 11z
  ioffset = idx(nx1,ny1,1);
  for( int i=0; i<nz; i++ )
    buffers[25][i] = a.V[ ioffset + i ];

// corner 111
  *buffers[26] = a.V[ idx( nx1, ny1, nz1 ) ];

} //end ScalarGcellBuffer::copy_in_from_array_gcells
 


// <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>

/* \brief copy contents of Gcell buffers to ghost cells of array


*/

void ScalarGcellBuffer::copy_out_to_array_gcells( const ScalarZarray& a )
{
  int ioffset, ibuff;
  
// corner 000
  ioffset = idx(0,0,0);
  a.V[ ioffset ] = *buffers[0] ;

// edge 00z
  ioffset = idx(0,0,1);
  for( int i=0; i<nz; i++ ) a.V[ ioffset + i ] = buffers[1][i];

// corner 001
  a.V[ idx( 0, 0, nz1 ) ] = *buffers[2];

// edge 0y0
  ioffset = idx(0,1,0);
  for( int i=0; i<ny; i++ )
  {
    a.V[ ioffset ] = buffers[3][i];
    ioffset += nz2;
  }

// face 0yz
  ibuff=0;
  ioffset = idx(0,1,1);
  for( int i=0; i<ny; i++ )
  {
    for( int j=0; j<nz; j++ )
      a.V[ ioffset + j ] = buffers[4][ibuff++];
    ioffset += nz2;
  }

// edge 0y1
  ioffset = idx(0,1,nz1);
  for( int i=0; i<ny; i++ )
  {
    a.V[ ioffset ] = buffers[5][i];
    ioffset += nz2;
  }

// corner 010
  ioffset = idx(0,ny1,0);
  a.V[ ioffset ] = *buffers[6];

// edge 01z
  ioffset = idx(0,ny1,1);
  for( int i=0; i<nz; i++ )
    a.V[ ioffset + i ] = buffers[7][i];

// corner 011
  a.V[ idx( 0, ny1, nz1 ) ] = *buffers[8];

// edge x00
  ioffset = idx(1,0,0);
  for( int i=0; i<nx; i++ )
  {
    a.V[ ioffset ] = buffers[9][i];
    ioffset += nyz2;
  }

// face x0z
  ibuff=0;
  ioffset = idx(1,0,1); 
  for( int i=0; i<nx; i++ )
  {
    for( int j=0; j<nz; j++ )
      a.V[ ioffset + j ] = buffers[10][ibuff++];
    ioffset += nyz2;
  }

// edge x01
  ioffset = idx(1,0,nz1);
  for( int i=0; i<nx; i++ )
  {
    a.V[ ioffset ] = buffers[11][i];
    ioffset += nyz2;
  }

// face xy0
  ibuff=0;
  ioffset = idx(1,1,0); 
  for( int i=0; i<nx; i++ )
  {
    int joffset;
    joffset = 0;
    for( int j=0; j<ny; j++ )
    { a.V[ ioffset + joffset] = buffers[12][ibuff++]; joffset += nz2; }
    ioffset += nyz2;
  }

// cube xyz

//  nothing here - no buffer

// face xy1
  ibuff=0;
  ioffset = idx(1,1,nz1); 
  for( int i=0; i<nx; i++ )
  {
    int joffset;
    joffset = 0;
    for( int j=0; j<ny; j++ )
    { a.V[ ioffset + joffset] = buffers[14][ibuff++]; joffset += nz2; }
    ioffset += nyz2;
  }
  
// edge x10
  ioffset = idx(1,ny1,0);
  for( int i=0; i<nx; i++ )
  {
    a.V[ ioffset ] = buffers[15][i];
    ioffset += nyz2;
  }

// face x1z
  ibuff=0;
  ioffset = idx(1,ny1,1); 
  for( int i=0; i<nx; i++ )
  {
    for( int j=0; j<nz; j++ )
      a.V[ ioffset + j ] = buffers[16][ibuff++];
    ioffset += nyz2;
  }

// edge x11
  ioffset = idx(1,ny1,nz1);
  for( int i=0; i<nx; i++ )
  {
    a.V[ ioffset ] = buffers[17][i];
    ioffset += nyz2;
  }

// corner 100
  ioffset = idx(nx1,0,0);
  a.V[ ioffset ] = *buffers[18];

// edge 10z
  ioffset = idx(nx1,0,1);
  for( int i=0; i<nz; i++ )
    a.V[ ioffset + i ] = buffers[19][i];

// corner 101
  a.V[ idx( nx1, 0, nz1 ) ] = *buffers[20];

// edge 1y0
  ioffset = idx(nx1,1,0);
  for( int i=0; i<ny; i++ )
  {
    a.V[ ioffset ] = buffers[21][i];
    ioffset += nz2;
  }

// face 1yz
  ibuff=0;
  ioffset = idx(nx1,1,1);
  for( int i=0; i<ny; i++ )
  {
    for( int j=0; j<nz; j++ )
      a.V[ ioffset + j ] = buffers[22][ibuff++];
    ioffset += nz2;
  }

// edge 1y1
  ioffset = idx(nx1,1,nz1);
  for( int i=0; i<ny; i++ )
  {
    a.V[ ioffset ] = buffers[23][i];
    ioffset += nz2;
  }

// corner 110
  ioffset = idx(nx1,ny1,0);
  a.V[ ioffset ] = *buffers[24];

// edge 11z
  ioffset = idx(nx1,ny1,1);
  for( int i=0; i<nz; i++ )
    a.V[ ioffset + i ] = buffers[25][i];

// corner 111
  a.V[ idx( nx1, ny1, nz1 ) ] = *buffers[26];

} //end ScalarGcellBuffer::copy_out_to_array_gcells
 


// <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>

// <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>
/* \brief use Gcell buffers to increment interior edges/faces of array

*/
void ScalarGcellBuffer::copy_out_to_array_increment( const ScalarZarray& a )
{
  int ioffset, ibuff;
  
// corner 000
  ioffset = idx(1,1,1);
  a.V[ ioffset ] += *buffers[0];

// edge 00z
  ioffset = idx(1,1,1);
  for( int i=0; i<nz; i++ ) a.V[ ioffset + i ] += buffers[1][i];

// corner 001
  a.V[ idx( 1, 1, nz ) ] += *buffers[2];

// edge 0y0
  ioffset = idx(1,1,1);
  for( int i=0; i<ny; i++ )
  {
    a.V[ ioffset ] += buffers[3][i];
    ioffset += nz2;
  }

// face 0yz
  ibuff=0;
  ioffset = idx(1,1,1);
  for( int i=0; i<ny; i++ )
  {
    for( int j=0; j<nz; j++ )
      a.V[ ioffset + j ] += buffers[4][ibuff++];
    ioffset += nz2;
  }

// edge 0y1
  ioffset = idx(1,1,nz);
  for( int i=0; i<ny; i++ )
  {
    a.V[ ioffset ] += buffers[5][i];
    ioffset += nz2;
  }

// corner 010
  ioffset = idx(1,ny,1);
  a.V[ ioffset ] += *buffers[6];

// edge 01z
  ioffset = idx(1,ny,1);
  for( int i=0; i<nz; i++ )
    a.V[ ioffset + i ] += buffers[7][i];

// corner 011
  a.V[ idx( 1, ny, nz ) ] += *buffers[8];

// edge x00
  ioffset = idx(1,1,1);
  for( int i=0; i<nx; i++ )
  {
    a.V[ ioffset ] += buffers[9][i];
    ioffset += nyz2;
  }

// face x0z
  ibuff=0;
  ioffset = idx(1,1,1); 
  for( int i=0; i<nx; i++ )
  {
    for( int j=0; j<nz; j++ )
      a.V[ ioffset + j ] += buffers[10][ibuff++];
    ioffset += nyz2;
  }

// edge x01
  ioffset = idx(1,1,nz);
  for( int i=0; i<nx; i++ )
  {
    a.V[ ioffset ] += buffers[11][i];
    ioffset += nyz2;
  }

// face xy0
  ibuff=0;
  ioffset = idx(1,1,1); 
  for( int i=0; i<nx; i++ )
  {
    int joffset;
    joffset = 0;
    for( int j=0; j<ny; j++ )
    { a.V[ ioffset + joffset] += buffers[12][ibuff++];  joffset += nz2; }
    ioffset += nyz2;
  }

// cube xyz

//  nothing here - no buffer

// face xy1
  ibuff=0;
  ioffset = idx(1,1,nz); 
  for( int i=0; i<nx; i++ )
  {
    int joffset;
    joffset = 0;
    for( int j=0; j<ny; j++ )
    { a.V[ ioffset + joffset] += buffers[14][ibuff++];  joffset += nz2; }
    ioffset += nyz2;
  }
  
// edge x10
  ioffset = idx(1,ny,1);
  for( int i=0; i<nx; i++ )
  {
    a.V[ ioffset ] += buffers[15][i];
    ioffset += nyz2;
  }

// face x1z
  ibuff=0;
  ioffset = idx(1,ny,1); 
  for( int i=0; i<nx; i++ )
  {
    for( int j=0; j<nz; j++ )
      a.V[ ioffset + j ] += buffers[16][ibuff++];
    ioffset += nyz2;
  }

// edge x11
  ioffset = idx(1,ny,nz);
  for( int i=0; i<nx; i++ )
  {
    a.V[ ioffset ] += buffers[17][i];
    ioffset += nyz2;
  }

// corner 100
  ioffset = idx(nx,1,1);
  a.V[ ioffset ] += *buffers[18];

// edge 10z
  ioffset = idx(nx,1,1);
  for( int i=0; i<nz; i++ )
    a.V[ ioffset + i ] += buffers[19][i];

// corner 101
  a.V[ idx( nx, 1, nz ) ] += *buffers[20];

// edge 1y0
  ioffset = idx(nx,1,1);
  for( int i=0; i<ny; i++ )
  {
    a.V[ ioffset ] += buffers[21][i];
    ioffset += nz2;
  }

// face 1yz
  ibuff=0;
  ioffset = idx(nx,1,1);
  for( int i=0; i<ny; i++ )
  {
    for( int j=0; j<nz; j++ )
      a.V[ ioffset + j ] += buffers[22][ibuff++];
    ioffset += nz2;
  }

// edge 1y1
  ioffset = idx(nx,1,nz);
  for( int i=0; i<ny; i++ )
  {
    a.V[ ioffset ] += buffers[23][i];
    ioffset += nz2;
  }

// corner 110
  ioffset = idx(nx,ny,1);
  a.V[ ioffset ] += *buffers[24];

// edge 11z
  ioffset = idx(nx,ny,1);
  for( int i=0; i<nz; i++ ) a.V[ ioffset + i ] += buffers[25][i];

// corner 111
  a.V[ idx( nx, ny, nz ) ] += *buffers[26];

} //end ScalarGcellBuffer::copy_out_to_array_increment

// <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>

// <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>


void ScalarZarray::mpi_recv( int src, int tag )
{
  int mpierr;
  MPI_Status mpi_recv_status;
  mpierr = MPI_Recv( V, nxyz2, MPI_DOUBLE, src, tag,
             MPI_COMM_WORLD, &mpi_recv_status );
  if( mpierr != MPI_SUCCESS )
    throw HypsiMPIException( 
      "MPI_Recv failed", "ScalarZarray::mpi_recv", mpierr);
}

void ScalarZarray::mpi_irecv( int src, int tag, MPI_Request* mpi_request )
{
  int mpierr;
  mpierr = MPI_Irecv( V, nxyz2, MPI_DOUBLE, src, tag,
             MPI_COMM_WORLD, mpi_request );
  if( mpierr != MPI_SUCCESS )
    throw HypsiMPIException(
    "MPI_Irecv failed", "ScalarZarray::mpi_irecv", mpierr);
}

void ScalarZarray::mpi_send( int dst, int tag )
{
  int mpierr = MPI_Send( V, nxyz2, MPI_DOUBLE, dst, tag,
                 MPI_COMM_WORLD );
  if( mpierr != MPI_SUCCESS )
    throw HypsiMPIException(
    "MPI_Send failed", "ScalarZarray::mpi_send", mpierr);
}

void ScalarZarray::mpi_isend( int dst, int tag, MPI_Request* mpi_request )
{
  int mpierr = MPI_Isend( V, nxyz2, MPI_DOUBLE, dst, tag,
                 MPI_COMM_WORLD, mpi_request );
  if( mpierr != MPI_SUCCESS )
    throw HypsiMPIException(
    "MPI_Isend failed", "ScalarZarray::mpi_isend", mpierr);
}

void ScalarZarray::make_xyzperiodic( void )
{
   send_gcell_buff.copy_in_from_array( *this );

   recv_gcell_buff.irecv_from_neighbours();

//cout <<"ScalarZarray::make_xyzperiodic: posted irecv\n";

   MPI_Barrier( MPI_COMM_WORLD );

   send_gcell_buff.isend_to_neighbours();

   while( ! ( recv_gcell_buff.irecv_complete()
                && send_gcell_buff.isend_complete() ) )
   { ; }

   recv_gcell_buff.copy_out_to_array_gcells( *this );

   MPI_Barrier( MPI_COMM_WORLD );
}

void ScalarZarray::adjust_moment_edges_xyzperiodic( void )
{
   send_gcell_buff.copy_in_from_array_gcells( *this );

   recv_gcell_buff.irecv_from_neighbours();

//cout <<"ScalarZarray::adjust_moment_edges_xyzperiodic: posted irecv\n";

   MPI_Barrier( MPI_COMM_WORLD );

   send_gcell_buff.isend_to_neighbours();

   while( ! ( recv_gcell_buff.irecv_complete()
                && send_gcell_buff.isend_complete() ) )
   { ; }

   recv_gcell_buff.copy_out_to_array_increment( *this );

   MPI_Barrier( MPI_COMM_WORLD );
}



// <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>


}  // end namespace HYPSI
