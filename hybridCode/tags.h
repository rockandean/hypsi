//! file tags.h


namespace HYPSI {

extern const int TAG_RQ_ZONEFIELDS;
extern const int TAG_ZONEFIELDS;
extern const int TAG_PMOVE_ZMOMENTS;
extern const int TAG_NOPARTICLESINZONE;
extern const int TAG_COLLECTNV_ZMOMENTS;
extern const int TAG_COLLECTNV_T_ZMOMENTS;
extern const int TAG_NNEIGHBOURS_SEND;

}
