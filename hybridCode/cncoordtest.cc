/*! \file cncoordtest.cc
 *\warning  Not clear about this file usage within HYPSI.
*/
#include <iostream>
using namespace std;

int main(void)
{
    /*! Number of Cores per axis:
     *      - \b nxcn cores x-axis
     *      - \b nycn cores y-axis
     *      - \b nzcn cores z-axis
     *      - \b cntot Total number of cores
     */
  int nxcn = 3,nycn=3, nzcn=2, cntot = nxcn*nycn*nzcn;
  int nyzcn = nycn*nzcn;
  int ix, iy, iz, irem, j;
  
  for(int i=0; i<cntot; i++)
    {
     ix = i / nyzcn;
     irem = i % nyzcn;
     iy = irem / nzcn;
     iz = irem % nzcn;
    
     j = ix * nyzcn + iy * nzcn + iz;

     cout << i << ": " << ix <<", "<<iy<<", "<<iz<<" ---> "<<j<<"\n";
    }

  int ixcn = 1, iycn=1, izcn=1;

  cout << "Neighbours for : " << ixcn<<", "<<iycn<<", "<<izcn<<"\n";

  int jj=0;
  for(int ixx=ixcn-1;ixx<=ixcn+1;ixx++)
  for(int iyy=iycn-1;iyy<=iycn+1;iyy++)
  for(int izz=izcn-1;izz<=izcn+1;izz++)
    {
      jj++;
      int neighbour = (ixx+nxcn)%nxcn * nyzcn
                           + (iyy+nycn)%nycn * nzcn
                           + (izz+nzcn)%nzcn;

      int jx = neighbour / nyzcn;
      int irem = neighbour % nyzcn;
      int jy = irem / nzcn;
      int jz = irem % nzcn;

      cout << jj << ": " << neighbour <<"  ( "<<jx<<", "<<jy<<", "<<jz<<")\n";
     
    }
return 0;
}
