#include "hypsi.h"

namespace HYPSI {

using namespace std;


// PTAF_collectnvt: PARTICLE THREAD ACTION FUNCTOR: COLLECT NV
// -----------------------------------------------------------------------

PTAF_collectnvt::PTAF_collectnvt( const Zone& zone )
    : PTTHR_ActionFunctor()
{
    _zone = new Zone( zone );
    nvtmoms = new NVTMoments( zone );
}

PTAF_collectnvt::~PTAF_collectnvt( void )
{
  delete _zone;
  delete nvtmoms;
}

void PTAF_collectnvt::set_zone( const Zone& zone )
{
 (*_zone)=zone;
 _zoid = zone.izone;
}

void PTAF_collectnvt::action( void )
{
  int np, ipc, ip;
  double* pp;
  double xp, yp, zp, vxp, vyp, vzp;
  InterpWeights w;

  try {

  np = _psetp->zoneidx_list[_zoid].num;

/*
  cout << "Z" << _hostzone << " PTAF_collectnvt::action: for ipset: <"
       << _psetp->type()
  << "> zone: " << _zoid
  << " np: "  << np
  << " idx: [" 
  << _psetp->zoneidx_list[_zoid].first
  << " -> " << _psetp->zoneidx_list[_zoid].last << "]\n";
*/

// initialize moment accumulation arrays

// zero all moment arrays
  nvtmoms->set_zero();

// loop over particles in this zone    
  for( ipc = _psetp->zoneidx_list[_zoid].first ;
       ipc <= _psetp->zoneidx_list[_zoid].last ; ++ipc )
  {
    ip = _psetp->plist[ipc];

    pp = _psetp->pdata + ip*6;

    xp = pp[0];
    yp = pp[1];
    zp = pp[2];
    vxp = pp[3];
    vyp = pp[4];
    vzp = pp[5];

    _zone->calculate_eweights( pp, w );

// dna, Uax etc collected using old position E cell weightings

    nvtmoms->dn[ w.ei000 ] += w.ew000;
    nvtmoms->dn[ w.ei100 ] += w.ew100;
    nvtmoms->dn[ w.ei010 ] += w.ew010;
    nvtmoms->dn[ w.ei110 ] += w.ew110;
    nvtmoms->dn[ w.ei001 ] += w.ew001;
    nvtmoms->dn[ w.ei101 ] += w.ew101;
    nvtmoms->dn[ w.ei011 ] += w.ew011;
    nvtmoms->dn[ w.ei111 ] += w.ew111;

    nvtmoms->Vx[ w.ei000 ] += w.ew000 * vxp;
    nvtmoms->Vx[ w.ei100 ] += w.ew100 * vxp;
    nvtmoms->Vx[ w.ei010 ] += w.ew010 * vxp;
    nvtmoms->Vx[ w.ei110 ] += w.ew110 * vxp;
    nvtmoms->Vx[ w.ei001 ] += w.ew001 * vxp;
    nvtmoms->Vx[ w.ei101 ] += w.ew101 * vxp;
    nvtmoms->Vx[ w.ei011 ] += w.ew011 * vxp;
    nvtmoms->Vx[ w.ei111 ] += w.ew111 * vxp;

    nvtmoms->Vy[ w.ei000 ] += w.ew000 * vyp;
    nvtmoms->Vy[ w.ei100 ] += w.ew100 * vyp;
    nvtmoms->Vy[ w.ei010 ] += w.ew010 * vyp;
    nvtmoms->Vy[ w.ei110 ] += w.ew110 * vyp;
    nvtmoms->Vy[ w.ei001 ] += w.ew001 * vyp;
    nvtmoms->Vy[ w.ei101 ] += w.ew101 * vyp;
    nvtmoms->Vy[ w.ei011 ] += w.ew011 * vyp;
    nvtmoms->Vy[ w.ei111 ] += w.ew111 * vyp;

    nvtmoms->Vz[ w.ei000 ] += w.ew000 * vzp;
    nvtmoms->Vz[ w.ei100 ] += w.ew100 * vzp;
    nvtmoms->Vz[ w.ei010 ] += w.ew010 * vzp;
    nvtmoms->Vz[ w.ei110 ] += w.ew110 * vzp;
    nvtmoms->Vz[ w.ei001 ] += w.ew001 * vzp;
    nvtmoms->Vz[ w.ei101 ] += w.ew101 * vzp;
    nvtmoms->Vz[ w.ei011 ] += w.ew011 * vzp;
    nvtmoms->Vz[ w.ei111 ] += w.ew111 * vzp;

  }  // end of loop over particles


  } catch ( HypsiException& e )
  { 
    cout << "HYPSI Exception in particle thread\n";
    e.diag_cout();
    cout << endl;
    e.push_err_msg("HYPSI Exception in PTAF_collectnvt::action" );
    throw e;
  }

}

// **************************************************************************
// **************************************************************************

void HypsimCollectnvtMsgHandler::handle_incoming_messages(
       const string& dstr,
       int max_msg_count,
       NVTMoments* pset_nvtmoms_ptr )
{
  int mpierr;
  int mpi_iprobe_flag;
  MPI_Status mpi_iprobe_status;
  MPI_Status mpi_recv_status;

  int mpi_work_count = 0;

  _n_times_called++;
  
  while( MPI_Iprobe( MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD,
		     &mpi_iprobe_flag, &mpi_iprobe_status ),
	( mpi_iprobe_flag && mpi_work_count < max_msg_count ) )
  {

    mpi_work_count++;

//   >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> test for arrival of particle zone moments

    MPI_Iprobe( MPI_ANY_SOURCE, TAG_COLLECTNV_ZMOMENTS,
                MPI_COMM_WORLD,
       	        &mpi_iprobe_flag, &mpi_iprobe_status );

    if( mpi_iprobe_flag )
    {

// receive zone moments from zone MPI_SOURCE for this hostzone
// add into zone moments for this hostzone and for current ipset
// and update status info

      _nvtmoms->mpi_recv_nv( mpi_iprobe_status.MPI_SOURCE,
                              TAG_COLLECTNV_ZMOMENTS );

      pset_nvtmoms_ptr->addto_nv( *_nvtmoms );

      _nvtmoms_recv_list[ mpi_iprobe_status.MPI_SOURCE ] = true;
      _nvtmoms_recv_n++;
      _nvtmoms_recv_complete = (_nvtmoms_recv_n == _n_zones);

    }  // end of receive zone moments

// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> test for arrival of  NO PARTICLES IN ZONE

    MPI_Iprobe( MPI_ANY_SOURCE, TAG_NOPARTICLESINZONE,
		  MPI_COMM_WORLD,
		  &mpi_iprobe_flag, &mpi_iprobe_status );

    if( mpi_iprobe_flag )
    {

      mpierr = MPI_Recv( &_mpidummy_noptcles_recv, 1, MPI_INT,
		 mpi_iprobe_status.MPI_SOURCE,
		 TAG_NOPARTICLESINZONE,
		 MPI_COMM_WORLD,
		 &mpi_recv_status );

      if( mpierr != MPI_SUCCESS )
	throw HypsiMPIException(
	  "MPI_Recv for TAG_NOPARTICLESINZONE failed",
	  "HypsimPmoveMsgHandler::handle_incoming_messages::pmove", mpierr);

// no particles in this(host) zone from zone  mpi_iprobe_status.MPI_SOURCE
// so act as if  nvtmoms have been received

      _nvtmoms_recv_list[ mpi_iprobe_status.MPI_SOURCE ] = true;
      _nvtmoms_recv_n++;
      _nvtmoms_recv_complete = (_nvtmoms_recv_n == _n_zones);

    } // end of if( iprobe( ...TAG_NOPARTICLESINZONE .. )  )

// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ADD Further tests here (?)

  } // end of while( mpi probe and mpi_work_count < .. )

  if( mpi_work_count >= max_msg_count )
    _n_times_max_reached++;
}

// **************************************************************************
// **************************************************************************
void Hypsim::collect_nvtmoms_xyzperiodic( bool collectT )
{
  collect_nvtmoms( collectT );

  for( int ipset=0; ipset < sim_params.npsets; ++ipset )
  {
    pset_nvtmoms[ipset]->make_nv_xyzperiodic();
  }
}

// **************************************************************************
// **************************************************************************
void Hypsim::collect_nvtmoms( bool collectT )
{
  int mpi_iprobe_flag, mpi_test_flag;
  MPI_Status mpi_iprobe_status, mpi_test_status;
  MPI_Status mpi_recv_status;
  int mpierr;

  bool rq_zonef_pending;

  int mpidummy_rq_zonf_send=123, mpidummy_rq_zonf_recv;

  MPI_Request nvtmoms_send_rq;

  PTTHR::State xstate;

  int trylock_count = 0;

// create Particle Thread Action Functor & Initialize

// NB: some other quantities are initialized later, before thread work starts

  PTAF_collectnvt* ptaf_p = new PTAF_collectnvt( zinfo );
  
  ptaf_p->set_hostzone_id( sim_params.hostzone );

  PTTHR ptthr( ptaf_p, sim_params.hostzone );

  ptthr.set_state( PTTHR::THR_STATE_NOT_READY );

  ptthr.lock_mutex();

  ptthr.thread_create();

  ptthr.cond_wait();
  
  if( ptthr._state != PTTHR::THR_STATE_READY )
    throw HypsiException(
            "State not THR_STATE_READY after thread create",
            "Hypsim::collectnv" );

  ptthr.unlock_mutex();

  MPI_Barrier( MPI_COMM_WORLD );

// ------------------------------------------------------ start of ipset loop
  for( int ipset=0; ipset < sim_params.npsets; ++ipset )
  {

// zero the collectnv zone moments for this particle set

    pset_nvtmoms[ipset]->set_zero();

// initializations for incoming message handler

    collectnvt_msg_handler->initialize();

#ifdef DIAG_COLLECTNVT
dmsg("collect_nvtmoms: start of ipset loop: about to MPI barrier");
#endif  

    MPI_Barrier( MPI_COMM_WORLD );

// --------------------------------------------- start of particle zone loop

    for( int izc=0; izc < sim_params.n_zones; ++izc )
    {
      int iz = (izc + sim_params.hostzone) % sim_params.n_zones;

#ifdef DIAG_COLLECTNVT
cout<< "Z" << sim_params.hostzone
    << ": collect_nvtmoms: ipset, iz  zone: "<< ipset << ", " << iz << endl;
#endif

//   >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> collectnvt message handler

      collectnvt_msg_handler->handle_incoming_messages(
        "COLLECTNVT_STAGE1", 25, pset_nvtmoms[ipset] );

//   <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

// if there are no particles for this pset in this zone ....
//   send TAG_NOPARTICLESINZONE
//   ... and that's all!

      if( psets[ ipset ]->zoneidx_list[ iz ].num == 0 )
      {

	
#ifdef DIAG_COLLECTNVT
	//cout<< "Z" << sim_params.hostzone
	//    << ": COLLECT NVT: NO PARTICLES: ipset, iz: "<< ipset << ", " << iz << endl;
#endif

        mpierr = MPI_Issend(
                   collectnvt_msg_handler->_mpidummy_noptcles_send+iz, 1,
                   MPI_INT,
                   iz, TAG_NOPARTICLESINZONE,
                   MPI_COMM_WORLD,
                   collectnvt_msg_handler->_noptcls_isend_rq+iz );

        if( mpierr != MPI_SUCCESS )
          throw HypsiMPIException(
            "MPI_Isend TAG_NOPARTICLESINZONE failed", "Hypsim::collect_nvtmoms", mpierr);

        collectnvt_msg_handler->_noptcls_isend_pending[ iz ] = true;

        continue;   // ie skip rest of loop

      }

// --------------------------------------- end of TAG_NOPARTICLESINZONE

// --------------------------------------- Start of particle work phase

      trylock_count = 0;

      while( true )
      {

      ptthr.ssleep( 0.01 );

      if( ptthr.trylock_mutex() )
      {

        xstate = ptthr.state();

#ifdef DIAG_COLLECTNVT
cout << "Z" << sim_params.hostzone
     << " COLLECTNVT: trylock successful:  state: "  << ptthr.state() << endl;
#endif
        if( xstate == PTTHR::THR_STATE_START_WORK 
            || xstate == PTTHR::THR_STATE_REQUEST_EXIT )
        {

#ifdef DIAG_COLLECTNVT
cout << "Z" << sim_params.hostzone
     << " COLLECTNVT: trylock ok: INCOMPLETE cond_signal: eg THR_STATE_START_WORK state:"
     << xstate << endl;
#endif
          ptthr.unlock_mutex();

        } else if( xstate == PTTHR::THR_STATE_WORKING )
        {
          trylock_count++;
          ptthr.unlock_mutex();

        } else if( xstate == PTTHR::THR_STATE_WORK_DONE )
        {
        
#ifdef DIAG_COLLECTNVT
dmsg("COLLECTNVT: trylock successful: RECEIVED THR_STATE_WORK_DONE");
cout << "Z" << sim_params.hostzone
     << ": COLLECTNVT: trylock count: " << trylock_count << endl;
#endif
          trylock_count = 0;

          ptthr.set_state( PTTHR::THR_STATE_READY );
          ptthr.unlock_mutex();
          break;

        } else if( xstate == PTTHR::THR_STATE_READY )
        {

// set up data for particle thread work, set working zone number
// set pointer to particle set for working, and time step

#ifdef DIAG_COLLECTNVT
dmsg("COLLECTNVT: trylock successful: ABOUT to set THR_STATE_START_WORK");
#endif
          ptaf_p->set_zone( zones_info[iz] );
          ptaf_p->set_psetp( psets[ ipset ] );
          ptthr.set_state( PTTHR::THR_STATE_START_WORK );

#ifdef DIAG_COLLECTNVT
dmsg("COLLECTNVT: trylock successful: ABOUT to cond_signal");
#endif
          ptthr.cond_signal();

#ifdef DIAG_COLLECTNVT
cout << "Z" << sim_params.hostzone
     << " COLLECTNVT: RETURNED from cond_signal, about to unlock: state: "
     << ptthr.state() << endl;
#endif

          ptthr.unlock_mutex();

        }

      } // -----------------------------------------  end of if( trylock() )
      

// ++++++++++++++++++++++++++++++++++++++++++++ MPI work during thread work

// Main thread work while waiting for particle thread to complete its work

//   >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> collectnvt message handler

      collectnvt_msg_handler->handle_incoming_messages(
        "COLLECTNVT_STAGE2", 25, pset_nvtmoms[ipset] );

//   <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


// --------------------------------------------------------------------------

      } // ------------------  end of while(true) loop (particle thread work)

// ----------------------------------------- pmove zone moments send to owner

// if the current zone is the host zone, then just access the
//   particle zone moments directly (since on same node)

      if( iz == sim_params.hostzone )
      {
        pset_nvtmoms[ipset]->addto_nv( *ptaf_p->nvtmoms );

        collectnvt_msg_handler->_nvtmoms_recv_list[ iz ] = true;
        collectnvt_msg_handler->_nvtmoms_recv_n++;
        collectnvt_msg_handler->_nvtmoms_recv_complete = 
	   (collectnvt_msg_handler->_nvtmoms_recv_n == sim_params.n_zones);
    
      } else
      {

// ELSE .... send the moments collected in particle thread to proper zone

#ifdef DIAG_COLLECTNVT
cout << "Z" << sim_params.hostzone << " COLLECTNVT: about to isend nvtmoms"
     << "to zone: " << iz << endl;
#endif

        ptaf_p->nvtmoms->mpi_isend_nv( iz, TAG_COLLECTNV_ZMOMENTS,
                                        &nvtmoms_send_rq );
      
#ifdef DIAG_COLLECTNVT
cout << "Z" << sim_params.hostzone
     << " COLLECTNVT: start MPI_Test loop on isend nvtmoms to Z"
     << iz << endl;
#endif

// ----------------------------------------- wait for nvtmoms send to complete

        while( MPI_Test( &nvtmoms_send_rq, &mpi_test_flag, &mpi_test_status ),
               !mpi_test_flag )
        {

//          ptthr.ssleep( 0.01 );

//   >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> collectnv message handler

          collectnvt_msg_handler->handle_incoming_messages(
            "COLLECTNVT_STAGE1", 25, pset_nvtmoms[ipset] );

//   <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

        
        }  // end of while( Test(  nvtmoms_send_rq ) )
 
#ifdef DIAG_COLLECTNVT
cout << "Z" << sim_params.hostzone
     << " COLLECTNVT: COMPLETED MPI_Test loop to Z" << iz << endl;
#endif
      }


    } // end of for( iz ... ) loop

// --------------------------------------------------- end of loop over zones

#ifdef DIAG_COLLECTNVT
cout << "Z" << sim_params.hostzone << " COLLECTNVT: end of iz zone loop"
     << " nvtmoms_recv_n: " << collectnvt_msg_handler->_nvmoms_recv_n << endl;
for( int i=0; i < sim_params.n_zones; i++ )
  if( !collectnvt_msg_handler->_nvtmoms_recv_list[ i ] )
    cout << "Z" << sim_params.hostzone
         << " nvtmoms_recv waiting for zone: " << i << endl;
#endif
    
// --------------------------------------------- post zone loop waiting work

   collectnvt_msg_handler->_noptcls_isend_all_complete = false;

    int n_nvtloop = 0;

    while( collectnvt_msg_handler->_nvtmoms_recv_n != sim_params.n_zones 
             || !collectnvt_msg_handler->_noptcls_isend_all_complete )
    {

      ptthr.ssleep( 0.01 );

      n_nvtloop++;

      if( !collectnvt_msg_handler->_noptcls_isend_all_complete )
      {
        int n_pending = 0;
        for(int iz=0; iz<sim_params.n_zones; iz++)
        {
          if( collectnvt_msg_handler->_noptcls_isend_pending[ iz ] )
          {
            MPI_Test( collectnvt_msg_handler->_noptcls_isend_rq+iz,
                        &mpi_test_flag, &mpi_test_status);
            if( mpi_test_flag )
              collectnvt_msg_handler->_noptcls_isend_pending[ iz ] = false;
            else
              n_pending++;
          }
        }
        collectnvt_msg_handler->_noptcls_isend_all_complete = (n_pending==0);

/*
if( ( n_pending > 0 && n_nvtloop < 5 ) ||
    ( n_pending > 0 && n_nvtloop >10 && n_nvtloop%50 == 0) )
  cout << "Z" << sim_params.hostzone
       <<" PMOVE: noptcls_isend_all_complete n_pending: "<< n_pending
       <<" (n_nvtloop: " << n_nvtloop << ")\n";
*/

      }

//   >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> collectnv message handler

      collectnvt_msg_handler->handle_incoming_messages(
        "COLLECTNV_STAGE3", 20, pset_nvtmoms[ipset] );

//   <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    } // end of while( sends/receives incomplete )

// ----------------------------------------------- END OF post zone loop work

#ifdef COLLECTNVT
cout << "Z" << sim_params.hostzone
     << "COLLECTNVT: end of EXTRA while loop rq_zonef_answered_n: " << rq_zonef_answered_n << endl;
#endif

// Adjust moments for this particle set according to
//   particle statistical weight

    pset_nvtmoms[ipset]->multiply_nv( psets[ ipset ]->w );

  } // end of ipset particle set loop

#ifdef DIAG_COLLECTNVT
dmsg("COLLECTNVT: exited from ipset, iz zone loops, about to MPI barrier");
#endif
  
  MPI_Barrier( MPI_COMM_WORLD );

#ifdef DIAG_COLLECTNVT
dmsg("COLLECTNVT: after MPI barrier, Now THREAD .. REQUEST_EXIT");
#endif

  ptthr.lock_mutex();

  ptthr.set_state( PTTHR::THR_STATE_REQUEST_EXIT );

  ptthr.cond_broadcast( );

  ptthr.unlock_mutex();

#ifdef DIAG_COLLECTNVT
dmsg( "COLLECTNVT:  about to thr_join " );
#endif

  ptthr.ssleep(0.01);

  ptthr.thread_join();

// clean up particle thread action functor
  delete ptaf_p;

  /*
if( sim_params.hostzone < 8 )
  cout << "Z" << sim_params.hostzone
     <<" COLLECTNVT: msg_handler num calls: "
     << collectnvt_msg_handler->_n_times_called
     <<" times max'd: "
     << collectnvt_msg_handler->_n_times_max_reached << "\n";
  */

} // end Hypsim::collectnvt



}  // end namespace HYPSI
