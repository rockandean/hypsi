/*! \file Header for Numerical Recipes RNG functions.
*/

#ifndef _NR_NRG_H_
#define _NR_NRG_H_

#ifdef __cplusplus
extern "C" {
#endif

float ran2(long *idum);
float ran4(long *idum);

#ifdef __cplusplus
}
#endif

#endif // _NR_NRG_H_
