#include <iostream>

#include "test_scalararray.h"

#include <string>

#include "rng.h"

using namespace std;

#include "ptcls.h"

using namespace std;
using namespace HYPSI;

main( int argc, char** argv )
{
  Timer timer;

  try
  {

  HypsimTest hypsim;


  hypsim.mpi_comm.initialize( &argc, &argv );
  
  cout << "Number of nodes: " << hypsim.mpi_comm.nnodes << "\n";
  cout << "This node: " << hypsim.mpi_comm.thisnode << "\n";
  
  hypsim.initialize( argc, argv );
  
  hypsim.zinfo.print( cout, "hypsi_main: This zone ... " );
  cout << " Barrier: zone: "<< hypsim.zinfo.izone<<"\n";

  MPI_Barrier( MPI_COMM_WORLD );

  ScalarZarray za_test( hypsim.zinfo );

  za_test.set_test_data();

  za_test.test_output( "testout/zatest_A", hypsim.zinfo.izone );

  cout<<"Z"<<hypsim.zinfo.izone << " za_test allocated ok: \n";

  MPI_Barrier( MPI_COMM_WORLD );

  za_test.make_xyzperiodic();

  za_test.test_output( "testout/zatest_B", hypsim.zinfo.izone );

  cout <<"Z"<<hypsim.zinfo.izone << " completed make_xyzperiodic \n";

  MPI_Barrier( MPI_COMM_WORLD );
  cout <<"Z"<<hypsim.zinfo.izone << " completed: \n";

  sleep(1);
  
// random number generator
  RNG prng;
  prng.set_seed( -12351 );

  ParticleSet pset( 10000 );  // space for 2 10^6 particles
  
  
  int np = 100;   // number of particle

  double vthpar = 1.0;
  double vthperp = 0.5;
  
  xyzVector Bvec( 1.0, 0.0, 0.0 );
  xyzVector Bhatvec( Bvec );
  Bhatvec.normalize();

  xyzRegion xyz_region( hypsim.domain_info.region );
  
  double Vparshift = 0.0;
  xyzVector Vshift( 0.0, 0.0, 0.0 );
  
  pidxDoublet pidx_range;

  pset.initialize_uniform_bimax(
    np,
    &prng,
    xyz_region,
    vthpar,
    vthperp,
    Bvec,
    Vparshift,
    Vshift,
    pidx_range );

  cout << "Added particles: " << pidx_range.first 
       << " -> " << pidx_range.last << "\n";

  pset.sort_by_zone( hypsim.domain_info, hypsim.zones_info );

  } catch( HypsiException& e )
  {
    cout << "HYPSI Exception at main level\n";
    e.diag_cout();
    cout << endl;
  }
  
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

namespace HYPSI {

void ConfigData::initialize( int argc, char** argv )
{

cout << "ConfigData::initialize  ";
cout << "argc: " << argc << "\n" ;
for(int i=0;i<argc;++i)
  cout << i << ": " << argv[i] << "\n" ;

  if( argc >= 2 )
    config_fname = argv[1];
  else
    throw HypsiException( "Insufficient arguments (no configuration file?)", "ConfigData::initialize" );

  try{
  
    cout << " about to read " << config_fname << "\n" ;

    read_file( config_fname );
    
    if( num_parse_errors() != 0 )
    {
      diag_print_errors() ;
      throw HypsiException(
        "Parse errors reading configuration file: <" + config_fname + ">",
        "ConfigData::initialize" );    }
 
  } catch( KVF::kvfException& e )
  {
    cout << "Caught KVF exception!\n";
    e.diag_cout();
    throw HypsiException(
      "Failed to open/read configuration file: <" + config_fname + ">",
      "ConfigData::initialize" );
  }

} // end ConfigData::initialize

void SimulationParameters::initialize_from_config_data( ConfigData& config_data )
{
  vector<double> domain_size_vec, domain_cellsize_vec;
  vector<double> cn_numxyz_vec;

  try{
    config_data.get_data( "output_control/data_filename_root", output_datafile_root );
    config_data.get_data( "time_step", dt );
    config_data.get_data( "n_timesteps", ntimesteps );
    config_data.get_data( "n_field_substeps", nsubsteps );
    config_data.get_data( "uniform_resistivity", uniform_resis );
    config_data.get_data( "initial_electron_temperature", initial_Te );
    config_data.get_data( "electron_fluid_gamma", electron_gamma );
    config_data.get_data( "domain/xyzsize", domain_size_vec );
    config_data.get_data( "domain/xyzcell", domain_cellsize_vec );
    
    config_data.get_data( "compute_nodes_xyz_geometry", cn_numxyz_vec );

    config_data.get_data( "particle_rng_seed", prng_seed );
    config_data.get_data( "particle_sets/num_psets", npsets );
    config_data.get_data(
      "smoothing_control/smoothfac_EinEBadvance", smoothfac_EinEBadvance );

  } catch( KVF::kvfException& e )
  {
    e.diag_cout();
    throw HypsiException(
      "Failed to get simulation parameters data from file: <" + config_data.filename() + ">",
      "SimulationParameters::initialize_from_config_data" );
  }

  if( npsets < 1 )
    throw HypsiException(
      "Invalid number of particle sets: in file <" + config_data.filename() + ">",
      "SimulationParameters::initialize_from_config_data" );

  if( domain_cellsize_vec.size() != 3 )
    throw HypsiException(
      "domain/xyzcell requires three numbers: in file <" + config_data.filename() + ">",
      "SimulationParameters::initialize_from_config_data" );
  if( domain_cellsize_vec[0]<= 0 || domain_cellsize_vec[1]<= 0 
      || domain_cellsize_vec[2]<= 0  )
    throw HypsiException(
      "domain/xyzcell must be positive: in file <" + config_data.filename() + ">",
      "SimulationParameters::initialize_from_config_data" );

  dxcell = domain_cellsize_vec[0];
  dycell = domain_cellsize_vec[1];
  dzcell = domain_cellsize_vec[2];

  if( domain_size_vec.size() != 3 )
    throw HypsiException(
      "domain/xyzsize requires three numbers: in file <" + config_data.filename() + ">",
      "SimulationParameters::initialize_from_config_data" );
  if( domain_size_vec[0]<= 0 || domain_size_vec[1]<= 0
      || domain_size_vec[2]<= 0  )
    throw HypsiException(
      "domain/xyzsize must be positive: in file <" + config_data.filename() + ">",
      "SimulationParameters::initialize_from_config_data" );

  xdomain = domain_size_vec[0];
  ydomain = domain_size_vec[1];
  zdomain = domain_size_vec[2];

  domain_region.set( 0.0, domain_size_vec[0], 0.0, domain_size_vec[1],
                  0.0, domain_size_vec[2] );
  
  domain_region.print( cout );

  nxdomain = static_cast<int>( domain_size_vec[0] / dxcell );
  nydomain = static_cast<int>( domain_size_vec[1] / dycell );
  nzdomain = static_cast<int>( domain_size_vec[2] / dzcell );
  
  if( nxdomain != xdomain / dxcell 
      || nydomain != ydomain / dycell 
      || nzdomain != zdomain / dzcell )
    throw HypsiException(
      "Cell size and domain size must be commensurate: file <" + config_data.filename() + ">",
      "SimulationParameters::initialize_from_config_data" );

  if( cn_numxyz_vec.size() != 3 )
    throw HypsiException(
      "compute_nodes_xyz_geometry requires three numbers: file <" + config_data.filename() + ">",
      "SimulationParameters::initialize_from_config_data" );
  if( cn_numxyz_vec[0]<= 0 || cn_numxyz_vec[1]<= 0
      || cn_numxyz_vec[2]<= 0  )
    throw HypsiException(
      "compute_nodes_xyz_geometry must be positive: file <" + config_data.filename() + ">",
      "SimulationParameters::initialize_from_config_data" );

  nx_zones = static_cast<int>( cn_numxyz_vec[0] );
  ny_zones = static_cast<int>( cn_numxyz_vec[1] );
  nz_zones = static_cast<int>( cn_numxyz_vec[2] );

  if( cn_numxyz_vec[0] != nx_zones || cn_numxyz_vec[1] != ny_zones
      || cn_numxyz_vec[2] != nz_zones  )
    throw HypsiException(
      "compute_nodes_xyz_geometry must be integers: file <" + config_data.filename() + ">",
      "SimulationParameters::initialize_from_config_data" );

  if( nxdomain % nx_zones || nydomain % ny_zones || nxdomain % nz_zones  )
    throw HypsiException(
      "compute_nodes_xyz_geometry and domain must be commensurate: file <" + config_data.filename() + ">",
      "SimulationParameters::initialize_from_config_data" );

}  //  end SimulationParameters::initialize_from_config_data(


// ------------------------------------------------------------------------

void HypsimTest::initialize_zones_info( void )
{

  zones_info.resize( sim_params.n_zones );

 for( int i=0; i < sim_params.n_zones ; ++i )
 {

    zones_info[i].initialize( i, sim_params.n_zones, &domain_info );

//    zones_info[i].print( cout );

// zone info for this node
    if( i == mpi_comm.thisnode )
      zinfo = zones_info[i];
  }

   zinfo.print( cout, "This zone ... " );

} // end Hypsim::initial_zones_info


} // end namespace HYPSI
