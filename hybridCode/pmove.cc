
#include <iostream>
#include <string>

#include "hypsi.h"

namespace HYPSI {

using namespace std;


void HypsimPmoveMsgHandler::handle_incoming_messages(
       const string& dstr,
       int max_msg_count,
       PmoveZMoments* pset_pzmoms_ptr,
       ZoneFields* zfields_ptr )
{
  int mpierr;
  int mpi_iprobe_flag;
  MPI_Status mpi_iprobe_status;
  MPI_Status mpi_recv_status;

  int mpi_work_count = 0;

  n_times_called++;
  
  while( MPI_Iprobe( MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD,
		     &mpi_iprobe_flag, &mpi_iprobe_status ),
	( mpi_iprobe_flag && mpi_work_count < max_msg_count ) )
  {

    mpi_work_count++;

//   >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> test for arrival of particle zone moments

    MPI_Iprobe( MPI_ANY_SOURCE, TAG_PMOVE_ZMOMENTS,
                MPI_COMM_WORLD,
       	        &mpi_iprobe_flag, &mpi_iprobe_status );

    if( mpi_iprobe_flag )
    {

// receive zone moments from zone MPI_SOURCE for this hostzone
// add into zone moments for this hostzone and for current ipset
// and update status info
#ifdef PMOVE_MSGHANDLER_DIAG
cout << "Z" << sim_params.hostzone<< "PMOVE_MSGHANDLER_DIAG"
     << dstr <<" pzmoms recvd"
     << "from Z" << mpi_iprobe_status.MPI_SOURCE << endl;
#endif

      pzmoms->mpi_recv( mpi_iprobe_status.MPI_SOURCE,
			TAG_PMOVE_ZMOMENTS );

      pset_pzmoms_ptr->add( *pzmoms );

      pzmoms_recv_list[ mpi_iprobe_status.MPI_SOURCE ] = true;
      pzmoms_recv_n++;
      pzmoms_recv_complete = (pzmoms_recv_n == n_zones);

    }  // end of receive zone moments

//  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> test for arrival of zone fields request

    MPI_Iprobe( MPI_ANY_SOURCE, TAG_RQ_ZONEFIELDS,
		  MPI_COMM_WORLD,
		  &mpi_iprobe_flag, &mpi_iprobe_status );

    if( mpi_iprobe_flag )
    {

#ifdef PMOVE_MSGHANDLER_DIAG
cout << "Z" << sim_params.hostzone<< "PMOVE_MSGHANDLER_DIAG" << dstr
     <<" got zf request from Z" << mpi_iprobe_status.MPI_SOURCE << endl;
#endif
      mpierr = MPI_Recv( &mpidummy_rq_zonef_recv, 1, MPI_INT,
		 mpi_iprobe_status.MPI_SOURCE,
		 TAG_RQ_ZONEFIELDS,
		 MPI_COMM_WORLD,
		 &mpi_recv_status );

      if( mpierr != MPI_SUCCESS )
	throw HypsiMPIException(
	"MPI_Recv for TAG_RQ_ZONEFIELDS failed",
	"HypsimPmoveMsgHandler::handle_incoming_messages", mpierr);

      zfields_ptr->mpi_send( mpi_iprobe_status.MPI_SOURCE, TAG_ZONEFIELDS );

#ifdef PMOVE_MSGHANDLER_DIAG
cout << "Z" << sim_params.hostzone<< "PMOVE_MSGHANDLER_DIAG" << dstr
     <<" TAG_ZONEFIELDS Send REPLY completed\n";
#endif

      rq_zonef_answered_list[ mpi_iprobe_status.MPI_SOURCE ] = true;
      rq_zonef_answered_n++;

    } // end of if( iprobe( ...TAG_RQ_ZONEFIELDS .. )  )

// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> test for arrival of  NO PARTICLES IN ZONE

    MPI_Iprobe( MPI_ANY_SOURCE, TAG_NOPARTICLESINZONE,
		  MPI_COMM_WORLD,
		  &mpi_iprobe_flag, &mpi_iprobe_status );

    if( mpi_iprobe_flag )
    {

      mpierr = MPI_Recv( &mpidummy_noptcles_recv, 1, MPI_INT,
		 mpi_iprobe_status.MPI_SOURCE,
		 TAG_NOPARTICLESINZONE,
		 MPI_COMM_WORLD,
		 &mpi_recv_status );

      if( mpierr != MPI_SUCCESS )
	throw HypsiMPIException(
	  "MPI_Recv for TAG_NOPARTICLESINZONE failed",
	  "HypsimPmoveMsgHandler::handle_incoming_messages::pmove", mpierr);

// no particles in this(host) zone from zone  mpi_iprobe_status.MPI_SOURCE
// so act as if zonef request is answered AND pzmoms have been received

      rq_zonef_answered_list[ mpi_iprobe_status.MPI_SOURCE ] = true;
      rq_zonef_answered_n++;
      pzmoms_recv_list[ mpi_iprobe_status.MPI_SOURCE ] = true;
      pzmoms_recv_n++;
      pzmoms_recv_complete = (pzmoms_recv_n == n_zones);

    } // end of if( iprobe( ...TAG_NOPARTICLESINZONE .. )  )

  } // end of while( mpi probe and mpi_work_count < .. )

  if( mpi_work_count >= max_msg_count )
    n_times_max_reached++;
}


void Hypsim::pmove( double dt )
{
  int mpi_iprobe_flag, mpi_test_flag;
  MPI_Status mpi_iprobe_status, mpi_test_status;
  MPI_Status mpi_recv_status;
  int mpierr;

  bool rq_zonef_pending;

  int mpidummy_rq_zonf_send=123, mpidummy_rq_zonf_recv;

  MPI_Request pzmoms_send_rq;

  MPI_Request zfrequest_isend_rq;

  MPI_Request zf_recv_rq;

  PTTHR::State xstate;

  int trylock_count = 0;

 // create Particle Thread Action Functor & Initialize
// NB: some other quantities are initialized later, before thread work starts

  PTAF_pmove* ptaf_p = new PTAF_pmove( zinfo );
  
  ptaf_p->set_hostzone( sim_params.hostzone );

// set functor for dealing with particle boundary conditions
  
  ptaf_p->set_particle_bcfp( particle_bcf );
  
// create Particle Thread  and initialize state

  PTTHR ptthr( ptaf_p, sim_params.hostzone );

  ptthr.set_state( PTTHR::THR_STATE_NOT_READY );

  ptthr.lock_mutex();

#ifdef DIAG_PMOVE
   dmsg("PMOVE: pmove: about to thread create");
#endif

  ptthr.thread_create();

#ifdef DIAG_PMOVE
   dmsg("PMOVE: pmove: about to cond_wait");
#endif

  ptthr.cond_wait();
  
  if( ptthr._state != PTTHR::THR_STATE_READY )
    throw HypsiException(
            "State not THR_STATE_READY after thread create",
            "Hypsim::pmove" );

  ptthr.unlock_mutex();

#ifdef DIAG_PMOVE
   dmsg("PMOVE: pmove: mutex unlocked  ... state now READY to send requests to PTTHR");
#endif
#ifdef DIAG_PMOVE
dmsg("PMOVE: pmove: about to MPI barrier before ipset loop");
#endif  

  MPI_Barrier( MPI_COMM_WORLD );

// ------------------------------------------------------ start of ipset loop
  for( int ipset=0; ipset < sim_params.npsets; ++ipset )
  {

// zero the pmove zone moments for this particle set

    pset_pzmoms[ipset]->set_zero();

// initializations for incoming message handler

    pmove_msg_handler->initialize();

#ifdef DIAG_PMOVE
dmsg("PMOVE: start of ipset loop: about to MPI barrier");
#endif  

    MPI_Barrier( MPI_COMM_WORLD );

// --------------------------------------------- start of particle zone loop
    for( int izc=0; izc < sim_params.n_zones; ++izc )
    {
      int iz = (izc + sim_params.hostzone) % sim_params.n_zones;

#ifdef DIAG_PMOVE
cout<< "Z" << sim_params.hostzone
    << ": PMOVE: ipset, iz  zone: "<< ipset << ", " << iz << endl;
#endif

//   >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> pmove message handler

      pmove_msg_handler->handle_incoming_messages(
        "PMOVE_STAGE1", 25, pset_pzmoms[ipset], zfields );

//   <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

// if there are no particles for this pset in this zone ....
//   send TAG_NOPARTICLESINZONE
//   ... and that's all!

      if( psets[ ipset ]->zoneidx_list[ iz ].num == 0 )
      {

/*
cout<< "Z" << sim_params.hostzone
    << ": PMOVE NO PARTICLES: ipset, iz: "<< ipset << ", " << iz << endl;
*/
        mpierr = MPI_Issend(
                   pmove_msg_handler->mpidummy_noptcles_send+iz, 1, MPI_INT,
                   iz, TAG_NOPARTICLESINZONE,
                   MPI_COMM_WORLD, pmove_msg_handler->noptcls_isend_rq+iz );

        if( mpierr != MPI_SUCCESS )
          throw HypsiMPIException(
            "MPI_Isend TAG_NOPARTICLESINZONE failed", "Hypsim::pmove", mpierr);

        pmove_msg_handler->noptcls_isend_pending[ iz ] = true;

        continue;   // ie skip rest of loop

      }

// send zone fields request

      if( iz != sim_params.hostzone )
      {
        rq_zonef_pending = true;

// open a pending receive ...

        ptaf_p->_zfields->mpi_irecv( iz, TAG_ZONEFIELDS, &zf_recv_rq );

// send request for zone fields    

        mpierr = MPI_Isend( &mpidummy_rq_zonf_send, 1, MPI_INT,
                   iz, TAG_RQ_ZONEFIELDS,
                   MPI_COMM_WORLD, &zfrequest_isend_rq );

        if( mpierr != MPI_SUCCESS )
          throw HypsiMPIException(
            "MPI_Isend TAG_RQ_ZONEFIELDS failed", "Hypsim::pmove", mpierr);

#ifdef DIAG_PMOVE
cout << "Z" << sim_params.hostzone
     << ": PMOVE_SOZLOOP TAG_RQ_ZONEFIELDS MPI_Isend to Z" << iz << endl;
#endif
      } else
      {
// this zone's fields, and set request satisfied 

// copy this zone's zone fields and set zone info

        ptaf_p->_zfields->copy_EBdata( *zfields );
        ptaf_p->_zfields->set_zoneinfo( zinfo );

        rq_zonef_pending = false;

        pmove_msg_handler->rq_zonef_answered_list[ iz ] = true;
        pmove_msg_handler->rq_zonef_answered_n++;
      }

// ----------------------------------------------- end of zone fields request

      while( rq_zonef_pending )
      {

// sleep awhile
//        ptthr.ssleep( 0.01 );

// test for completion of zone fields receive

        MPI_Test( &zf_recv_rq, &mpi_test_flag, &mpi_test_status );

        if( mpi_test_flag )
        {

          rq_zonef_pending = false;

// set zone info in zone fields for the zone currently being worked
          ptaf_p->_zfields->set_zoneinfo( zones_info[ iz ] );

#ifdef DIAG_PMOVE
cout << "Z" << sim_params.hostzone
     << " PMOVE_STAGE1: TAG_ZONEFIELDS irecv completed [stage 1], from: Z"
     << mpi_iprobe_status.MPI_SOURCE << endl;
#endif
        }

//   >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> pmove message handler

        pmove_msg_handler->handle_incoming_messages(
          "PMOVE_STAGE1", 25, pset_pzmoms[ipset], zfields );

//   <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

      } // end of while( rq_zonef_pending )

#ifdef DIAG_PMOVE
      dmsg("PMOVE_STAGE1: end of while( rq_zonef_pending )");
#endif

// --------------------------------------- end of zone fields request pending

// --------------------------------------- Start of particle work phase

      trylock_count = 0;

      while( true )
      {

      ptthr.ssleep( 0.01 );

      if( ptthr.trylock_mutex() )
      {

        xstate = ptthr.state();

#ifdef DIAG_PMOVE
cout << "Z" << sim_params.hostzone
     << " PMOVE: trylock successful:  state: "
     << ptthr.state() << endl;
#endif
        if( xstate == PTTHR::THR_STATE_START_WORK 
            || xstate == PTTHR::THR_STATE_REQUEST_EXIT )
        {

#ifdef DIAG_PMOVE
cout << "Z" << sim_params.hostzone
     << " PMOVE: trylock ok: INCOMPLETE cond_signal: eg THR_STATE_START_WORK state:"
     << xstate << endl;
#endif
          ptthr.unlock_mutex();

        } else if( xstate == PTTHR::THR_STATE_WORKING )
        {
          trylock_count++;
          ptthr.unlock_mutex();

        } else if( xstate == PTTHR::THR_STATE_WORK_DONE )
        {
        
#ifdef DIAG_PMOVE
dmsg("PMOVE: trylock successful: RECEIVED THR_STATE_WORK_DONE");
cout << "Z" << sim_params.hostzone
     << ": PMOVE: trylock count: " << trylock_count << endl;
#endif
          trylock_count = 0;

          ptthr.set_state( PTTHR::THR_STATE_READY );
          ptthr.unlock_mutex();
          break;

        } else if( xstate == PTTHR::THR_STATE_READY )
        {

// set up data for particle thread work, set working zone number
// set pointer to particle set for working, and time step

#ifdef DIAG_PMOVE
dmsg("PMOVE: trylock successful: ABOUT to set THR_STATE_START_WORK");
#endif
          ptaf_p->set_zone( iz );
          ptaf_p->set_psetp_dt( psets[ ipset ], dt );
          ptthr.set_state( PTTHR::THR_STATE_START_WORK );

#ifdef DIAG_PMOVE
dmsg("PMOVE: trylock successful: ABOUT to cond_signal");
#endif
          ptthr.cond_signal();

#ifdef DIAG_PMOVE
cout << "Z" << sim_params.hostzone
     << " PMOVE: RETURNED from cond_signal, about to unlock: state: "
     << ptthr.state() << endl;
#endif

          ptthr.unlock_mutex();

        }

      } // -----------------------------------------  end of if( trylock() )
      

// ++++++++++++++++++++++++++++++++++++++++++++ MPI work during thread work

// Main thread work while waiting for particle thread to complete its work

//   >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> pmove message handler

      pmove_msg_handler->handle_incoming_messages(
        "PMOVE_STAGE1", 25, pset_pzmoms[ipset], zfields );

//   <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


// --------------------------------------------------------------------------

      } // ------------------  end of while(true) loop (particle thread work)

// ----------------------------------------- pmove zone moments send to owner

// if the current zone is the host zone, then just access the
//   particle zone moments directly (since on same node)

      if( iz == sim_params.hostzone )
      {
        pset_pzmoms[ipset]->add( *ptaf_p->_pzmoms );

        pmove_msg_handler->pzmoms_recv_list[ iz ] = true;
        pmove_msg_handler->pzmoms_recv_n++;
        pmove_msg_handler->pzmoms_recv_complete = (pmove_msg_handler->pzmoms_recv_n == sim_params.n_zones);
    
      } else
      {

#ifdef DIAG_PMOVE
cout << "Z" << sim_params.hostzone << " PMOVE: about to isend pzmoms"
     << "to zone: " << iz << endl;
#endif

        ptaf_p->_pzmoms->mpi_isend( iz, TAG_PMOVE_ZMOMENTS, &pzmoms_send_rq );
      
#ifdef DIAG_PMOVE
cout << "Z" << sim_params.hostzone
     << " PMOVE: start MPI_Test loop on isend pzmoms [stage 2a] to Z"
     << iz << endl;
#endif

// ----------------------------------------- wait for pzmoms send to complete

        while( MPI_Test( &pzmoms_send_rq, &mpi_test_flag, &mpi_test_status ),
               !mpi_test_flag )
        {

//          ptthr.ssleep( 0.01 );

//   >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> pmove message handler

          pmove_msg_handler->handle_incoming_messages(
            "PMOVE_STAGE1", 25, pset_pzmoms[ipset], zfields );

//   <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

        
        }  // end of while( Test(  pzmoms_send_rq ) )
 
#ifdef DIAG_PMOVE
cout << "Z" << sim_params.hostzone
     << " PMOVE: COMPLETED MPI_Test loop  [stage 2a] to Z" << iz << endl;
#endif
      }


    } // end of for( iz ... ) loop

// --------------------------------------------------- end of loop over zones

#ifdef DIAG_PMOVE
cout << "Z" << sim_params.hostzone << " PMOVE: end of iz zone loop"
     << ": rq_zonef_answered_n: " << rq_zonef_answered_n
     << " pzmoms_recv_n: " << pzmoms_recv_n << endl;
for( int i=0; i < sim_params.n_zones; i++ )
  if( !rq_zonef_answered_list[ i ] )
    cout << "Z" << sim_params.hostzone
         << " rq_zonef waiting for zone: " << i << endl;
for( int i=0; i < sim_params.n_zones; i++ )
  if( !pzmoms_recv_list[ i ] )
    cout << "Z" << sim_params.hostzone
         << " pzmoms_recv waiting for zone: " << i << endl;
#endif
    
// --------------------------------------------- post zone loop waiting work

   pmove_msg_handler->noptcls_isend_all_complete = false;

    int n_pzloop = 0;

    while( pmove_msg_handler->rq_zonef_answered_n != sim_params.n_zones 
             || pmove_msg_handler->pzmoms_recv_n != sim_params.n_zones 
             || !pmove_msg_handler->noptcls_isend_all_complete )
    {

      ptthr.ssleep( 0.01 );

      n_pzloop++;

      if( !pmove_msg_handler->noptcls_isend_all_complete )
      {
        int n_pending = 0;
        for(int iz=0; iz<sim_params.n_zones; iz++)
        {
          if( pmove_msg_handler->noptcls_isend_pending[ iz ] )
          {
            MPI_Test(  pmove_msg_handler->noptcls_isend_rq+iz, &mpi_test_flag, &mpi_test_status);
            if( mpi_test_flag )
              pmove_msg_handler->noptcls_isend_pending[ iz ] = false;
            else
              n_pending++;
          }
        }
        pmove_msg_handler->noptcls_isend_all_complete = ( n_pending == 0 );

/*
if( ( n_pending > 0 && n_pzloop < 5 ) ||
    ( n_pending > 0 && n_pzloop >10 && n_pzloop%50 == 0) )
  cout << "Z" << sim_params.hostzone
       <<" PMOVE: noptcls_isend_all_complete n_pending: "<< n_pending
       <<" (n_pzloop: " << n_pzloop << ")\n";
*/

      }

//   >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> pmove message handler

      pmove_msg_handler->handle_incoming_messages(
        "PMOVE_STAGE1", 20, pset_pzmoms[ipset], zfields );

//   <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    } // end of while( rq_zonef_answered_n != sim_params.n_zones )

// ----------------------------------------------- END OF post zone loop work

#ifdef DIAG_PMOVE
cout << "Z" << sim_params.hostzone
     << ": end of EXTRA while loop rq_zonef_answered_n: " << rq_zonef_answered_n << endl;
#endif


  } // end of ipset loop

#ifdef DIAG_PMOVE
dmsg("PMOVE: exited from ipset, iz zone loops, about to MPI barrier");
#endif
  
  MPI_Barrier( MPI_COMM_WORLD );
#ifdef DIAG_PMOVE
dmsg("PMOVE: after MPI barrier, Now THREAD .. REQUEST_EXIT");
#endif

  ptthr.lock_mutex();

  ptthr.set_state( PTTHR::THR_STATE_REQUEST_EXIT );

  ptthr.cond_broadcast( );

  ptthr.unlock_mutex();

#ifdef DIAG_PMOVE
dmsg( "PMOVE:  about to thr_join " );
#endif

  ptthr.ssleep(0.01);

  ptthr.thread_join();

// clean up particle thread action functor
  delete ptaf_p;

  /*
if( sim_params.hostzone < 16 )
  cout << "Z" << sim_params.hostzone
     <<" PMOVE: msg_handler num calls: "<< pmove_msg_handler->n_times_called
     <<" times max'd: " << pmove_msg_handler->n_times_max_reached << "\n";
  */

} // end Hypsim::test_pmove2

}  // end namespace HYPSI
