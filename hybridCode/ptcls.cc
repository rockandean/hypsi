
#include <cmath>

#include "zonearray.h"
#include "ptcls.h"

using namespace HYPSI;

namespace HYPSI {

/*! \brief Functor for uniform spatial distribution
*/
void xyzUniformRandomIF::setxyz( double* xyzp )
{
//  cout << "xyzUniformRandomIF::setxyz: "  << "\n";
  double x, y, z;
  
  x = _xyz_region.x0 + (*_rng_p)() * _xyz_region.xlen();
  y = _xyz_region.y0 + (*_rng_p)() * _xyz_region.ylen();
  z = _xyz_region.z0 + (*_rng_p)() * _xyz_region.zlen();

// cout << "xyz: " << x << ", " << y <<", "<<z << "\n";

  *xyzp++ = x;
  *xyzp++ = y;
  *xyzp++ = z;
}

/*! \brief Functor for  bimaxwellian distribution
*/
void vxyzBimaxIF::setxyz( double* xyzp )
{
//     cout << "vxyzBimaxIF::setxyz: "  << "\n";

  static int count=0;

  double vx, vy, vz, vvx, vvy, vvz;
  double sqrt2 = sqrt(2.0);
  double twopi = 2.0 * 3.14159265358979323846;

// NOTE: the sqrt2 factor multiplying _vthperp and _vthpar
// depends on the definition of the thermal speed.
// without the sqrt2 the definition is as Stix etc. - THIS IS WHAT IS
// USED IN HYPSI
// with sqrt2 it is the same as Gary.

//  double sqrt2 = sqrt(2.0);

// calculate bi-max assuming that parallel direction is x-axis
// random perpendicular velocity (+ve and -ve)

// vth perp

// Gary definition of thermal speed
//  double vmag  = sqrt2 * _vthperp 
//                 * sqrt(-log( 1.0-0.99999999999* (*_rng_p)() ));

  double vmag  = _vthperp * sqrt(-log( 1.0-0.99999999999* (*_rng_p)() ));

// distribute velocity uniformly in gyrophase
  double angle = twopi * (*_rng_p)();

  vvy   = vmag * cos(angle);
  vvz   = vmag * sin(angle);

// vth parallel
  vmag  = sqrt(-log( 1.0-0.99999999999* (*_rng_p)() ));
  angle = twopi * (*_rng_p)();

// Gary definition of thermal speed
//  vvx   = sqrt2 * _vthpar * vmag * cos(angle);

  vvx   = _vthpar * vmag * cos(angle);

// add on the bulk parallel velocity
  vvx = vvx + _vpar_shift;

// rotate so that parallel direction is as specified by Bvec
  vx = rot[0][0] * vvx + rot[0][1] * vvy + rot[0][2] * vvz;
  vy = rot[1][0] * vvx + rot[1][1] * vvy + rot[1][2] * vvz;
  vz = rot[2][0] * vvx + rot[2][1] * vvy + rot[2][2] * vvz;

// finally add on vx-vy-vz shift
  vx += _vxyz_shift._x;
  vy += _vxyz_shift._y;
  vz += _vxyz_shift._z;

// copy into supplied target
  *xyzp++ = vx;
  *xyzp++ = vy;
  *xyzp++ = vz;

/*  if( count < 10 )
  {
//    cout << "vxyzBimaxIF::setxyz: xyzp: " << xyzp << "\n";
//    cout << "vxyzBimaxIF::setxyz: _vthpar, _vthperp: " << _vthpar<<", "<<_vthperp<<"\n";
    cout << "vxyzBimaxIF::setxyz: vvx,vvy,vvz: " << vvx<<", "<<vvy<<", "<<vvz<<"\n";
    cout << "vxyzBimaxIF::setxyz: _vxyz_shift: " << _vxyz_shift._x<<", "<<_vxyz_shift._y<<", "<<_vxyz_shift._z<<"\n";
    cout << "vxyzBimaxIF::setxyz: vx,vy,vz: " << vx<<", "<<vy<<", "<<vz<<"\n";
  }
  count++;
*/
}

/*! \brief Functor for  Maxwellian distribution
*/
void vxyzMaxweIF::setxyz( double* xyzp )
{
  //   cout << "vxyzMaxweIF::setxyz: "  << "\n";

  static int count=0;

  double vx, vy, vz, vvx, vvy, vvz;
  double sqrt2 = sqrt(2.0);
  double twopi = 2.0 * 3.14159265358979323846;

// NOTE: the sqrt2 factor multiplying _vthperp and _vthpar
// depends on the definition of the thermal speed.
// without the sqrt2 the definition is as Stix etc. - THIS IS WHAT IS
// USED IN HYPSI
// with sqrt2 it is the same as Gary.

//  double sqrt2 = sqrt(2.0);

// calculate bi-max assuming that parallel direction is x-axis
// random perpendicular velocity (+ve and -ve)

// vth perp

// Gary definition of thermal speed
//  double vmag  = sqrt2 * _vthperp
//                 * sqrt(-log( 1.0-0.99999999999* (*_rng_p)() ));

  double vmag  = _vthermal * sqrt(-log( 1.0-0.99999999999* (*_rng_p)() ));

// distribute velocity uniformly in gyrophase
  double angle = twopi * (*_rng_p)();

  vvy   = vmag * cos(angle);
  vvz   = vmag * sin(angle);
//
  double vmag2  = _vthermal * sqrt(-log( 1.0-0.99999999999* (*_rng_p)() ));
  double angle2 = twopi * (*_rng_p)();
  vvx   = vmag2 * cos(angle2);

// rotate so that parallel direction is as specified by Bvec
  vx = rot[0][0] * vvx + rot[0][1] * vvy + rot[0][2] * vvz;
  vy = rot[1][0] * vvx + rot[1][1] * vvy + rot[1][2] * vvz;
  vz = rot[2][0] * vvx + rot[2][1] * vvy + rot[2][2] * vvz;

// finally add on vx-vy-vz shift
  vx += _vxyz_shift._x;
  vy += _vxyz_shift._y;
  vz += _vxyz_shift._z;

// copy into supplied target
  *xyzp++ = vx;
  *xyzp++ = vy;
  *xyzp++ = vz;

  /*if( count < 10 )
  {
    cout << "vxyzMaxweIF::setxyz: xyzp: " << xyzp << "\n";
    cout << "vxyzMaxweIF::setxyz: _vthermal: " << _vthermal <<"\n";
    cout << "vxyzMaxweIF::setxyz: vvx,vvy,vvz: " << vvx<<", "<<vvy<<", "<<vvz<<"\n";
    cout << "vxyzMaxweIF::setxyz: _vxyz_shift: " << _vxyz_shift._x<<", "<<_vxyz_shift._y<<", "<<_vxyz_shift._z<<"\n";
    cout << "vxyzMaxweIF::setxyz: vx,vy,vz: " << vx<<", "<<vy<<", "<<vz<<"\n";
  }
  count++;
*/
}

/*! \brief add particles to particle set
*/
void ParticleSet::add( int npreq, xyzInitFunctor& xyz_initf,
                       xyzInitFunctor& vxyz_initf, ZonePtclIdx& pidx_range )
{
/*
  cout << "ParticleSet::add: " << npreq << "\n";
  cout << "currently: np = " << np << "\n";
  cout << "currently: nalloc = " << nalloc << "\n";

  cout << "Velocity initialization: " << vxyz_initf.type() << "\n";
  cout << "Spatial initialization: " << xyz_initf.type() << endl;
*/


  if( (np + npreq) > nalloc )
    throw  HypsiException(
      "Insufficient space for new particles in ParticleSet <<" + pset_id + ">>",
      "ParticleSet::add" );

  for( int i=0; i<npreq; ++i )
  {
    xyz_initf.setxyz( pdata+(np+i)*6 );
    vxyz_initf.setxyz( pdata+(np+i)*6 + 3 );
    pzone[ np+i ] = 0;
  }

//  for( int i=0; i<5; ++i )
//  {
//    double* pp = pdata + i*6;
//    cout << "pdata["<<i<<"]: " << pp[0] << ", "<< pp[1] << ", "<< pp[2] << ", "
//    << pp[3] << ", "<< pp[4] << ", "<< pp[5] << "\n";
//  }
  
  pidx_range.first = np;
  pidx_range.last = np + npreq - 1;
  np = np + npreq;
}

void ParticleSet::calc_stats( void )
{
  stats.np = np;
  stats.v_min = 10000000.0;
  stats.v_max = 0.0;
  stats.vx_min = 10000000.0;
  stats.vx_max = -10000000.0;
  stats.vy_min = 10000000.0;
  stats.vy_max = -10000000.0;
  stats.vz_min = 10000000.0;
  stats.vz_max = -10000000.0;
  stats.vsq_sum = 0.0;
  stats.v_sum = 0.0;
  stats.vx_sum = 0.0;
  stats.vy_sum = 0.0;
  stats.vz_sum = 0.0;

  for( int ip=0; ip < np ; ++ip )
  {
    double* pvel = pdata + ip*6 + 3;  // ptr to particle velocity components
    double v;

    v = sqrt( pvel[0]* pvel[0] + pvel[1]* pvel[1] + pvel[2]* pvel[2] );

    if( v < stats.v_min ) stats.v_min = v;
    if( v > stats.v_max ) stats.v_max = v;
    if( pvel[0] < stats.vx_min ) stats.vx_min = pvel[0];
    if( pvel[0] > stats.vx_max ) stats.vx_max = pvel[0];
    if( pvel[1] < stats.vy_min ) stats.vy_min = pvel[1];
    if( pvel[1] > stats.vy_max ) stats.vy_max = pvel[1];
    if( pvel[2] < stats.vz_min ) stats.vz_min = pvel[2];
    if( pvel[2] > stats.vz_max ) stats.vz_max = pvel[2];

    stats.vsq_sum += v * v;
    stats.v_sum += v;
    stats.vx_sum += pvel[0];
    stats.vy_sum += pvel[1];
    stats.vz_sum += pvel[2];
  }

  stats.v_mean = stats.v_sum / np;
  stats.vx_mean = stats.vx_sum / np;
  stats.vy_mean = stats.vy_sum / np;
  stats.vz_mean = stats.vz_sum / np;

}


} // end namespace HYPSI
