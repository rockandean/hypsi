#include <iostream>

using namespace std;

main()
{
  cout << " INTEGER TYPES " << endl;
  cout << "================================" << endl;
  cout << " sizeof( char ):           " << sizeof(char) << endl;
  cout << " sizeof( unsigned char ):  " << sizeof(unsigned char) << endl;
  
  cout << " sizeof( short ):          " << sizeof(short) << endl;
  cout << " sizeof( unsigned short ): " << sizeof(unsigned short) << endl;
  
  cout << " sizeof( long ):           " << sizeof(long) << endl;
  cout << " sizeof( unsigned long ):  " << sizeof(unsigned long) << endl;
  
  cout << " sizeof( int ):            " << sizeof(int) << endl;
  cout << " sizeof( unsigned int ):   " << sizeof(unsigned int) << endl;
  
  cout << " sizeof( short int ):      " << sizeof(short int) << endl;
  cout << " sizeof( unsigned short int ): " << sizeof( unsigned short int) << endl;
  cout << " sizeof( long int ):       " << sizeof(long int) << endl;
  cout << " sizeof( unsigned long int ): " << sizeof( unsigned long int) << endl;
  cout << " sizeof( long long ):      " << sizeof(long long) << endl;
  cout << " sizeof( unsigned long long ): " << sizeof( unsigned long long) << endl;
  cout << " sizeof( long long int):   " << sizeof(long long int) << endl;
  cout << " sizeof( unsigned long long int ): " << sizeof( unsigned long long int) << endl;

  cout << " FLOATING POINT TYPES " << endl;
  cout << "================================" << endl;
  cout << " sizeof( float ):       " << sizeof(float) << endl;
  cout << " sizeof( double ):      " << sizeof(double) << endl;
  cout << " sizeof( long double ): " << sizeof(long double) << endl;
  cout << " sizeof( long long double ): " << sizeof(long long double) << endl;

}
