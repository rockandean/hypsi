
//! \file hdf5adaptor.h
/*!
* \brief Base exception class
*/

#ifndef _HDF5_ADAPTOR_H_
#define _HDF5_ADAPTOR_H_

#include "output.h"
#include <algorithm>

#ifndef C_API
#include "hdf5.h"
#include "H5LTpublic.h" // changed to H5LTpublic?
#endif

#ifdef CPP_API
#include "H5Cpp.h"
#include <H5IdComponent.h>
using namespace std;
#endif

namespace HYPSI {

#ifdef CPP_API
using namespace H5;
using namespace std;
#endif

class HDF5OutputAdaptor : public OutputAdaptor {

#ifndef C_API
  std::string _hdf5_file_name;
  hid_t  _hdf5_file_id;
  
  static std::string purify_object_name( const std::string& objname );
  static void split_name( const std::string& name, 
                   std::vector<std::string>& elements );

  void get_dataset_context( const std::string& name,
                            std::vector<hid_t>& hid_array,
                            std::string& dataset_name );
#endif
#ifdef CPP_API
  H5std_string _hdf5_file_name;
  hid_t  _hdf5_file_id;
/*!
hid_t
This variable comes from HDF5 and is a type for managing references to nodes. Each node reference is represented as an integer and hid_t keeps track of them.
  */
  static string purify_object_name( const string& objname );

  static void split_name( const string& name,
                          vector<string>& elements );

  void get_dataset_context( const string& name,
                            vector<hid_t>& hid_array,
                            string& dataset_name );
#endif
public:
  HDF5OutputAdaptor(void) {;}
 
  void open( const std::string& name );
  void close( void );
  void flush( void );

  void write( const std::string& tag, int i_value );
  void write( const std::string& tag,
              const Dimens dimens, const int* i_array );
  void write( const std::string& tag,
	      const Dimens dimens,const long* i_array );
  
  void write( const std::string& tag,
              const Dimens dimens, const std::vector<int>& i_array );

  void write( const std::string& tag,
	      const Dimens dimens, const std::vector<long>& i_array );
  
  void write( const std::string& objname,
                        const Dimens dimens,
                        const int*** i_array );


// write float functions
  void write( const std::string& objname, float f );
  void write( const std::string& objname,
                        const Dimens dimens,
                        const float* f_array );
  void write( const std::string& objname,
                        const Dimens dimens,
                        const std::vector<float>& f_array );
  void write( const std::string& objname,
                        const Dimens dimens,
                        const float*** f_array );

// write double functions
  void write( const std::string& objname, double d );
  void write( const std::string& objname,
              const Dimens& dimens,
              const double* d_array );
  void write( const std::string& objname,
              const Dimens& dimens,
              const std::vector<double>& d_array );
  void write( const std::string& objname,
                        const Dimens dimens,
                        double*** d_array );
  void write( const std::string& objname,
	      const Dimens dimens,const int i,
	      double**** d_array );

  void write( const std::string& objname,
	      const Dimens dimens,
	      double** d_array );

  void write( const std::string& objname,
	      const Dimens dimens,const int i,
	      double*** d_array );

// write string functions
  void write( const std::string& objname, const std::string& s);

};


void HDF5OutputAdaptor::get_dataset_context( const std::string& name,
  std::vector<hid_t>& hid_array,
  std::string& dataset_name )
{
  hid_array.clear();

  std::vector<std::string> name_components;

  split_name( name, name_components );

//  for( int i=0; i< name_components.size() ; ++i )
//    std::cout<< i << ": <" << name_components[i] << ">\n";
  
  int ncompx = name_components.size();
  
  hid_array.resize( ncompx );
  
  if( ncompx == 0 )
  {
    throw HYPSI::OutputException(
      "HDF5OutputAdaptor::get_dataset_context()>> zero name components");

  } else if( ncompx == 1 )
  {
    hid_array[0] = _hdf5_file_id ;
    dataset_name = name_components[0];
  } else
  {
/* HDF5 error handling 
   code from http://hdf.ncsa.uiuc.edu/HDF5/doc/Errors.html */
/* Save old error handler */
    herr_t (*old_func)(void*);
    void *old_client_data;
    H5Eget_auto1(&old_func, &old_client_data);

    hid_array[0] = _hdf5_file_id ;

    for( int i=0; i < ncompx-1; ++i )
    {

//std::cout << "group open/create " << i << ": <" << name_components[i] << ">\n";

      dataset_name = name_components[ncompx-1];

/* Turn off error handling */
      H5Eset_auto1(NULL, NULL);

      hid_array[i+1] = H5Gopen1( hid_array[i], name_components[i].c_str() );

/* Restore previous error handler */
      H5Eset_auto1(old_func, old_client_data);

      if( hid_array[i+1] < 0 )
      {

//std::cout << "group open failed \n" ;
    
        hid_array[i+1] = H5Gcreate1( hid_array[i], name_components[i].c_str(), 0 );

        if( hid_array[i+1] < 0 )
        {

//std::cout << "group create failed \n" ;
        
          throw HYPSI::OutputException(
            "Failed to open/create group for <"
              +name+"> at element <"+name_components[i]+">",
            "HDF5OutputAdaptor::get_dataset_context()");
        }

      }

    }
  } // end more than one name component
  
}


/*

abort if zero length

add leading "/" if doesn't start with "/"

*/
std::string HDF5OutputAdaptor::purify_object_name( const std::string& objname )
{
  if( objname.length()==0 )
    throw HYPSI::OutputException(
      "Zero length tag name", "HDF5OutputAdaptor::purify_object_name()" );

  return objname[0]!='/' ? "/"+objname : objname ;  

}

/*
split name into elements

*/
void HDF5OutputAdaptor::split_name( const std::string& name, 
                                    std::vector<std::string>& elements )
{

  elements.clear();
  
  int endidx = name.length()-1;
  int startidx;
  
  do 
  {
    startidx = name.rfind("/",endidx );
//    std::cout<< startidx << " " << endidx << "\n";
    elements.push_back(  name.substr( startidx+1, endidx - startidx ) );
    endidx = startidx - 1; }
  while( startidx > 0 );

  reverse( elements.begin(), elements.end() );

}

/*

*/
void HDF5OutputAdaptor::open( const std::string& name )
{

// use H5F_ACC_TRUNC to delete any existing file
// or ACC_EXCL to fail if file exists
  _hdf5_file_id = H5Fcreate( name.c_str(), H5F_ACC_TRUNC, H5P_DEFAULT,
                             H5P_DEFAULT );

  if( _hdf5_file_id <= 0 )
  {
    HYPSI::OutputException e( "H5FCreate fails", "HDF5OutputAdaptor::open()" );

// if using H5F_ACC_EXCL
//    e.push_err_msg( "Using H5F_ACC_EXCL: Check if file " +name + " already exists" );

    throw e;
  }
  
  _hdf5_file_name = name; 

}



/*

*/
void HDF5OutputAdaptor::close( void )
{

  herr_t hdf5err = H5Fclose( _hdf5_file_id );
  
  if( hdf5err < 0 )
  {
    HYPSI::OutputException e( "HDF5OutputAdaptor::close()>> H5Fclose fails");
    throw e;
  }
  
  _hdf5_file_name.clear(); 
  _hdf5_file_id = 0;

}

/*

*/
void HDF5OutputAdaptor::flush( void )
{

  herr_t hdf5err = H5Fflush( _hdf5_file_id, H5F_SCOPE_GLOBAL );
  
  if( hdf5err < 0 )
  {
    HYPSI::OutputException e( "HDF5OutputAdaptor::flush()>> H5Fflush fails");
    throw e;
  }

}



/*


*/
void HDF5OutputAdaptor::write( const std::string& tag, int i_value )
{
try
{
  std::string ptag = purify_object_name( tag );
  
  std::vector<hid_t> hid_array;
  std::string dataset_name;

  get_dataset_context( tag, hid_array, dataset_name );
  
  hsize_t* hdf5dims = new hsize_t[1];
  hdf5dims[0] = 1;
  
  herr_t hdf5err = H5LTmake_dataset_int( hid_array[hid_array.size()-1],
                                         dataset_name.c_str(),
                                         1, hdf5dims, &i_value );

  if( hdf5err < 0 )
  {
    HYPSI::OutputException e(
      "make_dataset fails for "+tag,
      "HDF5OutputAdaptor::write(int)");
    throw e;
  }

// close groups, if any, but don't try to close the file id at [0]
  for( int i = hid_array.size()-1; i>0; --i )
    hdf5err = H5Gclose( hid_array[i] );
  
  delete[] hdf5dims;

} catch( HYPSI::HypsiException& e )
  {
     e.push_err_msg( "In HDF5OutputAdaptor::write(int)" );
     throw e;
  }
}

void HDF5OutputAdaptor::write( const std::string& tag,
              const Dimens dimens, const int* i_array )
{
try
{
  if( dimens.size() == 0 )
  {
    HYPSI::OutputException e(
     "Zero Dimens size",
     "HDF5OutputAdaptor::write(int* array)" );
    throw e;
  }
  
  std::string ptag = purify_object_name( tag );  
  
  std::vector<hid_t> hid_array;
  std::string dataset_name;

  get_dataset_context( tag, hid_array, dataset_name );
  
  hsize_t* hdf5dims = new hsize_t[dimens.size()];
  for( int i=0; i<dimens.size(); ++i )
    hdf5dims[i] = dimens[i];
 
  herr_t hdf5err = H5LTmake_dataset_int( hid_array[hid_array.size()-1],
                                         dataset_name.c_str(),
                                         dimens.size(), hdf5dims, i_array );

  if( hdf5err < 0 )
  {
    HYPSI::OutputException e(
      "make_dataset fails for "+tag,
      "HDF5OutputAdaptor::write(int* array)");
    throw e;
  }

// delete space associated with hdf5dims used in make_dataset
  delete[] hdf5dims;

// close groups, if any, but don't try to close the file id at [0]
  for( int i = hid_array.size()-1; i>0; --i )
    hdf5err = H5Gclose( hid_array[i] );

} catch( HYPSI::HypsiException& e )
  {
     e.push_err_msg( "In HDF5OutputAdaptor::write(int* array)" );
     throw e;
  }
}

void HDF5OutputAdaptor::write( const std::string& tag,
			       const Dimens dimens, const long* i_array) 
{
	try
	{
		if( dimens.size() == 0 )
		{
			HYPSI::OutputException e(
					"Zero Dimens size",
			"HDF5OutputAdaptor::write(long* array)" );
			throw e;
		}
  
		std::string ptag = purify_object_name( tag );  
  
		std::vector<hid_t> hid_array;
		std::string dataset_name;

		get_dataset_context( tag, hid_array, dataset_name );
  
		hsize_t* hdf5dims = new hsize_t[dimens.size()];
		for( int i=0; i<dimens.size(); ++i )
			hdf5dims[i] = dimens[i];
 
		herr_t hdf5err = H5LTmake_dataset_long( hid_array[hid_array.size()-1],
				dataset_name.c_str(),
				dimens.size(), hdf5dims, i_array );

		if( hdf5err < 0 )
		{
			HYPSI::OutputException e(
					"make_dataset fails for "+tag,
			"HDF5OutputAdaptor::write(long* array)");
			throw e;
		}

// close groups, if any, but don't try to close the file id at [0]
		for( int i = hid_array.size()-1; i>0; --i )
			hdf5err = H5Gclose( hid_array[i] );

// delete space associated with hdf5dims used in make_dataset
  delete[] hdf5dims;


	} catch( HYPSI::HypsiException& e )
	{
		e.push_err_msg( "In HDF5OutputAdaptor::write(long* array)" );
		throw e;
	}
}

void HDF5OutputAdaptor::write( const std::string& tag,
            const Dimens dimens, const std::vector<int>& i_array )
{
try
{
  int n = dimens.nels();
  int* i_array_p = new int[n];
  for(int i=0; i<n; ++i )
    i_array_p[i] = i_array[i];
  write( tag, dimens, i_array_p );
  delete[] i_array_p;
} catch( HYPSI::HypsiException& e )
  {
     e.push_err_msg( "In HDF5OutputAdaptor::write(vector<int> array)" );
     throw e;
  }
}

void HDF5OutputAdaptor::write( const std::string& tag,
			       const Dimens dimens, const std::vector<long>& i_array )
{
	try
	{
		int n = dimens.nels();
		long* i_array_p = new long[n];
		for(int i=0; i<n; ++i )
			i_array_p[i] = i_array[i];
		write( tag, dimens, i_array_p );
		delete[] i_array_p;
	} catch( HYPSI::HypsiException& e )
	{
		e.push_err_msg( "In HDF5OutputAdaptor::write(vector<long> array)" );
		throw e;
	}
}

void HDF5OutputAdaptor::write( const std::string& objname,
                        const Dimens dimens, const int*** i_array )
{
  if( dimens.size() != 3 )
  {
    HYPSI::OutputException e(
      "Dimens size not 3 for object "+objname,
      "HDF5OutputAdaptor::write(int*** array)");
    throw e;
  }

try {
  int nels = dimens.nels();
  int* i_array_p = new int[nels];
  const int di = dimens[0];
  const int dj = dimens[1];
  const int dk = dimens[2];
  const int djk = dk * dj;
  for( int i=0; i<di; ++i )
    for( int j=0; j<dj; ++j )
      for( int k=0; k<dk; ++k )
        i_array_p[ i*djk + j*dk + k ] = i_array[i][j][k];
  write( objname, dimens, i_array_p );
  delete[] i_array_p;
} catch( HYPSI::HypsiException& e )
  {
     e.push_err_msg( "In HDF5OutputAdaptor::write(int*** array)" );
     throw e;
  }
}


// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  WRITE float FUNCTIONS
// -----------------------------------------------------------------------

/*


*/
void HDF5OutputAdaptor::write( const std::string& tag, float f_value )
{
try
{
  std::string ptag = purify_object_name( tag );
  
  std::vector<hid_t> hid_array;
  std::string dataset_name;

  get_dataset_context( tag, hid_array, dataset_name );
  
  hsize_t* hdf5dims = new hsize_t[1];
  hdf5dims[0] = 1;
  
  herr_t hdf5err = H5LTmake_dataset_float( hid_array[hid_array.size()-1],
                                         dataset_name.c_str(),
                                         1, hdf5dims, &f_value );

  if( hdf5err < 0 )
  {
    HYPSI::OutputException e(
      "make_dataset fails for "+tag,
      "HDF5OutputAdaptor::write(float)");
    throw e;
  }

// close groups, if any, but don't try to close the file id at [0]
  for( int i = hid_array.size()-1; i>0; --i )
    hdf5err = H5Gclose( hid_array[i] );
  
  delete[] hdf5dims;

} catch( HYPSI::HypsiException& e )
  {
     e.push_err_msg( "In HDF5OutputAdaptor::write(float)" );
     throw e;
  }
}

void HDF5OutputAdaptor::write( const std::string& tag,
              const Dimens dimens, const float* f_array )
{
try
{
  if( dimens.size() == 0 )
  {
    HYPSI::OutputException e(
     "Zero Dimens size",
     "HDF5OutputAdaptor::write(float* array)" );
    throw e;
  }
  
  std::string ptag = purify_object_name( tag );  
  
  std::vector<hid_t> hid_array;
  std::string dataset_name;

  get_dataset_context( tag, hid_array, dataset_name );
  
  hsize_t* hdf5dims = new hsize_t[dimens.size()];
  for( int i=0; i<dimens.size(); ++i )
    hdf5dims[i] = dimens[i];
 
  herr_t hdf5err = H5LTmake_dataset_float( hid_array[hid_array.size()-1],
                                         dataset_name.c_str(),
                                         dimens.size(), hdf5dims, f_array );

  if( hdf5err < 0 )
  {
    HYPSI::OutputException e(
      "make_dataset fails for "+tag,
      "HDF5OutputAdaptor::write(float* array)");
    throw e;
  }

// delete space associated with hdf5dims used in make_dataset
  delete[] hdf5dims;

// close groups, if any, but don't try to close the file id at [0]
  for( int i = hid_array.size()-1; i>0; --i )
    hdf5err = H5Gclose( hid_array[i] );

} catch( HYPSI::HypsiException& e )
  {
     e.push_err_msg( "In HDF5OutputAdaptor::write(float* array)" );
     throw e;
  }
}

void HDF5OutputAdaptor::write( const std::string& tag,
            const Dimens dimens, const std::vector<float>& f_array )
{
try
{
  int n = dimens.nels();
  float* f_array_p = new float[n];
  for(int i=0; i<n; ++i )
    f_array_p[i] = f_array[i];
  write( tag, dimens, f_array_p );
  delete[] f_array_p;
} catch( HYPSI::HypsiException& e )
  {
     e.push_err_msg( "In HDF5OutputAdaptor::write(vector<float> array)" );
     throw e;
  }
}

void HDF5OutputAdaptor::write( const std::string& objname,
                        const Dimens dimens, const float*** f_array )
{
  if( dimens.size() != 3 )
  {
    HYPSI::OutputException e(
      "Dimens size not 3 for object "+objname,
      "HDF5OutputAdaptor::write(float*** array)");
    throw e;
  }

try {
  int nels = dimens.nels();
  float* f_array_p = new float[nels];
  const int di = dimens[0];
  const int dj = dimens[1];
  const int dk = dimens[2];
  const int djk = dk * dj;
  for( int i=0; i<di; ++i )
    for( int j=0; j<dj; ++j )
      for( int k=0; k<dk; ++k )
        f_array_p[ i*djk + j*dk + k ] = f_array[i][j][k];
  write( objname, dimens, f_array_p );
  delete[] f_array_p;
} catch( HYPSI::HypsiException& e )
  {
     e.push_err_msg( "In HDF5OutputAdaptor::write(float*** array)" );
     throw e;
  }
}


// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  WRITE double FUNCTIONS
// -----------------------------------------------------------------------

/*


*/
void HDF5OutputAdaptor::write( const std::string& tag, double d_value )
{
try
{
  std::string ptag = purify_object_name( tag );
  
  std::vector<hid_t> hid_array;
  std::string dataset_name;

  get_dataset_context( tag, hid_array, dataset_name );
  
  hsize_t* hdf5dims = new hsize_t[1];
  hdf5dims[0] = 1;
  
  herr_t hdf5err = H5LTmake_dataset_double( hid_array[hid_array.size()-1],
                                         dataset_name.c_str(),
                                         1, hdf5dims, &d_value );

  if( hdf5err < 0 )
  {
    HYPSI::OutputException e(
      "make_dataset fails for "+tag,
      "HDF5OutputAdaptor::write(double)");
    throw e;
  }

// close groups, if any, but don't try to close the file id at [0]
  for( int i = hid_array.size()-1; i>0; --i )
    hdf5err = H5Gclose( hid_array[i] );
  
  delete[] hdf5dims;

} catch( HYPSI::HypsiException& e )
  {
     e.push_err_msg( "In HDF5OutputAdaptor::write(double)" );
     throw e;
  }
}

void HDF5OutputAdaptor::write( const std::string& tag,
              const Dimens& dimens, const double* d_array )
{
try
{
  if( dimens.size() == 0 )
  {
    HYPSI::OutputException e(
     "Zero Dimens size",
     "HDF5OutputAdaptor::write(double* array)" );
    throw e;
  }
  
  std::string ptag = purify_object_name( tag );  
  
  std::vector<hid_t> hid_array;
  std::string dataset_name;

  get_dataset_context( tag, hid_array, dataset_name );
  
  hsize_t* hdf5dims = new hsize_t[dimens.size()];
  for( int i=0; i<dimens.size(); ++i )
    hdf5dims[i] = dimens[i];
 
  herr_t hdf5err = H5LTmake_dataset_double( hid_array[hid_array.size()-1],
                                         dataset_name.c_str(),
                                         dimens.size(), hdf5dims, d_array );

  if( hdf5err < 0 )
  {
    HYPSI::OutputException e(
      "make_dataset fails for "+tag,
      "HDF5OutputAdaptor::write(double* array)");
    throw e;
  }

// delete space associated with hdf5dims used in make_dataset
  delete[] hdf5dims;

// close groups, if any, but don't try to close the file id at [0]
  for( int i = hid_array.size()-1; i>0; --i )
    hdf5err = H5Gclose( hid_array[i] );

} catch( HYPSI::HypsiException& e )
  {
     e.push_err_msg( "In HDF5OutputAdaptor::write(double* array)" );
     throw e;
  }
}

void HDF5OutputAdaptor::write( const std::string& tag,
            const Dimens& dimens, const std::vector<double>& d_array )
{
try {
  int n = dimens.nels();
  double* d_array_p = new double[n];
  for(int i=0; i<n; ++i )
    d_array_p[i] = d_array[i];
  write( tag, dimens, d_array_p );
  delete[] d_array_p;
} catch( HYPSI::HypsiException& e )
  {
     e.push_err_msg( "In HDF5OutputAdaptor::write(vector<double> array)" );
     throw e;
  }
}

void HDF5OutputAdaptor::write( const std::string& objname,
                        const Dimens dimens, double*** d_array )
{
  if( dimens.size() != 3 )
  {
    HYPSI::OutputException e(
      "Dimens size not 3 for object "+objname,
      "HDF5OutputAdaptor::write(double*** array)");
    throw e;
  }

try {
  int nels = dimens.nels();
  double* d_array_p = new double[nels];
  const int di = dimens[0]; 
  const int dj = dimens[1];
  const int dk = dimens[2];
  const int djk = dk * dj;
  for( int i=0; i<di; ++i )
    for( int j=0; j<dj; ++j )
      for( int k=0; k<dk; ++k )
        d_array_p[ i*djk + j*dk + k ] = d_array[i+1][j+1][k+1]; // I am not writing ghost cells
  write( objname, dimens, d_array_p );
  delete[] d_array_p;
} catch( HYPSI::HypsiException& e )
  {
     e.push_err_msg( "In HDF5OutputAdaptor::write(double*** array)" );
     throw e;
  }
}

void HDF5OutputAdaptor::write( const std::string& objname,
			       const Dimens dimens, const int ns, double**** d_array )
{
	if( dimens.size() != 3 )
	{
		HYPSI::OutputException e(
				"Dimens size not 3 for object "+objname,
		"HDF5OutputAdaptor::write(double**** array)");
		throw e;
	}

	try {
		int nels = dimens.nels();
		double* d_array_p = new double[nels];
		const int di = dimens[0]; 
		const int dj = dimens[1]; 
		const int dk = dimens[2]; 
		const int djk = dk * dj;
		for( int i=0; i<di; ++i )
			for( int j=0; j<dj; ++j )
				for( int k=0; k<dk; ++k )
				d_array_p[ i*djk + j*dk + k] = d_array[i+1][j+1][k+1][ns]; // I am not writing ghost cells
		write( objname, dimens, d_array_p );
		delete[] d_array_p;
	} catch( HYPSI::HypsiException& e )
	{
		e.push_err_msg( "In HDF5OutputAdaptor::write(double**** array)" );
		throw e;
	}
}

void HDF5OutputAdaptor::write( const std::string& objname,
			       const Dimens dimens, double** d_array )
{
	if( dimens.size() != 2 )
	{
		HYPSI::OutputException e(
				"Dimens size not 2 for object "+objname,
		"HDF5OutputAdaptor::write(double** array)");
		throw e;
	}

	try {
		int nels = dimens.nels();
		double* d_array_p = new double[nels];
		const int di = dimens[0]; 
		const int dj = dimens[1]; 
		for( int i=0; i<di; ++i )
		for( int j=0; j<dj; ++j )
		  d_array_p[ i*dj + j] = d_array[i+1][j+1]; // I am not writing ghost cells
		write( objname, dimens, d_array_p );
		delete[] d_array_p;
	} catch( HYPSI::HypsiException& e )
	{
		e.push_err_msg( "In HDF5OutputAdaptor::write(double** array)" );
		throw e;
	}
}

void HDF5OutputAdaptor::write( const std::string& objname,
			       const Dimens dimens, const int ns, double*** d_array )
{
	if( dimens.size() != 2 )
	{
		HYPSI::OutputException e(
				"Dimens size not 2 for object "+objname,
		"HDF5OutputAdaptor::write(double*** array)");
		throw e;
	}

	try {
		int nels = dimens.nels();
		double* d_array_p = new double[nels];
		const int di = dimens[0]; 
		const int dj = dimens[1]; 
		for( int i=0; i<di; ++i )
			for( int j=0; j<dj; ++j )
					d_array_p[ i*dj + j] = d_array[i+1][j+1][ns]; // I am not writing ghost cells
		write( objname, dimens, d_array_p );
		delete[] d_array_p;
	} catch( HYPSI::HypsiException& e )
	{
		e.push_err_msg( "In HDF5OutputAdaptor::write(double*** array)" );
		throw e;
	}
}



// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  WRITE string FUNCTIONS
// -----------------------------------------------------------------------

/*


*/
void HDF5OutputAdaptor::write( const std::string& tag, const std::string& s_value )
{
try
{
  std::string ptag = purify_object_name( tag );
  
  std::vector<hid_t> hid_array;
  std::string dataset_name;

  get_dataset_context( tag, hid_array, dataset_name );
  
  hsize_t* hdf5dims = new hsize_t[1];
  hdf5dims[0] = 1;
  
  herr_t hdf5err = H5LTmake_dataset_string( hid_array[hid_array.size()-1],
                                         dataset_name.c_str(),
					   s_value.c_str() );

  if( hdf5err < 0 )
  {
    HYPSI::OutputException e(
      "make_dataset fails for "+tag,
      "HDF5OutputAdaptor::write(string)");
    throw e;
  }

// close groups, if any, but don't try to close the file id at [0]
  for( int i = hid_array.size()-1; i>0; --i )
    hdf5err = H5Gclose( hid_array[i] );
  
  delete[] hdf5dims;

} catch( HYPSI::HypsiException& e )
  {
     e.push_err_msg( "In HDF5OutputAdaptor::write(string)" );
     throw e;
  }
}

} // end namespace HYPSI

#endif // _HDF5_ADAPTOR_H_
