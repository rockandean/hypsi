/*! \file: zonearray.h
 *  
 * This file is included by hypsi.h
 * This file is included by ptcls.cc
*/

#ifndef _HYPSI_ZONEARRAY_H_
#define _HYPSI_ZONEARRAY_H_

#include <string>
#include <fstream>
#include <iostream>
#include <vector>
#include "mpi.h"
#include "exception.h"

namespace HYPSI {

// forward declaration

class ScalarZarray;


//! \brief Simple xyz (Cartesian) vector (double)
//! There is an overloaded << operator for a xyzVector object
//! There is a dot(a, b) for a,b xyzVector objects
class xyzVector {
public:
  double _x, _y, _z;

  xyzVector( void                          ) : _x(0), _y(0), _z(0) {;}
  xyzVector( const xyzVector& v            ) : _x(v._x), _y(v._y), _z(v._z) {;}
  xyzVector( double x, double y, double z  ) : _x(x), _y(y), _z(z) {;}
  xyzVector( const vector<double>& v       )
  { if( v.size()!=3 ) throw HypsiException(
     "Initialization from vector<> not size 3","Hypsi::xyzVector" );
     _x = v[0]; _y = v[1]; _z=v[2];}

  xyzVector& set( double x, double y, double z  )
  { _x = x; _y = y; _z = z; return *this;}

  double mag( void ) const { return sqrt( _x*_x + _y*_y + _z*_z ); }

  xyzVector& operator=( const xyzVector& v )
  { _x = v._x; _y = v._y; _z = v._z; return *this; }

  xyzVector& operator=( const vector<double>& v )
  { if( v.size()!=3 ) throw HypsiException(
      "Initialization from vector<> not size 3","Hypsi::xyzVector::op=" );
    _x = v[0]; _y = v[1]; _z=v[2]; return *this; }

// theta: 0 to PI, phi: -PI to +PI
  void rthetaphi( double& r, double& theta, double& phi ) const
  { r = mag();
    theta = (r==0.0)? 0.0 : acos( _z/r );
    phi = (_x==0.0 && _y==0.0)? 0.0 : atan2( _y, _x ); }
  
  xyzVector& normalize( double unitvalue = 1.0 )
  {  double magval = mag();
     _x *= unitvalue/magval; _y *= unitvalue/magval; _z *= unitvalue/magval;
     return *this; }
  
}; // end --- xyzVector

inline double dot( const xyzVector& a, const xyzVector& b )
{ return a._x*b._x + a._y*b._y + a._z*b._z; }

inline ostream& operator<<( ostream& ostrm, const xyzVector& v )
{ ostrm << "(" << v._x << "," << v._y << "," << v._z << ")"; return ostrm; }

/*
xyzVector operator+( const xyzVector& v1, const xyzVector& v2 )
{ return xyzVector( v1._x + v2._x, v1._y + v2._y, v1._z + v2._z ); }
xyzVector operator-( const xyzVector& v1, const xyzVector& v2 )
{ return xyzVector( v1._x - v2._x, v1._y - v2._y, v1._z - v2._z ); }
double dot( const xyzVector& v1, const xyzVector& v2 )
{ return v1._x * v2._x + v1._y * v2._y + v1._z * v2._z; }
*/

/*! \brief region in xyz space
*/
class xyzRegion {
public:
  double x0, x1;
  double y0, y1;
  double z0, z1;

  xyzRegion( void )
   : x0(0), x1(0), y0(0), y1(0), z0(0), z1(0) {;}
  xyzRegion( const xyzRegion& r )
   : x0(r.x0), x1(r.x1), y0(r.y0), y1(r.y1), z0(r.z0), z1(r.z1) {;}
  xyzRegion( double xx0, double xx1, double yy0, double yy1,
             double zz0, double zz1 )
  { x0 = xx0; x1 = xx1; y0 = yy0; y1 = yy1; z0 = zz0; z1 = zz1; }

  void set( double xx0, double xx1, double yy0, double yy1,
            double zz0, double zz1 )
  { x0 = xx0; x1 = xx1; y0 = yy0; y1 = yy1; z0 = zz0; z1 = zz1; }
  
  void set( const vector<double>& v )
  {
    if( v.size()!= 6 ) throw HypsiException(
      "Set from vector<> not size 6","Hypsi::xyzRegion" );
    x0 = v[0]; x1 = v[1]; y0 = v[2]; y1 = v[3]; z0 = v[4]; z1 = v[5]; }
  
  xyzRegion& operator=( const xyzRegion& r )
  { x0 = r.x0; x1 = r.x1; y0 = r.y0; y1 = r.y1; z0 = r.z0;
    z1 = r.z1; return *this; }

  double xlen( void ) const { return x1 - x0; }
  double ylen( void ) const { return y1 - y0; }
  double zlen( void ) const { return z1 - z0; }

  bool in_region( const xyzRegion& r ) const
  { return ( r.x0 >= x0 && r.x1 <= x1 )
            && ( r.y0 >= y0 && r.y1 <= y1 ) && ( r.z0 >= z0 && r.z1 <= z1 );}

  bool in_region( double x, double y, double z ) const
  { return ( x >= x0 && x <= x1 )
            && ( y >= y0 && y <= y1 ) && ( z >= z0 && z <= z1 );}

  bool in_region_co( double x, double y, double z ) const
  { return ( x >= x0 && x < x1 )
            && ( y >= y0 && y < y1 ) && ( z >= z0 && z < z1 );}
  
  void get_vector( vector<double>& v )
  { v.resize(6); 
    v[0] = x0; v[1] = x1; v[2] = y0; v[3] = y1; v[4] = z0; v[5] = z1; }

  void print( ostream& ostrm, string s="" ) const
  {  ostrm << s << "xyzRegion: " 
           << " [" << x0 << "," << x1
           << "][" << y0 << "," << y1
           << "][" << z0 << "," << z1 << "]\n"; }
};


// ------------------------------------------------------------------------

/*! \brief the InterpWeights class
*  vertices of cube are labelled
*
*  0 .. 000  ... corner with smallest xyz coordinates
*  1 .. 001  ... x,   y,   z+1
*  2 .. 010  ... x,   y+1, z
*  3 .. 011  ... x,   y+1, z+1
*  4 .. 100  ... x+1, y,   z
*  5 .. 101  ... x+1, y,   z+1
*  6 .. 110  ... x+1, y+1, z
*  7 .. 111  ... x+1, y+1, z+1
*/

class InterpWeights {
public:
  double bw000, bw100, bw010, bw110, bw001, bw101, bw011, bw111;
  double ew000, ew100, ew010, ew110, ew001, ew101, ew011, ew111;
  int bi000, bi100, bi010, bi110, bi001, bi101, bi011, bi111;
  int ei000, ei100, ei010, ei110, ei001, ei101, ei011, ei111;

  union {
    double ew[8];
    struct {
      double ew000, ew100, ew010, ew110, ew001, ew101, ew011, ew111;
           } w;
        } ew;

  bool valid[8];

  bool check;

  InterpWeights( void ) {;}

};

// ------------------------------------------------------------------------


/*! \brief Information about the Domain - ie the complete simulated region
*/
class Domain {

public:

  bool initialized;

  int nxcell, nycell, nzcell;     //!< number of cells in domain
  double dxcell, dycell, dzcell;  //!< cell sizes

  double xlen, ylen, zlen;  //!< domain sides lengths
  xyzRegion region;

  int cntotal;             //!< total number of compute nodes
  int n_zones;             //!< total number of zones (==compute nodes)
  int nxcn, nycn, nzcn;    //!< number compute nodes in x,y,z coords
  int nx_zn, ny_zn, nz_zn;    //!< (same as nxcn etc) number compute nodes in x,y,z coords
  int nyzcn, nyz_zn ;

  double x_znlen, y_znlen, z_znlen;  //!< lengths of sides of a zone

  Domain( void ) : initialized(false) {;}
  
  void initialize( int nxcn_a, int nycn_a, int nzcn_a,
                   int nxcell_a, int nycell_a, int nzcell_a,
                   xyzRegion& domain_region )
  {
    nxcn = nxcn_a; nycn = nycn_a; nzcn = nzcn_a;
    nyzcn = nycn * nzcn;
    nx_zn = nxcn; ny_zn = nycn; nz_zn = nzcn;
    nyz_zn = ny_zn * nz_zn;
    
    n_zones = nxcn_a * nycn_a * nzcn_a;
    cntotal = n_zones;

    nxcell = nxcell_a; nycell = nycell_a; nzcell = nzcell_a;
    xlen = domain_region.xlen(); ylen = domain_region.ylen();
    zlen = domain_region.zlen();
    dxcell = xlen/nxcell; dycell = ylen/nycell; dzcell = zlen/nzcell;
    region = domain_region;
    x_znlen = xlen/nxcn; y_znlen = ylen/nycn; z_znlen = zlen/nzcn;
    initialized = true;
  }

  void get_cellcentre_position( int ix, int iy, int iz, xyzVector& p )
  {
    if( ix < 1 || ix > nxcell || iy < 1 || iy > nycell
         || iz < 1 || iy > nzcell )
      throw HypsiException(
      "Index out of bound","Domain::get_cellcentre_position" );
    p._x = (ix - 0.5) * dxcell; p._y = (iy - 0.5) * dycell;
    p._z = (iz - 0.5) * dzcell;
  }

  void get_cellvertex_position( int ix, int iy, int iz, xyzVector& p )
  {
    if( ix < 1 || ix > nxcell+1 || iy < 1 || iy > nycell+1
          || iz < 1 || iz > nzcell+1 )
      throw HypsiException(
      "Index out of bound","Domain::get_cellcentre_position" );
    p._x = (ix - 1.0) * dxcell; p._y = (iy - 1.0) * dycell;
    p._z = (iz - 1.0) * dzcell;
  }

}; 

/*! \brief All about a Zone 
*/

class Zone {

public:

  bool initialized;

  xyzRegion region;       //!< region covered by zone
  double dx, dy, dz;      //!< cell size
  int nx, ny, nz;         //!< number of cells
  int nx2, ny2, nz2, nyz2, nxyz2;

  int izone;              //!< node number of this zone
  int nzones;             //!< total number of zones
  int ixcn, iycn, izcn;   //!< position of zone in compute node space
  int znneighbours[27];   //!< node numbers of neighbouring zones
  
  const Domain* domainp;   //!< ptr to info about domain and compute nodes etc

  Zone( void ) : izone(-1), nzones(0), initialized( false ) {;}

  Zone( const Zone& zi )
    : izone( zi.izone ), nzones( zi.nzones ),
      ixcn( zi.ixcn ), iycn( zi.iycn ), izcn( zi.izcn ),
      dx( zi.dx ), dy( zi.dy ), dz( zi.dz ),
      nx( zi.nx ), ny( zi.ny ), nz ( zi.nz ), region( zi.region ),
      domainp( zi.domainp ),
      initialized( zi.initialized )
      {
        nx2 = nx+2; ny2 = ny+2; nz2 = nz+2;
        nyz2 = ny2*nz2; nxyz2 = nx2 * nyz2;
        for(int i=0;i<27;i++) znneighbours[i] = zi.znneighbours[i];
      }
      
  void initialize( int izone_a, int nzones_a, const Domain* domainp_a )
  {
    izone = izone_a;
    nzones = nzones_a;
    domainp = domainp_a;

// copy cell sizes from domain
    dx = domainp->dxcell;
    dy = domainp->dycell;
    dz = domainp->dzcell;

// number of cells in zone - values already checked for commensurability
    nx = domainp->nxcell / domainp->nxcn;
    ny = domainp->nycell / domainp->nycn;
    nz = domainp->nzcell / domainp->nzcn;
    nx2 = nx+2; ny2 = ny+2; nz2 = nz+2;
    nyz2 = ny2*nz2; nxyz2 = nx2 * nyz2;

// zone position in compute node space
    ixcn = izone / domainp->nyzcn;
    int rem = izone % domainp->nyzcn;
    iycn = rem / domainp->nzcn;
    izcn = rem % domainp->nzcn;

// zone region
    double xzone = domainp->xlen / domainp->nxcn;
    double yzone = domainp->ylen / domainp->nycn;
    double zzone = domainp->zlen / domainp->nzcn;

    region.set( xzone*ixcn, xzone*(ixcn+1),
                yzone*iycn, yzone*(iycn+1), zzone*izcn, zzone*(izcn+1) );

// zone neigbours as compute node (zone) number

// xyz periodic
    int j=0;
    for(int ix=ixcn-1;ix<=ixcn+1;ix++)
    for(int iy=iycn-1;iy<=iycn+1;iy++)
    for(int iz=izcn-1;iz<=izcn+1;iz++)
    {
      znneighbours[ j++ ] = (ix+domainp->nxcn)%domainp->nxcn * domainp->nyzcn
                           + (iy+domainp->nycn)%domainp->nycn * domainp->nzcn
                           + (iz+domainp->nzcn)%domainp->nzcn;
    }

    initialized = true;
  }


  Zone& operator=( const Zone& zone )
  {
    izone = zone.izone;
    nzones = zone.nzones;
    domainp = zone.domainp;
    ixcn = zone.ixcn; iycn = zone.iycn; izcn = zone.izcn;
    dx = zone.dx; dy = zone.dy; dz = zone.dz;
    nx = zone.nx; ny = zone.ny; nz = zone.nz;
    nx2 = nx+2; ny2 = ny+2; nz2 = nz+2;
    nyz2 = ny2*nz2; nxyz2 = nx2 * nyz2;
    region = zone.region;
    for(int i=0;i<27;i++) znneighbours[i] = zone.znneighbours[i];
    initialized = zone.initialized;
    return *this;
  }

  inline int idx( int ix, int iy, int iz ) const
  { return ix*nyz2 + iy*nz2 + iz; }

  void calculate_eweights( const double* xyzp, InterpWeights& w );
  void calculate_eweights_check( const double* xyzp, InterpWeights& w );
  void calculate_eweights_nocheck( const double* xyzp, InterpWeights& w );

  void print( ostream& ostrm, string s="" ) const
  { ostrm << s << "Z" << izone << "/" << nzones << " ";
    region.print( ostrm, "" );
    ostrm << s << "Z" << izone << "/" << nzones
          << " comp : ["<<ixcn<<", "<<iycn<<", "<<izcn
          <<"] cell: [" << dx << ", " << dy << ", " << dz
          << "] ncells: [" << nx << ", " << ny << ", " << nz << "]\n";
  }

};  // end of Class Zone


// ------------------------------------------------------------------------
class ScalarGcellBuffer {
public:

  double* buffp;       //!< pointer to buffer values block
  double* buffers[27]; //!< pointers to 27 neighbour buffers
  int buffssiz[27];    //!< sizes of 27 neighnour buffers

  int nx, nx1, nx2, ny, ny1, ny2, nz, nz1, nz2, nyz2, nxyz2;
  int znneighbours[27];   //!< node numbers of neighbouring zones

  bool isend_complete_arr[27];
  bool irecv_complete_arr[27];
  MPI_Request send_requests[27];
  MPI_Request recv_requests[27];

  ScalarGcellBuffer( void ) : buffp(0) {;}
  ScalarGcellBuffer( const Zone& zone );

  ~ScalarGcellBuffer( void )
  {
    if( buffp != 0 ) delete[] buffp;
  }

  void initialize( const Zone& zone );

  inline int idx( int ix, int iy, int iz ) const
  { return ix*nyz2 + iy*nz2 + iz; }

  void isend_to_neighbours( void );
  bool isend_complete( void );
  void irecv_from_neighbours( void );
  bool irecv_complete( void );
  void copy_in_from_array( const ScalarZarray& a );
  void copy_in_from_array_gcells( const ScalarZarray& a );
  void copy_out_to_array_gcells( const ScalarZarray& a );
  void copy_out_to_array_increment( const ScalarZarray& a );

};  // end 
// ------------------------------------------------------------------------

class ScalarZarray {
public:

  double *V;

  Zone zinfo;
  int izone;
  int nx, nx1, nx2, ny, ny1, ny2, nz, nz1, nz2, nyz2, nxyz2;
  bool V_alloc;     //!< true if space for values allocated by object

  ScalarGcellBuffer send_gcell_buff, recv_gcell_buff;

private:

public:

  ScalarZarray( void ) : V_alloc(false), V(0)
   { ;
//     cout << "ScalarZarray default(VOID) constructor\n";
   }

  ScalarZarray( const Zone& zi, double* V_a=0 )
    : zinfo( zi ), send_gcell_buff( zi ), recv_gcell_buff( zi )
  {
    izone = zi.izone;
    nx = zi.nx; nx1 = nx+1; nx2 = nx+2;
    ny = zi.ny; ny1 = ny+1; ny2 = ny+2;
    nz = zi.nz; nz1 = nz+1; nz2 = nz+2;
    nyz2 = ny2*nz2; nxyz2 = nx2 * nyz2;

//  cout<<"Z"<<zi.izone<<": ScalarZarray::ScalarZarray: : "
//      <<nx<<" "<<ny<<" "<<nz<<"(" << nxyz2 <<") [ptr:"<<V_a << "]\n";

    if( V_a != 0 )
    { V_alloc = false; V = V_a; }
    else
    { V_alloc = true; V = new double[ nxyz2 ]; }
  }
  
  ~ScalarZarray( void )
  {
    if( V_alloc )
      delete[] V;
  }

  void initialize( const Zone& zi, double* V_a=0 )
  {
    zinfo = zi;

    if( !zi.initialized )
    throw HypsiException(
      "Zone not initialized", "ScalarZarray::initialize");

    send_gcell_buff.initialize(zi);
    recv_gcell_buff.initialize(zi);

    izone = zi.izone;
    nx = zi.nx; nx1 = nx+1; nx2 = nx+2;
    ny = zi.ny; ny1 = ny+1; ny2 = ny+2;
    nz = zi.nz; nz1 = nz+1; nz2 = nz+2;
    nyz2 = ny2*nz2; nxyz2 = nx2 * nyz2;

//  cout<<"Z"<<zi.izone<<": ScalarZarray::initialize: : "
//      <<nx<<" "<<ny<<" "<<nz<<"(" << nxyz2 <<") [ptr:"<<V_a << "]\n";

    if( V_a != 0 )
    { V_alloc = false; V = V_a; }
    else
    { V_alloc = true; V = new double[ nxyz2 ]; }
  }
 
  void set_zoneinfo( const Zone& zi )
  { if( !( zi.nx == nx && zi.ny == ny && zi.nz == nz ) )
      throw  HypsiException( "Incommensurate sizes", "ScalarZarray::set_zoneinfo()" );
    zinfo = zi;
    izone = zi.izone; }

  inline int idx( int ix, int iy, int iz ) const
  { return ix*nyz2 + iy*nz2 + iz; }
  
  void set_zero( void )
  { for( int i=0; i<nxyz2; ++i ) V[i] = 0.0; }

  void set_uniform( double v )
  { for( int i=0; i<nxyz2; ++i ) V[i] = v; }

  void set_test_data( void )
  { for( int ix=0; ix<=nx1; ix++)
    for( int iy=0; iy<=ny1; iy++)
    for( int iz=0; iz<=nz1; iz++)
      if( ix==0 || iy==0 || iz==0 || ix==nx1 || iy==ny1 || iz==nz1 )
        V[ idx(ix,iy,iz) ] = -100000;
      else
        V[ idx(ix,iy,iz) ] = 1000000 + 10000*izone + 100*ix + 10*iy + iz;
  }

  void copy_data_in( const ScalarZarray& a )
  { if( !( a.nx == nx && a.ny == ny && a.nz ) )
      throw  HypsiException( "Incommensurate sizes", "ScalarZarray::copy_data_in()" );
    for( int i=0; i < nxyz2; ++i ) V[i] = a.V[i];
  }

  void mpi_send( int dst, int tag );
  void mpi_isend( int dst, int tag, MPI_Request* mpi_request );
  void mpi_recv( int src, int tag );
  void mpi_irecv( int src, int tag, MPI_Request* mpi_request );

  void make_xyzperiodic( void );
  void adjust_moment_edges_xyzperiodic( void );

  void test_output( string f, int zn )
  {
    stringstream ss;
    ss.str("");
    ss<<f<<"_Z"<<zn;
    ofstream fs;
    fs.open( ss.str().c_str() );
    fs << "ScalarZarray test_output FILE: "<<ss.str()<<"\n";
    fs << " Neighbours: \n";
    for(int i=0;i<27;i++)
    { fs << "  " << zinfo.znneighbours[i];
      if( (i+1)%9 == 0 ) fs<<"\n";
      else fs <<", ";
    }

    fs.precision(0);
    for( int ix=0; ix<=nx1; ix++)
    {
      fs << "\n ix = "<< ix << "\n";
      for( int iy=ny1; iy>=0; iy--)
      {
        for( int iz=0; iz<=nz1; iz++) fs<< fixed << V[ idx(ix,iy,iz) ] << " ";
        fs << "\n";
      }
    }
    fs << "\n";
    fs.close();
  }

};  // end of ScalarZarray


} // end namespace HYPSI



#endif      //   _HYPSI_ZONEARRAY_H_
