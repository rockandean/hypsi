#ifndef _BLOBS3D_H_
#define _BLOBS3D_H_

#include <complex>
#include <string>
#include <fstream>
#include <iostream>
#include <sstream>

namespace HYPSI {
class Blobs3d
{
public:
    Blobs3d(void){;}

    UniformIC uniform_initial_conditions;

    void read_input_data( ConfigData& config_data);

    void initialize_fields_and_particles( Hypsim& hypsim);
    /*!
     put here the parameters needed for the blob initialization
    */
    void output_params( ostream& ostrm, string s="" ) const
    {

    }
};
} // end namespace HIPSY
#endif // BLOBS3D_H
