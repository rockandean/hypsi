
#include <iostream>

#include "hypsi.h"

namespace HYPSI {

using namespace std;

void ConfigData::initialize( int argc, char** argv )
{

  /*
cout << "ConfigData::initialize  ";
cout << "argc: " << argc << "\n" ;
for(int i=0;i<argc;++i)
  cout << i << ": " << argv[i] << "\n" ;
  */

  if( argc >= 2 )
    config_fname = argv[1];
  else
    throw HypsiException( "Insufficient arguments", "ConfigData::initialize" );

  try{
  
    read_file( config_fname );

    if( num_parse_errors() != 0 )
    {
      diag_print_errors() ;
      throw HypsiException(
        "Parse errors reading configuration file: <" + config_fname + ">",
        "ConfigData::initialize" );    }
 
  } catch( KVF::kvfException& e )
  {
    cout << "Caught KVF exception!\n";
    e.diag_cout();
    throw HypsiException(
      "Failed to open/read configuration file: <" + config_fname + ">",
      "ConfigData::initialize" );
  }
 
} // end ConfigData::initialize

void SimulationParameters::initialize_from_config_data( ConfigData& config_data )
{
  vector<double> domain_size_vec, domain_cellsize_vec;
  vector<double> cn_numxyz_vec;

  try{
    config_data.get_data( "output_control/data_filename_root", output_datafile_root );
    config_data.get_data( "time_step", dt );
    config_data.get_data( "n_timesteps", ntimesteps );
    config_data.get_data( "n_field_substeps", nsubsteps );
    config_data.get_data( "uniform_resistivity", uniform_resis );
    config_data.get_data( "initial_electron_temperature", initial_Te );
    config_data.get_data( "electron_fluid_gamma", electron_gamma );
    config_data.get_data( "domain/xyzsize", domain_size_vec );
    config_data.get_data( "domain/xyzcell", domain_cellsize_vec );
    
    config_data.get_data( "compute_nodes_xyz_geometry", cn_numxyz_vec );

    config_data.get_data( "particle_rng_seed", prng_seed );

    config_data.get_data( "smoothing_control/smoothfac_EinEBadvance", smoothfac_EinEBadvance );

  } catch( KVF::kvfException& e )
  {
    e.diag_cout();
    throw HypsiException(
      "Failed to get simulation parameters data from file: <" + config_data.filename() + ">",
      "SimulationParameters::initialize_from_config_data" );
  }

  if( domain_cellsize_vec.size() != 3 )
    throw HypsiException(
      "domain/xyzcell requires three numbers: in file <" + config_data.filename() + ">",
      "SimulationParameters::initialize_from_config_data" );
  if( domain_cellsize_vec[0]<= 0 || domain_cellsize_vec[1]<= 0 
      || domain_cellsize_vec[2]<= 0  )
    throw HypsiException(
      "domain/xyzcell must be positive: in file <" + config_data.filename() + ">",
      "SimulationParameters::initialize_from_config_data" );

  dxcell = domain_cellsize_vec[0];
  dycell = domain_cellsize_vec[1];
  dzcell = domain_cellsize_vec[2];

  if( domain_size_vec.size() != 3 )
    throw HypsiException(
      "domain/xyzsize requires three numbers: in file <" + config_data.filename() + ">",
      "SimulationParameters::initialize_from_config_data" );
  if( domain_size_vec[0]<= 0 || domain_size_vec[1]<= 0
      || domain_size_vec[2]<= 0  )
    throw HypsiException(
      "domain/xyzsize must be positive: in file <" + config_data.filename() + ">",
      "SimulationParameters::initialize_from_config_data" );

  xdomain = domain_size_vec[0];
  ydomain = domain_size_vec[1];
  zdomain = domain_size_vec[2];

  domain_region.set( 0.0, domain_size_vec[0], 0.0, domain_size_vec[1],
                  0.0, domain_size_vec[2] );
  
//  domain_region.print( cout );

  nxdomain = static_cast<int>( domain_size_vec[0] / dxcell );
  nydomain = static_cast<int>( domain_size_vec[1] / dycell );
  nzdomain = static_cast<int>( domain_size_vec[2] / dzcell );
  
  if( nxdomain != xdomain / dxcell 
      || nydomain != ydomain / dycell 
      || nzdomain != zdomain / dzcell )
    throw HypsiException(
      "Cell size and domain size must be commensurate: file <" + config_data.filename() + ">",
      "SimulationParameters::initialize_from_config_data" );

  if( cn_numxyz_vec.size() != 3 )
    throw HypsiException(
      "compute_nodes_xyz_geometry requires three numbers: file <" + config_data.filename() + ">",
      "SimulationParameters::initialize_from_config_data" );
  if( cn_numxyz_vec[0]<= 0 || cn_numxyz_vec[1]<= 0
      || cn_numxyz_vec[2]<= 0  )
    throw HypsiException(
      "compute_nodes_xyz_geometry must be positive: file <" + config_data.filename() + ">",
      "SimulationParameters::initialize_from_config_data" );

  nx_zones = static_cast<int>( cn_numxyz_vec[0] );
  ny_zones = static_cast<int>( cn_numxyz_vec[1] );
  nz_zones = static_cast<int>( cn_numxyz_vec[2] );

  if( cn_numxyz_vec[0] != nx_zones || cn_numxyz_vec[1] != ny_zones
      || cn_numxyz_vec[2] != nz_zones  )
    throw HypsiException(
      "compute_nodes_xyz_geometry must be integers: file <" + config_data.filename() + ">",
      "SimulationParameters::initialize_from_config_data" );

  if( nxdomain % nx_zones || nydomain % ny_zones || nzdomain % nz_zones  )
    throw HypsiException(
      "compute_nodes_xyz_geometry and domain must be commensurate: file <" + config_data.filename() + ">",
      "SimulationParameters::initialize_from_config_data" );

}  //  end SimulationParameters::initialize_from_config_data(


// ------------------------------------------------------------------------

void Hypsim::initialize_zones_info( void )
{
 zones_info.resize( sim_params.n_zones );

 for( int i=0; i < sim_params.n_zones ; ++i )
 {
    zones_info[i].initialize( i, sim_params.n_zones, &domain_info );

//    zones_info[i].print( cout );

// zone info for this node
    if( i == mpi_comm.thisnode )
      zinfo = zones_info[i];
  }

//  zinfo.print( cout, "This zone ... " );

} // end Hypsim::initial_zones_info


// ------------------------------------------------------------------------

void Hypsim::initialize_rng( void )
{

// make sure that rng seed is different for different zones

  sim_params.prng_seed += sim_params.hostzone;

/*
cout << "Hypsim::initialize_rng: (for zone: " << sim_params.hostzone << "): "
     << sim_params.prng_seed << endl;
*/
  
  prng.set_seed( sim_params.prng_seed );

} // end Hypsim::initialize_rng


/* sum over species for various charge moments

NOTE: all pzmoms are periodic at this stage - so don't need to
impose periodicity after summation

*/
void Hypsim::calc_nvq_moments( void )
{
  mp_qmoms->set_zero();
  ep_qmoms->set_zero();
  ca_qmoms->set_zero();

// cout << " COMPLETED moms set zero \n";

  for( int ipset=0; ipset < sim_params.npsets; ++ipset )
  {
    double ca_w;
    
    ca_w = psets[ipset]->qw * psets[ipset]->qw / psets[ipset]->mw;

// experiment ... 
//    mp_qmoms->addwfac_pzmoms_a( psets[ipset]->qw, *pset_pzmoms[ipset] );

    mp_qmoms->addwfac_pzmoms_a( 0.5*psets[ipset]->qw, *pset_pzmoms[ipset] );
    mp_qmoms->addwfac_pzmoms_b( 0.5*psets[ipset]->qw, *pset_pzmoms[ipset] );

    ep_qmoms->addwfac_pzmoms_b( psets[ipset]->qw, *pset_pzmoms[ipset] );

    ca_qmoms->addwfac_pzmoms_b( ca_w, *pset_pzmoms[ipset] );
  }

// TRY SMOOTHING

// MOVE THIS TO SEPARATE FUNCTION

/*

  mp_qmoms->smooth_dn_data( 0.15 );
  ep_qmoms->smooth_dn_data( 0.15 );
  ca_qmoms->smooth_dn_data( 0.15 );
  mp_qmoms->smooth_U_data( 0.15 );
  ep_qmoms->smooth_U_data( 0.15 );
  ca_qmoms->smooth_U_data( 0.15 );

*/
} // end Hypsim::calc_nvq_moments




void Hypsim::calc_camE_xyzperiodic( double dt, ZoneFields& F,
                       QZMoments& mp_qzmoms,
                       QZMoments& ep_qzmoms,
                       QZMoments& ca_qzmoms,
                       ScalarZarray& pe, ScalarZarray& resis )
{
// cell vertex indices
  int i000, i100, i010, i110, i001, i101, i011, i111;

// B at cell centre
  double Bcc_x, Bcc_y, Bcc_z;

// local object for mixed time level charge moments
//  QZMoments qmoms( F.zinfo );

//  cout<<"Z"<<mpi_comm.thisnode << ": camE done qmoms allocate\n";

// construct mixed time level charge moments

  camE_qmoms->copy_dn_data( ep_qzmoms );
  camE_qmoms->copy_U_data( mp_qzmoms );

//  cout<<"Z"<<mpi_comm.thisnode << ": camE done qmoms copy\n";

// calculate E based on mixed time level charge moments

  F.calcE( *camE_qmoms, pe, resis );

//  cout<<"Z"<<mpi_comm.thisnode << ": camE done calcE\n";

// make E periodic

  F.make_E_xyzperiodic();

//  cout<<"Z"<<mpi_comm.thisnode << ": camE done Exyzperiodic\n";

// current advance correction (loop over cells)

  for( int ix=1; ix <= F.nx; ++ix )
  for( int iy=1; iy <= F.ny; ++iy )
  for( int iz=1; iz <= F.nz; ++iz )
  {
    i000 = F.idx( ix,iy,iz );
    i100 = i000 + F.nyz2;
    i010 = i000 + F.nz2;
    i110 = i010 + F.nyz2;
    i001 = i000 + 1;
    i101 = i100 + 1;
    i011 = i010 + 1;
    i111 = i110 + 1;

// B at cell centre
    Bcc_x = ( F.Bx[i000] + F.Bx[i100] + F.Bx[i010]+ F.Bx[i110]
            + F.Bx[i001] + F.Bx[i101] + F.Bx[i011]+ F.Bx[i111] )/ 8.0;

    Bcc_y = ( F.By[i000] + F.By[i100] + F.By[i010]+ F.By[i110]
            + F.By[i001] + F.By[i101] + F.By[i011]+ F.By[i111] )/ 8.0;

    Bcc_z = ( F.Bz[i000] + F.Bz[i100] + F.Bz[i010]+ F.Bz[i110]
            + F.Bz[i001] + F.Bz[i101] + F.Bz[i011]+ F.Bz[i111] )/ 8.0;

// current advance method correction
    camE_qmoms->Ux[i000] = ep_qzmoms.Ux[i000]
               + dt * ( ca_qzmoms.dn[i000] * F.Ex[i000]
               + ca_qzmoms.Uy[i000] * Bcc_z - ca_qzmoms.Uz[i000] * Bcc_y ); 
    camE_qmoms->Uy[i000] = ep_qzmoms.Uy[i000]
               + dt * ( ca_qzmoms.dn[i000] * F.Ey[i000]
               + ca_qzmoms.Uz[i000] * Bcc_x - ca_qzmoms.Ux[i000] * Bcc_z ); 
    camE_qmoms->Uz[i000] = ep_qzmoms.Uz[i000]
               + dt * ( ca_qzmoms.dn[i000] * F.Ez[i000]
               + ca_qzmoms.Ux[i000] * Bcc_y - ca_qzmoms.Uy[i000] * Bcc_x );
  }

//  cout<<"Z"<<mpi_comm.thisnode << ": camE done qmoms set loop\n";

// make advanced charge moments periodic

  camE_qmoms->make_xyzperiodic();

//  cout<<"Z"<<mpi_comm.thisnode << ": camE done qmoms.make_xyzperiodic\n";

// calculate E based on mixed time level charge moments

  F.calcE( *camE_qmoms, pe, resis );

// make E periodic

  F.make_E_xyzperiodic();

}

/*
advance fields F (and leap frogging fields Fh) by time step dt
using nsubsteps

F and Fh are referred to same time instance at both function entry and exit

This is accomplished with initial and final half steps in leap frogging. 

charge moments: qzmoms
electron pressure: pe
resistivity: resis
*/
void Hypsim::advanceEB_xyzperiodic( ZoneFields& F, ZoneFields& Fh,
                        double dt, int nsubsteps,
                        QZMoments& qzmoms,
                        ScalarZarray& pe, ScalarZarray& resis,
                        double smoothfacE )
{

//cout << " Hypsim::advanceEB_xyzperiodic  using smoothing factor: "
// << smoothfacE <<"\n";

// number of substeps in leapfrogging solution
  int nss = nsubsteps * 2;  // force even

// leapfrog substep size
  double dtsubstep;

// Note loop is done nss+1 times for leapfrogging
//  ... and to include initial and final half steps

  for( int i=0; i<=nss; ++i )
  {
// first and last step half the usual size
    dtsubstep = dt / nsubsteps;
    if( i==0 || i==nss )
      dtsubstep = dtsubstep/2.0;

// leapfrogging depends if i is even or odd

    if( (i % 2) == 0 )
    {

// calculate E based on B
      F.calcE( qzmoms, pe, resis );

// make E periodic

      F.make_E_xyzperiodic();
      
// smooth E amd make periodic - again!

      F.smooth_Edata( smoothfacE );
      
      F.make_E_xyzperiodic();

// update E of half step solution
      Fh.copy_Edata( F );

// special
      Fh.advance_psi( dtsubstep );
      Fh.make_psi_xyzperiodic();

// advance B of half step solution
      Fh.advanceB( dtsubstep );

// make B periodic

      Fh.make_B_xyzperiodic();

// check div B

#ifdef DIAG_FIELD_DIVB
  {
  int i000, i100, i010, i110, i001, i101, i011, i111;
  double dx4i = 1.0 / ( 4.0 * zinfo.dx );
  double dy4i = 1.0 / ( 4.0 * zinfo.dy );
  double dz4i = 1.0 / ( 4.0 * zinfo.dz );
  double divB=0.0;
  double mxdivB=0.0;
  for( int ix=1; ix <= Fh.nx; ++ix )
  for( int iy=1; iy <= Fh.ny; ++iy )
  for( int iz=1; iz <= Fh.nz; ++iz )
  {
    i000 = Fh.idx( ix,iy,iz );
    i100 = i000 + Fh.nyz2;
    i010 = i000 + Fh.nz2;
    i110 = i010 + Fh.nyz2;
    i001 = i000 + 1;
    i101 = i100 + 1;
    i011 = i010 + 1;
    i111 = i110 + 1;
  
    divB = dx4i* (
            Fh.Bx[i100] + Fh.Bx[i101] + Fh.Bx[i110]  + Fh.Bx[i111]
          - Fh.Bx[i000] - Fh.Bx[i001] - Fh.Bx[i010]  - Fh.Bx[i011] );
    divB += dy4i* (
            Fh.By[i010] + Fh.By[i011] + Fh.By[i110]  + Fh.By[i111]
          - Fh.By[i000] - Fh.By[i001] - Fh.By[i100]  - Fh.By[i101] );
    divB += dz4i* (
            Fh.Bz[i001] + Fh.Bz[i011] + Fh.Bz[i101]  + Fh.Bz[i111]
          - Fh.Bz[i000] - Fh.Bz[i010] - Fh.Bz[i100]  - Fh.Bz[i110] );

    
    if( fabs( divB ) > mxdivB ) mxdivB = fabs( divB );
  }
  
  cout << "Div B (max): " << mxdivB;

  }
#endif

    }
    else
    {
// calculate E based on B half step
      Fh.calcE( qzmoms, pe, resis );

// make E periodic

      Fh.make_E_xyzperiodic();

// smooth E amd make periodic - again!

      Fh.smooth_Edata( smoothfacE );
      
      Fh.make_E_xyzperiodic();

// update E of full step solution
      F.copy_Edata( Fh );

// special
      F.advance_psi( dtsubstep );
      F.make_psi_xyzperiodic();

// advance B of full step solution
      F.advanceB( dtsubstep );

// make B periodic

      F.make_B_xyzperiodic();

    }
  
  } // end of loop over leapfrogging solution

#ifdef DIAG_FIELD_DIVB
  cout << "\n";
#endif

} // end of Hypsim::advanceEB_xyzperiodic

// ==========================================================================

void Hypsim::calc_pe_adiabatic_xyzperiodic( ScalarZarray& pe,
                               QZMoments& zmoms, double Te, double gamma )
{
  int i000;
  
// loop over cells for calculating pe
  for( int ix=1; ix <= pe.nx; ++ix )
  for( int iy=1; iy <= pe.ny; ++iy )
  for( int iz=1; iz <= pe.nz; ++iz )
  {
    i000 = pe.idx( ix,iy,iz );

    pe.V[i000] = Te * pow( zmoms.dn[i000], gamma );
  }

//  cout<<"Z"<<pe.zinfo.izone<<": about to pe.make_xyzperiodic\n";

  pe.make_xyzperiodic();

//  cout<<"Z"<<pe.zinfo.izone<<": done pe.make_xyzperiodic\n";

} // end of Hypsim::calc_pe_adiabatic



// ==========================================================================


}  // end namespace HYPSI
