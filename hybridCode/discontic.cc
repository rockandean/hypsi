#include "hypsi.h"
#include "discontic.h"

namespace HYPSI {

void DiscontIC::read_input_data( ConfigData& config_data )
{

// Start off by reading Uniform Plasma initial conditions

  try{

  uniform_initial_conditions.read_input_data( config_data );

  } catch( HypsiException& e )
  {
    e.push_err_msg(
   "In DiscontIC::read_input_data while reading UNIFORM PLASMA input data" );
    throw e;
  }

  //input parameters
  //double dd_width;
  //int dd_dir;
  vector<double> vxyz_disc;
  vector<double> Bxyz_disc;

  try{
    // this line for each input parameter
    config_data.get_data(
      "discont_initial_conditions/dd_width", _dd_width );   
    config_data.get_data(
      "discont_initial_conditions/dd_direction", _dd_dir );
    config_data.get_data(
      "discont_initial_conditions/Bxyz_disc", Bxyz_disc );
    config_data.get_data( 
      "discont_initial_conditions/Vxyz_disc", vxyz_disc );

  } catch(  KVF::kvfException& e )
  {
    e.diag_cout();
    throw HypsiException(
      "Failed to get Discontinuity IC data from file: <"
         + config_data.filename() + ">",
      "DiscontIC::initialize_from_config_data" );
  }

  // any error handling on input variables

  if( (  _dd_dir < 0 || _dd_dir > 3 ) )
   throw HypsiException(
    "discontinuity_initial_conditions/dd_direction must be 0, 1 or 2 in file: <"
       + config_data.filename() + ">",
   "DiscontIC::initialize_from_config_data" );

  if( Bxyz_disc.size() !=3 )
   throw HypsiException(
    "Insufficient data for discontinuity_initial_conditions/Bxyz_disc in file: <"
       + config_data.filename() + ">",
   "DiscontIC::initialize_from_config_data" );

  _Bvec_disc._x = Bxyz_disc[0];
  _Bvec_disc._y = Bxyz_disc[1];
  _Bvec_disc._z = Bxyz_disc[2];

  if( vxyz_disc.size() !=3 )
   throw HypsiException(
    "Insufficient data for discontinuity_initial_conditions/Vxyz_disc in file: <"
       + config_data.filename() + ">",
   "DiscontIC::initialize_from_config_data" );

  _Vshift_disc._x = vxyz_disc[0];
  _Vshift_disc._y = vxyz_disc[1];
  _Vshift_disc._z = vxyz_disc[2];

} // end DiscontIC::read_input_data


void DiscontIC::initialize_fields_and_particles( Hypsim& hypsim )
{
// initialize UNIFORM PLASMA initial conditions

// uniform B field from calculation above, ie // to k vector

  hypsim.initialize_uniform_B( uniform_initial_conditions._Bvec );
  
  hypsim.initialize_uniform_resis( hypsim.sim_params.uniform_resis );
  hypsim.initialize_pe( hypsim.sim_params.initial_Te );

//cout << "Z" << hypsim.mpi_comm.thisnode << ": disc ic: About to initialize_particle_sets \n";

// cellrandom

  uniform_initial_conditions.initialize_particle_sets( hypsim );

// zonerandom
//  uniform_initial_conditions.initialize_particle_sets_zonerandom( hypsim );

//cout << "Z" << hypsim.mpi_comm.thisnode << ": disc ic: Done initialize_particle_sets \n";

  // Calculate the positions of the interfaces
  int c1, c2;
  double dd_frac;
  if (_dd_dir == 0) {
    c1 =  hypsim.domain_info.nxcell/4;
    c2 =  3*hypsim.domain_info.nxcell/4;
  }
  else if (_dd_dir == 1) {
    c1 =  hypsim.domain_info.nycell/4;
    c2 =  3*hypsim.domain_info.nycell/4;
  }
  else {
    c1 =  hypsim.domain_info.nzcell/4;
    c2 =  3*hypsim.domain_info.nzcell/4;
  }

  // Loop over all psets and particles
  for (int ipset=0; ipset<hypsim.sim_params.npsets; ipset++)
  for (int ip=0; ip<hypsim.psets[ipset]->np; ++ip)
  {
    double xp, yp, zp, vx, vy, vz;
    double pcell;
    xp = hypsim.psets[ipset]->pdata[ip*6]; 
    yp = hypsim.psets[ipset]->pdata[ip*6+1]; 
    zp = hypsim.psets[ipset]->pdata[ip*6+2];
    vx = hypsim.psets[ipset]->pdata[ip*6+3]; 
    vy = hypsim.psets[ipset]->pdata[ip*6+4]; 
    vz = hypsim.psets[ipset]->pdata[ip*6+5];
    
    // Find the cell coordinate of the particle in the direction of the discontinuity normal
    if (_dd_dir == 0) {
      pcell = xp/hypsim.sim_params.dxcell;
    }
    else if (_dd_dir == 1) {
      pcell = yp/hypsim.sim_params.dycell;
    }
    else {
      pcell = zp/hypsim.sim_params.dzcell;
    }

    // Calculate the fraction of V field taken from the discontinuity conditions
    dd_frac = 0.5*(tanh((pcell-c1)/_dd_width)-tanh((pcell-c2)/_dd_width));

    // Adjust the particle velocities based on this fraction, adjusted for the fact that
    // the background Vshift has already been added to the particle velocities in UniformIC
    vx += dd_frac*(_Vshift_disc._x - uniform_initial_conditions._pbimax_vxyzshift[0]);
    vy += dd_frac*(_Vshift_disc._y - uniform_initial_conditions._pbimax_vxyzshift[1]);
    vz += dd_frac*(_Vshift_disc._z - uniform_initial_conditions._pbimax_vxyzshift[2]);
    
    hypsim.psets[ipset]->pdata[ip*6+3] = vx;
    hypsim.psets[ipset]->pdata[ip*6+4] = vy;
    hypsim.psets[ipset]->pdata[ip*6+5] = vz;

  }

// CAN ADD IN THE SHOCK TO THE FIELDS HERE

// LOOP over all cells

  for( int ix = 1; ix <= hypsim.zinfo.nx; ++ix )
  for( int iy = 1; iy <= hypsim.zinfo.ny; ++iy )
  for( int iz = 1; iz <= hypsim.zinfo.nz; ++iz )
  {
    int ixyz;
    int cell;
    
    // Calculate the position of the cell based on the whole domain
    if (_dd_dir == 0) {
      cell = ix+hypsim.zinfo.ixcn*hypsim.zinfo.nx;
    }
    else if (_dd_dir == 1) {
      cell = iy+hypsim.zinfo.iycn*hypsim.zinfo.ny;
    }
    else {
      cell = iz+hypsim.zinfo.izcn*hypsim.zinfo.nz;
    }

    // Calculate the fraction of B field taken from the discontinuity conditions
    dd_frac = 0.5*(tanh((cell-c1)/_dd_width)-tanh((cell-c2)/_dd_width));

    // modify the field values

    ixyz = hypsim.zfields->idx(ix,iy,iz);

    hypsim.zfields->Bx[ ixyz ] = _Bvec_disc._x*dd_frac + (1-dd_frac)*uniform_initial_conditions._Bvec._x;
    hypsim.zfields->By[ ixyz ] = _Bvec_disc._y*dd_frac + (1-dd_frac)*uniform_initial_conditions._Bvec._y;
    hypsim.zfields->Bz[ ixyz ] = _Bvec_disc._z*dd_frac + (1-dd_frac)*uniform_initial_conditions._Bvec._z;
    
    
  }

//cout << "Z" << hypsim.mpi_comm.thisnode << ": disc ic: About to make_B_xyzperiodic \n";

  MPI_Barrier( MPI_COMM_WORLD );

//cout << "Z" << hypsim.mpi_comm.thisnode << ": disc ic: after Barrier: About to make_B_xyzperiodic \n";

  hypsim.zfields->make_B_xyzperiodic();
  
//cout << "Z" << hypsim.mpi_comm.thisnode << ": disc ic: Done make_B_xyzperiodic \n";

  hypsim.zhfields->copy_Bdata( *(hypsim.zfields) );



}


} // end namespace HYPSI
