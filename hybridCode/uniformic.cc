#include "hypsi.h"
#include "alfvenwic.h"
#include "discontic.h"

namespace HYPSI {

void UniformIC::initialize_Bfield( Hypsim& hypsim )
{
  hypsim.initialize_uniform_B( _Bvec ); //!< fill in all matrix values for B (from input file) and E(0,0,0)
}


void UniformIC::read_input_data( ConfigData& config_data )
{
  vector<double> Bxyz_in;

  try{

    config_data.get_data( "particle_sets/num_psets", _npsets );

    config_data.get_data( "particle_sets/initial_allocation_per_node", _pnalloc_per_node );

    config_data.get_data( "particle_sets/initial_active_per_node", _pninit_per_node );

    config_data.get_data( "particle_sets/pset_name", _psname );

    config_data.get_data( "particle_sets/mass_mp", _pmass );

    config_data.get_data( "particle_sets/charge_qp", _pcharge );

    config_data.get_data( "particle_sets/test_particle_flag", _ptpflag );

    //cout << "psname =" << _psname[0] << "\n";

    config_data.get_data( "uniformIC/number_density", _pnden );

    #ifdef DIAG_BIMAX
    config_data.get_data( "uniformIC/vth_par", _pbimax_vthpar );

    config_data.get_data( "uniformIC/vth_perp", _pbimax_vthperp );
    #endif

    #ifdef DIAG_MAXWE
    config_data.get_data( "uniformIC/vthermal", _pmaxwe_vthermal );
    #endif

    config_data.get_data( "uniformIC/Vpar_shift", _pbimax_vparshift );

    config_data.get_data( "uniformIC/Vxyz_shift", _pbimax_vxyzshift );

    config_data.get_data( "uniformIC/Bxyz_vec", Bxyz_in );

  } catch( KVF::kvfException& e )
  {
    e.diag_cout();
    throw HypsiException(
      "Failed to get uniform initial conditions data from file: <" + config_data.filename() + ">",
      "UniformIC::read_input_data" );
  }

  if( _npsets < 1 )
    throw HypsiException(
      "Invalid number of particle sets: in file <" + config_data.filename() + ">",
      "UniformIC::read_input_data" );

  if( _pnalloc_per_node.size() < _npsets )
    throw HypsiException(
      "Inadequate data for uniformIC/particle_sets/initial_allocation_per_node in file: <" + config_data.filename() + ">",
      "UniformIC::read_input_data" );
  if( _pninit_per_node.size() < _npsets )
    throw HypsiException(
      "Inadequate data for uniformIC/particle_sets/initial_active_per_node in file: <" + config_data.filename() + ">",
      "UniformIC::read_input_data" );
  if( _ptpflag.size() < _npsets )
    throw HypsiException(
      "Inadequate data for uniformIC/particle_sets/test_particle_flag: <" + config_data.filename() + ">",
      "UniformIC::read_input_data" );
  if( _psname.size() < _npsets )
    throw HypsiException(
      "Inadequate data for particle_sets/ps_name in file: <" + config_data.filename() + ">",
      "UniformIC::read_input_data" );
  if( _pmass.size() < _npsets )
    throw HypsiException(
      "Inadequate data for particle_sets/mass_mp in file: <" + config_data.filename() + ">",
      "UniformIC::read_input_data" );
  if( _pcharge.size() < _npsets )
    throw HypsiException(
      "Inadequate data for particle_sets/charge_qp in file: <" + config_data.filename() + ">",
      "UniformIC::read_input_data" );

  if( _pnden.size() < _npsets )
    throw HypsiException(
      "Inadequate data for particle_sets/initial/number_density in file: <" + config_data.filename() + ">",
      "UniformIC::read_input_data" );

  #ifdef DIAG_BIMAX
  if( _pbimax_vthpar.size() < _npsets )
    throw HypsiException(
      "Inadequate data for uniformIC/vth_par in file: <" + config_data.filename() + ">",
      "UniformIC::read_input_data" );
  if( _pbimax_vthperp.size() < _npsets )
    throw HypsiException(
      "Inadequate data for uniformIC/vth_perp in file: <" + config_data.filename() + ">",
      "UniformIC::read_input_data" );
  #endif

  #ifdef DIAG_MAXWE
  if( _pmaxwe_vthermal.size() < _npsets )
     throw HypsiException(
       " Inadequate data for uniformIC/vthermal in file: <" + config_data.filename() + ">",
       "UniformIC::read_input_data" );
  #endif

  if( _pbimax_vparshift.size() < _npsets )
    throw HypsiException(
      "Inadequate data for uniformIC/vpar_shift in file: <" + config_data.filename() + ">",
      "UniformIC::read_input_data" );
  if( _pbimax_vxyzshift.size() < 3*_npsets )
    throw HypsiException(
      "Inadequate data for uniformIC/vxyz_shift in file: <" + config_data.filename() + ">",
      "UniformIC::read_input_data" );
  if( Bxyz_in.size() != 3 )
    throw HypsiException(
      "Inadequate data for uniformIC/Bxyz_vec in file: <" + config_data.filename() + ">",
      "UniformIC::read_input_data" );

  //! initialize _Bvec from vector<double> Bxyz_in readed from input file
  _Bvec = Bxyz_in;

// cout << "UniformIC::read_input_data: Bvec: " << _Bvec << "\n";

} // end UniformIC::read_input_data


/*! Set the number of particles sets in simulation (protons, ions, etc), deliver to Simulation Parameters
* and resize accordingly. Initialize the ParticleSet class objects and set() the particle distributions.
* It also declare the argument required by both initialize_uniform_bimax() and initialize_uniform_maxwe()
* Precompiler Flag DIAG_BIMAX or DIAG_MAXWE select between two initialization for speeds.
*/

void UniformIC::initialize_particle_sets( Hypsim& hypsim )
{
  //! set number of particle sets in simulation (set from value in UniformIC)
  hypsim.sim_params.npsets = _npsets;

  //! calculate total number particles in simulation
  _pninit_total_sim.resize( _npsets );

  for( int i=0; i<_npsets; ++i )
  {    _pninit_total_sim[i] = _pninit_per_node[i] * hypsim.sim_params.n_zones;}

  //! resize the particle sets pointer array ...
  hypsim.psets.resize( _npsets );

  //! resize the particle zone moments pointer array for each particle set
  hypsim.pset_pzmoms.resize( _npsets );

  double ncell_domain =
    hypsim.sim_params.nxdomain * hypsim.sim_params.nydomain * hypsim.sim_params.nzdomain;

  double ncell_per_zone_f =
    hypsim.zinfo.nx * hypsim.zinfo.ny * hypsim.zinfo.nz;

  int ncell_per_zone_i =
    hypsim.zinfo.nx * hypsim.zinfo.ny * hypsim.zinfo.nz; //!< This zone(node) number of cells

  //! loop over _npsets or ipset or num_psets.
  for( int i=0; i<_npsets; ++i )
  {
      //! statistical weight for this particle set
      double statw;

      /*! allocate the particle zone moments (for this node's zone)
      - one for each particle set
      - NB Zone must be initialized at this stage
      */
      hypsim.pset_pzmoms[i] = new PmoveZMoments( hypsim.zinfo );

      //! allocate ParticleSet object
      hypsim.psets[i] = new ParticleSet();

      //! statistical weight for this particle set
      statw = _pnden[i] / ( _pninit_per_node[i] / ncell_per_zone_f );

      hypsim.psets[i]->initialize(
                                  _pnalloc_per_node[i],
                                  _pmass[i],
                                  _pcharge[i],
                                  statw,         //!< statistical weight
                                  _ptpflag[i],
                                  _psname [i] );

      //! velocity shift vector
      xyzVector pbimax_vxyzshift_vec;

      pbimax_vxyzshift_vec.set(
          _pbimax_vxyzshift[i*3], _pbimax_vxyzshift[i*3+1], _pbimax_vxyzshift[i*3+2] );

      //! argument required for initialize_uniform_bimax or initialize_uniform_maxwe
      //! This struct is declared in ptcls.h

      ZonePtclIdx pidx_range;

      int intp_pninit_per_cell; //!< intp_pninit_per_cell is number of ptcles per cell for every cell
      int extra_np_inzone;
      /*!< extra_np_inzone is number of "extra" particles that are left after equally distributing cells
       * (to make total up to _pninit_per_node[i]) which are spread randomly
       * over zone. extra_np_inzone will be less than number of cells in zone
      */

      intp_pninit_per_cell = _pninit_per_node[i] / ncell_per_zone_i; //!< integer division
      extra_np_inzone = _pninit_per_node[i] - intp_pninit_per_cell * ncell_per_zone_i;

      //! hypsim.zinfo.izone is the node number of this zone

      #ifdef DIAG_BIMAX
      if( hypsim.zinfo.izone == 0 )
        cout<<"Z"<<hypsim.zinfo.izone<<": about to initialize_uniform_bimax ("
            << intp_pninit_per_cell<<", "<<extra_np_inzone<<")\n";
      #endif

      #ifdef DIAG_MAXWE
      if( hypsim.zinfo.izone == 0 )
        cout<<"Z"<<hypsim.zinfo.izone<<": about to initialize_uniform_maxwe ("
            << intp_pninit_per_cell<<", "<<extra_np_inzone<<")\n";
      #endif

      //! loop over regions
      for( int ix = 0; ix < hypsim.zinfo.nx; ix++ )
      for( int iy = 0; iy < hypsim.zinfo.ny; iy++ )
      for( int iz = 0; iz < hypsim.zinfo.nz; iz++ )
      {

      xyzRegion cell_region;

      cell_region.set( hypsim.zinfo.region.x0 + ix*hypsim.zinfo.dx,
                       hypsim.zinfo.region.x0 + (ix+1)*hypsim.zinfo.dx,
                       hypsim.zinfo.region.y0 + iy*hypsim.zinfo.dy,
                       hypsim.zinfo.region.y0 + (iy+1)*hypsim.zinfo.dy,
                       hypsim.zinfo.region.z0 + iz*hypsim.zinfo.dz,
                       hypsim.zinfo.region.z0 + (iz+1)*hypsim.zinfo.dz );

      #ifdef DIAG_BIMAX
      hypsim.psets[i]->initialize_uniform_bimax( intp_pninit_per_cell,
                                                 &(hypsim.prng),
                                                 cell_region, // only region of cell of this zone
                                                 _pbimax_vthpar[i],
                                                 _pbimax_vthperp[i],
                                                 _Bvec,
                                                 _pbimax_vparshift[i],
                                                 pbimax_vxyzshift_vec,
                                                 pidx_range );
      #endif

      #ifdef DIAG_MAXWE
      hypsim.psets[i]->initialize_uniform_maxwe( intp_pninit_per_cell,
                                                 &(hypsim.prng),
                                                 cell_region,  // only region of cell of this zone
                                                 _pmaxwe_vthermal[i],
                                                 _Bvec,
                                                 _pbimax_vparshift[i],
                                                 pbimax_vxyzshift_vec,
                                                 pidx_range );
      #endif

      } // end loop over regions

      if( extra_np_inzone > 0 )
      {
        #ifdef DIAG_BIMAX
        hypsim.psets[i]->initialize_uniform_bimax( extra_np_inzone,
                                                   &(hypsim.prng),
                                                   hypsim.zinfo.region,   //  zone of this node
                                                   _pbimax_vthpar[i],
                                                   _pbimax_vthperp[i],
                                                   _Bvec,
                                                   _pbimax_vparshift[i],
                                                   pbimax_vxyzshift_vec,
                                                   pidx_range );
        #endif

        #ifdef DIAG_MAXWE
        hypsim.psets[i]->initialize_uniform_maxwe( extra_np_inzone,
                                                   &(hypsim.prng),
                                                   hypsim.zinfo.region,   //  zone of this node
                                                   _pmaxwe_vthermal[i],
                                                   _Bvec,
                                                   _pbimax_vparshift[i],
                                                   pbimax_vxyzshift_vec,
                                                   pidx_range );
        #endif
      }
  }  // end of loop over ipset

} // end UniformIC::initialize_particle_sets


void UniformIC::initialize_particle_sets_zonerandom( Hypsim& hypsim )
{
  //! set number of particle sets in simulation (set from value in UniformIC)
  hypsim.sim_params.npsets = _npsets;

  //! calculate total number particles in simulation
  _pninit_total_sim.resize( _npsets );

  for( int i=0; i<_npsets; ++i )
  { _pninit_total_sim[i] = _pninit_per_node[i] * hypsim.sim_params.n_zones;}

  //! resize the particle sets pointer array ...
  hypsim.psets.resize( _npsets );

  //! resize the particle zone moments pointer array for each particle set
  hypsim.pset_pzmoms.resize( _npsets );

  double ncell_domain =
      hypsim.sim_params.nxdomain * hypsim.sim_params.nydomain * hypsim.sim_params.nzdomain;

  double ncell_per_zone_f =
      hypsim.zinfo.nx * hypsim.zinfo.ny * hypsim.zinfo.nz;

  int ncell_per_zone_i =
      hypsim.zinfo.nx * hypsim.zinfo.ny * hypsim.zinfo.nz;

  for( int i=0; i<_npsets; ++i )
  {
    //! statistical weight for this particle set
    double statw;

    // allocate the particle zone moments (for this node's zone)
    //    - one for each particle set
    // NB Zone must be initialized at this stage!

    hypsim.pset_pzmoms[i] = new PmoveZMoments( hypsim.zinfo );

    // allocate ParticleSet object
    hypsim.psets[i] = new ParticleSet();

    // statistical weight for this particle set
    statw = _pnden[i] / ( _pninit_per_node[i] / ncell_per_zone_f );

    hypsim.psets[i]->initialize(
                                _pnalloc_per_node[i],
                                _pmass[i],
                                _pcharge[i],
                                statw,         // statistical weight
                                _ptpflag[i],
                                _psname [i] );


    // velocity shift vector
    xyzVector pbimax_vxyzshift_vec;

    pbimax_vxyzshift_vec.set(
      _pbimax_vxyzshift[i*3], _pbimax_vxyzshift[i*3+1], _pbimax_vxyzshift[i*3+2] );

    // argument required for initialize_uniform_bimax or initialize_uniform_maxwe

    ZonePtclIdx pidx_range;

    #ifdef DIAG_BIMAX
    if( hypsim.zinfo.izone ==0 )
      cout<<"Z"<<hypsim.zinfo.izone
          <<": about to initialize_uniform_bimax  --- ZONERANDOM!\n";

    hypsim.psets[i]->initialize_uniform_bimax( _pninit_per_node[i],
                                               &(hypsim.prng),
                                               hypsim.zinfo.region,   //  zone of this node
                                               _pbimax_vthpar[i],
                                               _pbimax_vthperp[i],
                                               _Bvec,
                                               _pbimax_vparshift[i],
                                               pbimax_vxyzshift_vec,
                                               pidx_range );
    #endif

    #ifdef DIAG_MAXWE
    if( hypsim.zinfo.izone ==0 )
      cout<<"Z"<<hypsim.zinfo.izone
          <<": about to initialize_uniform_maxwe  --- ZONERANDOM!\n";

    hypsim.psets[i]->initialize_uniform_maxwe( _pninit_per_node[i],
                                               &(hypsim.prng),
                                               hypsim.zinfo.region,   //  zone of this node
                                               _pmaxwe_vthermal[i],
                                               _Bvec,
                                               _pbimax_vparshift[i],
                                               pbimax_vxyzshift_vec,
                                               pidx_range );
    #endif

  }  // end of loop over ipset

} // end UniformIC::initialize_particle_sets_zonerandom

} // end namespace HYPSI
