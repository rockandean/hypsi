/*! \file exception.h
 *
 * \brief Header file for HypsiException class.
 *  David Burgess
 *  September 2006
 */

#ifndef HYPSI_EXCEPTION_H //closed at the end
#define HYPSI_EXCEPTION_H

#include <sstream>
#include <iostream>
#include <cstring>
#include <vector>

namespace HYPSI {

using namespace std;

//! \class Class HypsiException
/*!
 * \brief The HypsiException class.
 */
class HypsiException {
protected:
  bool   _can_recover; ///< Flag indicating recovery possible.
  string _err_str;     ///< Exception error message string.
  string _fn_str;      ///< Function name throwing exception.
  string _type_str;    ///< Exception type string, including inheritance.
  vector<string> _err_msgs;  ///< list of associated messages.
  int    _sys_errno;   ///< system error number from bad system call.
  string _sys_err_str; ///< system error message from bad system call.

public:
  //! Default constructor,
  HypsiException( void )
        : _type_str(" HypsiException:: "){;}

  //! First constructor
  HypsiException( const string& err_str,
                  const string  fn_str="",
                  int   sys_errno=0 )
        : _can_recover( true ),
          _err_str( err_str ),
          _fn_str( fn_str ),
          _type_str( " HypsiException:: " ),
          _sys_errno( sys_errno )
        { if( sys_errno != 0 )  _sys_err_str = strerror( sys_errno );}

  //! Second constructor
  HypsiException( const HypsiException& e )
        : _can_recover( e._can_recover ),
          _err_str( e._err_str ),
          _fn_str( e._fn_str ),
          _type_str( e._type_str ),
          _err_msgs( e._err_msgs ),
          _sys_errno( e._sys_errno ),
          _sys_err_str( e._sys_err_str ){;}

  //! \fn Function append_err_str append str to _err_str.
  void append_err_str( const string& str )
  {  _err_str += str; }

  //! \fn Function prepend_err_str prepend str to _err_str.
  void prepend_err_str( const string& str )
  {  _err_str = str + _err_str; }

  //! \fn Function push_err_msg  push err_msg at end of the vector _err_msgs.
  void push_err_msg( const string& err_msg )
  {  _err_msgs.push_back( err_msg ); }

  //! \fn Print error message ID throw by _fn_str, and full error list.
  void diag_cout()
  {
      cout << _type_str << " " << _err_str << " in function " << _fn_str << endl;

      if( _sys_errno != 0 )
      {
          cout << "sys errno: " << _sys_errno << " [" << _sys_err_str << "]" << endl;
          int n_msgs = _err_msgs.size();
          for( int i = 0; i < n_msgs; ++i )
              cout << "... " << _err_msgs[i] << endl;
      }
   }

};

//! \class Class HypsiMPIException
/*!
 * \brief The HypsiMPIException class inherited from HypsiException.
 */

class HypsiMPIException : public HypsiException
{
protected:
    int _mpi_errcode;

public:
    //! Inherited constructor from HypsiException.
    HypsiMPIException( void ) : HypsiException()
    { _type_str = "HypsiMPIException"; }

    //! first overload constructor
    HypsiMPIException( const string& err_str,
                       const string  fn_str,
                       int   mpi_errcode );

};

} // end namespace HYPSI

#endif
