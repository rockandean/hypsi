// NOTE: This file is included by hypsi.h

#ifndef _HYPSI_PTCLS_H_
#define _HYPSI_PTCLS_H_

#include <string>
#include <algorithm>

#include "rng.h"

namespace HYPSI {


using namespace std;

enum PFState {
  PF_INDOMAIN = 0,
  PF_NOTINDOMAIN = 1,
  PF_LEFTX = 2
};


/*! \brief Particle boundary conditions functor base class
*/
class ParticleBCFunctor {

public:
  ParticleBCFunctor( void ) {;}

  virtual void apply_bc( double* xyzp ) = 0;
  virtual int test_in_domain( double* xyzp ) = 0;
  virtual string type( void ) const = 0;
  virtual void print( ostream& ostrm, string s="" ) const = 0;
};

/*! \brief   X Shock and yz periodic boundary conditions functor class
*/
class XShockBCF : public ParticleBCFunctor {

  double domain_x0, domain_x1, domain_y0, domain_y1, domain_z0, domain_z1;
  double domx, domy, domz;

public:
  XShockBCF( xyzRegion& r ) : ParticleBCFunctor()
  { domain_x0 = r.x0; domain_x1 = r.x1; domx = domain_x1 - domain_x0;
    domain_y0 = r.y0; domain_y1 = r.y1; domy = domain_y1 - domain_y0;
    domain_z0 = r.z0; domain_z1 = r.z1; domz = domain_z1 - domain_z0; }

  int test_in_domain( double* xyzp )
  {
    return (xyzp[0] < domain_x0)? (PF_LEFTX | PF_NOTINDOMAIN) : PF_INDOMAIN;
  }

  void apply_bc( double* xyzp )
  { if( xyzp[0] < domain_x0 )
    {
      // out of domain left - don't do anything here
    }
    else if( xyzp[0] > domain_x1 )
    {
      // out of domain right - reverse motion
      // x folded back into domain & x velocity reversed
      xyzp[0] =  2.0 * domain_x1 - xyzp[0];
      xyzp[3] = - xyzp[3];
    }
    if( xyzp[1] < domain_y0 )
      xyzp[1] += domy;
    else if( xyzp[1] > domain_y1 )
      xyzp[1] -= domy;
    if( xyzp[2] < domain_z0 )
      xyzp[2] += domz;
    else if( xyzp[2] > domain_z1 )
      xyzp[2] -= domz; }
    
  string type( void ) const { return "XShockBCF"; }

  void print( ostream& ostrm, string s="" ) const
  { ostrm << s << "XShockBCF: domain "
     << domain_x0 << "," << domain_x1 << "," << domain_y0 << "," << domain_y1
     << "," << domain_z0 << "," << domain_z1 << " domxyz:"
     << domx << "," << domy << "," << domz << "\n";
  }


};

/*! \brief  Triply Periodic Particle boundary conditions functor class
*/
class PeriodicBCF : public ParticleBCFunctor {

  double domain_x0, domain_x1, domain_y0, domain_y1, domain_z0, domain_z1;
  double domx, domy, domz;

public:
  PeriodicBCF( xyzRegion& r ) : ParticleBCFunctor()
  { domain_x0 = r.x0; domain_x1 = r.x1; domx = domain_x1 - domain_x0;
    domain_y0 = r.y0; domain_y1 = r.y1; domy = domain_y1 - domain_y0;
    domain_z0 = r.z0; domain_z1 = r.z1; domz = domain_z1 - domain_z0; }

  int test_in_domain( double* xyzp )
  {
    return PF_INDOMAIN;
  }

  void apply_bc( double* xyzp )
  { if( xyzp[0] < domain_x0 )
      xyzp[0] += domx;
    else if( xyzp[0] > domain_x1 )
      xyzp[0] -= domx;
    if( xyzp[1] < domain_y0 )
      xyzp[1] += domy;
    else if( xyzp[1] > domain_y1 )
      xyzp[1] -= domy;
    if( xyzp[2] < domain_z0 )
      xyzp[2] += domz;
    else if( xyzp[2] > domain_z1 )
      xyzp[2] -= domz; }
    
  string type( void ) const { return "PeriodicBCF"; }

  void print( ostream& ostrm, string s="" ) const
  { ostrm << s << "PeriodicBCF: domain "
     << domain_x0 << "," << domain_x1 << "," << domain_y0 << "," << domain_y1
     << "," << domain_z0 << "," << domain_z1 << " domxyz:"
     << domx << "," << domy << "," << domz << "\n";
  }

};

/*! \brief Initialization functor base class
*/
class xyzInitFunctor {

public:
  xyzInitFunctor( void ) {;}

  virtual void setxyz( double* xyzp ) = 0;
  virtual string type( void ) const = 0;
};


/*! \brief Initialization functor class for uniform spatial distribution
*/
class xyzUniformRandomIF : public xyzInitFunctor {

  xyzRegion _xyz_region;
  RNG *_rng_p;

public:
  xyzUniformRandomIF( const xyzRegion& xyz_region, RNG* rng_p )
    :  xyzInitFunctor(), _xyz_region( xyz_region ), _rng_p(rng_p) 
  { ; }

  void setxyz( double* xyzp );
  string type( void ) const { return "xyzUniformRandomIF"; }
};

class xyzNullIF : public xyzInitFunctor {
public:

public:
  xyzNullIF( void ) 
    : xyzInitFunctor() { ; }

  void setxyz( double* xyzp ) { cout << "xyzNullIF::setxyz:\n";}
  string type( void ) const { return "xyzNullIF"; }
};

/*! \brief Initialization functor class for bimaxwellian distribution
*/
class vxyzBimaxIF : public xyzInitFunctor {

  double _vthpar, _vthperp;
  xyzVector _Bvec;
  double _vpar_shift;
  xyzVector _vxyz_shift;
  RNG *_rng_p;
  
  double rot[3][3];

public:
  vxyzBimaxIF( double vthpar, double vthperp, const xyzVector& Bvec,
               double vpar_shift, const xyzVector& vxyx_shift,
               RNG* rng_p )
    : xyzInitFunctor(),
      _vthpar(vthpar), _vthperp(vthperp), _Bvec(Bvec),
      _vpar_shift(vpar_shift), _vxyz_shift(vxyx_shift), _rng_p(rng_p)
  {
    double r, theta, phi;
    Bvec.rthetaphi( r, theta, phi );
      
//cout << "vxyzBimaxIF: Bvec.rthetaphi( r, theta, phi ): "
//<< r << "," << theta << ", " << phi << "\n" ;

// rotation uses latitude not colatitude; pi/2 from math.h for double
    double rottheta = 1.57079632679489661923 - theta;
// components of transformation matrix (row,column)
    rot[0][0] = cos( rottheta ) * cos( phi );
    rot[0][1] = -( cos( rottheta ) * sin( phi ) );
    rot[0][2] = sin( rottheta );
    rot[1][0] = sin( phi );
    rot[1][1] = cos( phi );
    rot[1][2] = 0.0;
    rot[2][0] = -( cos( phi ) * sin( rottheta ) );
    rot[2][1] = sin( rottheta ) * sin( phi );
    rot[2][2] = cos( rottheta );

/*     cout << "rot matrix\n"
     << rot[0][0] << "  "<< rot[0][1] << "  "<< rot[0][2] << "\n"
     << rot[1][0] << "  "<< rot[1][1] << "  "<< rot[1][2] << "\n"
     << rot[2][0] << "  "<< rot[2][1] << "  "<< rot[2][2] << "\n";
*/
  }

  void setxyz( double* xyzp );
  string type( void ) const { return "vxyzBimaxIF"; }
};

/*! \brief Initialization functor class for Maxwellian distribution
*/
class vxyzMaxweIF : public xyzInitFunctor {

  double _vthermal;
  xyzVector _Bvec;
  double _vpar_shift;
  xyzVector _vxyz_shift;
  RNG *_rng_p;

  double rot[3][3];

public:
  vxyzMaxweIF( double vthermal, const xyzVector& Bvec,
               double vpar_shift, const xyzVector& vxyx_shift,
               RNG* rng_p )
    : xyzInitFunctor(),
      _vthermal(vthermal), _Bvec(Bvec),
      _vpar_shift(vpar_shift), _vxyz_shift(vxyx_shift), _rng_p(rng_p)
  {
    double r, theta, phi;
    Bvec.rthetaphi( r, theta, phi );

//cout << "vxyzMaxweIF: Bvec.rthetaphi( r, theta, phi ): "
//<< r << "," << theta << ", " << phi << "\n" ;

// rotation uses latitude not colatitude; pi/2 from math.h for double
    double rottheta = 1.57079632679489661923 - theta;
// components of transformation matrix (row,column)
    rot[0][0] = cos( rottheta ) * cos( phi );
    rot[0][1] = -( cos( rottheta ) * sin( phi ) );
    rot[0][2] = sin( rottheta );
    rot[1][0] = sin( phi );
    rot[1][1] = cos( phi );
    rot[1][2] = 0.0;
    rot[2][0] = -( cos( phi ) * sin( rottheta ) );
    rot[2][1] = sin( rottheta ) * sin( phi );
    rot[2][2] = cos( rottheta );

/*     cout << "rot matrix\n"
     << rot[0][0] << "  "<< rot[0][1] << "  "<< rot[0][2] << "\n"
     << rot[1][0] << "  "<< rot[1][1] << "  "<< rot[1][2] << "\n"
     << rot[2][0] << "  "<< rot[2][1] << "  "<< rot[2][2] << "\n";
*/
  }

  void setxyz( double* xyzp );
  string type( void ) const { return "vxyzMaxweIF"; }
};


/*! \brief info about particle indices giving range of particles in zone
*/
struct ZonePtclIdx{
  int first;
  int last;
  int num;
};


/*! \brief Statistics related to a particle set
*/
class ParticleSetStats {
public:
  int np;
  double vx_min, vx_max, vy_min, vy_max, vz_min, vz_max;
  double v_min, v_max;
  double vsq_sum, v_sum, vx_sum, vy_sum, vz_sum;
  double v_mean, vx_mean, vy_mean, vz_mean;

  void print( ostream& ostrm, string s="" ) const
  { ostrm << s << "ParticleSetStats: \n";
    ostrm << s << "v^2 sum = " << vsq_sum << "\n";
    ostrm << s << "Range: vx[" << vx_min << "," << vx_max
          << "] vy[" << vy_min << "," << vy_max
          << "] vz[" << vz_min << "," << vz_max
          << "]   v[" << v_min << "," << v_max << "]\n";
    ostrm << s << "Mean: [" << vx_mean << "," << vy_mean
          << "," << vz_mean << "]  v_mean=" << v_mean << "\n";
  }
  
  void get_output_data( vector<double>& vdminmax, vector<double>& vdmean,
                   double& vsq_sumx )
  {
    vdminmax.resize(8);
    vdminmax[0] = vx_min;  vdminmax[1] = vx_max;
    vdminmax[2] = vy_min;  vdminmax[3] = vy_max;
    vdminmax[4] = vz_min;  vdminmax[5] = vz_max;
    vdminmax[6] = v_min;   vdminmax[7] = v_max;
    vdmean.resize(4);
    vdmean[0] = vx_mean;   vdmean[1] = vy_mean;
    vdmean[2] = vz_mean;   vdmean[3] = v_mean;
    vsq_sumx = vsq_sum;  
  }
};

/*! \brief Set of particles with same q,m etc
  This class can run the bimax and maxwe initialization for a uniform plasma initialization.

*/

class ParticleSet {

  friend class PTAF_pmove;
  friend class Hypsim;
  friend class AlfvenWaveIC;

public:

  int np;      //!< number in set
  int nalloc;  //!< number allocated

  string pset_id; //!< identifying string
  double m;   //!< mass (units of m_p)
  double q;   //!< charge (units of |e|)
  double w;   //!< statistical weight
  double mw;   //!< mass (units of m_p) per particle
  double qw;   //!< charge (units of |e|) per particle
  
  bool testp_flag; //!< treat as test particle population if true
  
  RNG* initf_rng_p;            //!< rng for particl initialization
  xyzInitFunctor* xyz_initf_p;
  xyzInitFunctor* vxyz_initf_p;

  double* pdata; //!< particle data block
  int* pzone;    //!<  particle zone data block (used for sorting)
  
  int* plist; //!< list of ptcle indices for sorting
  vector< struct ZonePtclIdx > zoneidx_list;
  int n_active_zones;

  ParticleSetStats stats;

public:
  ParticleSet( void )
    : np(0), nalloc(0), m(0), q(0), w(0), mw(0), qw(0), testp_flag(0),
      pset_id(""), pdata(0), pzone(0), plist(0), n_active_zones(0) {;}

  ParticleSet( int nn, double mm=1, double qq=1, double ww=1,
               bool testp=false, const string& psname="ions" )
    : np(0), nalloc(nn), m(mm), q(qq), w(ww), testp_flag(testp),
      pset_id(psname)
  { mw = m*w; qw =q*w;
    pdata = new double[nalloc*6];
    pzone = new int[nalloc];
    plist = new int[nalloc]; }
  
  void initialize( int nall, double mm=1, double qq=1, double ww=1,
                   bool testp=false, const string& psname="ions" )
  { np=0; nalloc=nall; m=mm; q=qq; w=ww; testp_flag=testp;
    mw = m*w; qw =q*w;
    pset_id=psname;
    pdata = new double[nalloc*6];
    pzone = new int[nalloc];
    plist = new int[nalloc];
  }

  void add( int npreq, xyzInitFunctor& xyz_initf,
            xyzInitFunctor& vxyz_initf, ZonePtclIdx& pidx_range );

  void initialize_uniform_bimax( int npreq,
                                 RNG* rng_p,
                                 const xyzRegion& xyz_region,
                                 double vthpar, double vthperp,
                                 const xyzVector& Bvec,
                                 double vpar_shift,
                                 const xyzVector& vxyx_shift,
                                 ZonePtclIdx& pidx_range )
  { initf_rng_p = rng_p;
    xyz_initf_p  = new xyzUniformRandomIF( xyz_region, rng_p );
    vxyz_initf_p = new vxyzBimaxIF( vthpar,
                                    vthperp,
                                    Bvec,
                                    vpar_shift,
                                    vxyx_shift,
                                    rng_p );
    add( npreq, *xyz_initf_p, *vxyz_initf_p, pidx_range );
  }

  void initialize_uniform_maxwe( int npreq,
                                 RNG* rng_p,
                                 const xyzRegion& xyz_region,
                                 double vthermal,
                                 const xyzVector& Bvec,
                                 double vpar_shift,
                                 const xyzVector& vxyx_shift,
                                 ZonePtclIdx& pidx_range )
  { initf_rng_p = rng_p;
    xyz_initf_p  = new xyzUniformRandomIF( xyz_region, rng_p );
    vxyz_initf_p = new vxyzMaxweIF( vthermal,
                                    Bvec,
                                    vpar_shift,
                                    vxyx_shift,
                                    rng_p );
    add( npreq, *xyz_initf_p, *vxyz_initf_p, pidx_range );
  }

  void calc_stats( void );
  void get_stats_output_data( vector<double>& vdminmax,
         vector<double>& vdmean, double& vsq_sumx )
  { stats.get_output_data( vdminmax, vdmean, vsq_sumx ); }
  
  int get_np( void ) const { return np; }
  int get_nalloc( void ) const { return nalloc; }

  double get_m( void ) const { return m; }
  double get_q( void ) const { return q; }

  void print( ostream& ostrm, string s="" ) const
  { ostrm << s << "ParticleSet: " << pset_id << " np: " << np
          << " nalloc: " << nalloc
          << " m: " << m
          << " q: " << q
          << " w: " << w
          << " testp: " << testp_flag << " [" << xyz_initf_p->type()
          << "," << vxyz_initf_p->type() << "]\n";
  }

  string type( void ) { return pset_id; }

// 20070815: new implementation, allowing for ptcles out of domain

// NB: particles are in zones as [ ) range

  void sort_by_zone( const Domain& domain, const vector<Zone>& zonesinfo )
  {
    // set up ordered array of indices
    for( int i=0; i<np; ++i )
      plist[i] = i;

//    for( int i=0; i<np; ++i )
//      cout << i << " [" << plist[i] <<"] x: " << pdata[plist[i]*6] << "\n";


// number of zones

   int nz = zonesinfo.size();

// calculate zone number for each particle and store in pzone.
//   use nzones to mark particle which is outside of domain

// note: use closed/open interval [ )

    for( int ip=0; ip<np; ++ip )
    {
      double xp, yp, zp;
      int ix_zn, iy_zn, iz_zn;
      xp = pdata[ip*6]; yp = pdata[ip*6+1]; zp = pdata[ip*6+2];
      if( domain.region.in_region_co( xp, yp, zp ) )
      {
        ix_zn = static_cast<int>( xp/domain.x_znlen );
        iy_zn = static_cast<int>( yp/domain.y_znlen );
        iz_zn = static_cast<int>( zp/domain.z_znlen );
        pzone[ip] = ix_zn*domain.nyz_zn + iy_zn*domain.nz_zn + iz_zn;
      } else
      {
        pzone[ip] = nz;
      }

    }

    sort( plist, plist+np, PtclZoneSortCmp( pzone ) );  //!< uses <algorithm>

/*    cout << "AFTER SORT ..." << endl;

    for( int i=0; i<np; ++i )
      cout << i << " [" << plist[i] <<"] Z: " << pzone[plist[i]]
           << "("<<pdata[plist[i]*6]<<", "<< pdata[plist[i]*6+1]<<", "
           << pdata[plist[i]*6+2]<< ")\n";

*/
     
    zoneidx_list.resize( nz+1 ); // one larger for out of domain set

// assemble zoneidx_list by traversing plist

    int ip = 0;

    for( int iz=0; iz < nz+1; ++iz )
    {
      if( ip == 0 && pzone[plist[ip]] > iz )
      {
        zoneidx_list[iz].last = zoneidx_list[iz].first = -1;
        zoneidx_list[iz].num = 0;
        continue;
      }

      while( ip < np && pzone[plist[ip]] < iz )
        ip++;
      zoneidx_list[iz].first = ip;
      while( ip < np && pzone[plist[ip]] == iz )
        ip++;
      ip--;
      zoneidx_list[iz].last = ip;
      zoneidx_list[iz].num =
        zoneidx_list[iz].last - zoneidx_list[iz].first + 1;

      if( zoneidx_list[iz].last < zoneidx_list[iz].first )
      {
        zoneidx_list[iz].last = zoneidx_list[iz].first = -1;
        zoneidx_list[iz].num = 0;
      }
    
    } // end of zones loop

// count active zones with particles in them
//   nb: don't count the "out of domain" zone!

    n_active_zones = 0;
    for( int iz=0; iz < nz; ++iz )
      if( zoneidx_list[iz].num != 0 )
        n_active_zones++;

/*
    cout << "sort_by_zone: ACTIVE ZONES: "<<  n_active_zones <<" \n";


    cout << "sort_by_zone: Sorted zones\n";
    for( int iz=0; iz < nz; ++iz )
    { if( zoneidx_list[iz].num != 0 )
        cout << iz << ": " << zoneidx_list[iz].first 
           << " --> " << zoneidx_list[iz].last 
           << " [ " << zoneidx_list[iz].num
           << " ] ( " << pzone[plist[zoneidx_list[iz].first]]
           << ", " << pzone[plist[zoneidx_list[iz].last]] << " )\n";
      else
        cout << iz << ": " << zoneidx_list[iz].first 
           << " --> " << zoneidx_list[iz].last 
           << " [ " << zoneidx_list[iz].num
           << " ] ( -, - )\n";
    }
*/


  }

private:
  class PtclZoneSortCmp
  {
    int *pzone;
    public:
    PtclZoneSortCmp( int *pzd ) : pzone(pzd) {;}
    int operator()( const int& a, const int& b) const
      { return pzone[a] < pzone[b]; }  
  };
};


} // end namespace HYPSI


#endif // _HYPSI_PTCLS_H_
