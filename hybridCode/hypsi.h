#ifndef _HYPSI_HYPSI_H_
#define _HYPSI_HYPSI_H_

#include <iostream>

#include "mpi.h"

#include "kvfDataSource.h"

#include "exception.h"
#include "rng.h"
#include "ptthr.h"
#include "timer.h"

#include "zonearray.h"
#include "ptcls.h"
#include "fams.h"
#include "tags.h"

namespace HYPSI {

/* Hypsim forward declaration */

class Hypsim;

// --------------------------------------------------------------------

extern const int TAG_RQ_ZONEFIELDS;
extern const int TAG_ZONEFIELDS;
extern const int TAG_PMOVE_ZMOMENTS;
extern const int TAG_NOPARTICLESINZONE;
extern const int TAG_COLLECTNV_ZMOMENTS;
extern const int TAG_COLLECTNV_T_ZMOMENTS;
extern const int TAG_NNEIGHBOURS_SEND;

// ------------------------------------------------------------------------
    /*! \brief MPI communications class for initialization and MPI identity
    */
class MPIcomm {

public:
  int nnodes;               //!< define MPI comm_sz
  int thisnode;             //!< defnie MPI my_rank

public:
  MPIcomm( void ) {;}
  ~MPIcomm( void )          //!< Destructor of MPI processes, also needed to Finalize process
  {
    cout << "~MPIcomm: " << "process " << thisnode << " is now closed\n";
    MPI_Finalize();
  }

  void initialize( int* argc, char*** argv )
  {
    MPI_Init( argc, argv );
    MPI_Comm_size(MPI_COMM_WORLD,&nnodes);
    MPI_Comm_rank(MPI_COMM_WORLD,&thisnode);
  }

};

// ------------------------------------------------------------------------
/*!
 *\brief Read input file using inherit functions from KVF::kvfDataSource
 */
class ConfigData : public KVF::kvfDataSource {

  string config_fname;
public:
  ConfigData( void ) {;}

  void initialize( int argc, char** argv );
  string filename(void) { return config_fname; }
};

// ------------------------------------------------------------------------
/*!
* \brief The TSOutputControlItem class
*/
class TSOutputControlItem {

friend class TSOutputControl;

public:
  int _ts0;        //!< starting
  int _ts1;        //!< ending
  int _tscadence;  //!< cadence
  string _tag;     //!< tag name associated with this control

  TSOutputControlItem( void ) {;}       //!< default constructor
    /*!
    * \brief Second constructor to initialize all output control variables:
    */
  TSOutputControlItem( const TSOutputControlItem& a )
    : _ts0(a._ts0), _ts1(a._ts1), _tscadence(a._tscadence) {;}

  void set( string tag, int ts0, int ts1, int tscadence )
  { _tag = tag; _ts0 = ts0; _ts1 = ts1; _tscadence = tscadence; }

  string get_tag( void ) const { return _tag; }

  bool test( int ts ) const
  { return ( ts < _ts0 || ts > _ts1 )? false : !((ts - _ts0)%_tscadence); } 
};

// ------------------------------------------------------------------------

class OutputManager;  /* OutputManager forward declaration */

// ------------------------------------------------------------------------
/*!
* \brief The TSOutputControl class
*/
class TSOutputControl {
public:

  int _nitems;
  vector<TSOutputControlItem> _control_list; //!< a vector based on TSOutputControlItem

  TSOutputControl( void ) {;}

  void initialize_from_config_data( ConfigData& config_data );

  void output( OutputManager& outputmgr, int ts );
  
  bool test( const string tag, int ts ) const
  {
    vector<TSOutputControlItem>::const_iterator iter;
    for( iter=_control_list.begin(); iter!=_control_list.end(); ++iter )
    {
      if( iter->_tag.find(tag,0)!= string::npos && iter->test(ts) )
        return true;
    }
    return false;
  }

};
// ------------------------------------------------------------------------
/*!
 * \brief The PDataOutputControl class
 */
class PDataOutputControl {
public:
  int _nsamples;
  vector<TSOutputControlItem> _ts_control_list;
  
  vector< vector<int> > _psets_lists;
  vector<xyzRegion> _region_list;
  vector<int> _nptcle_cadence;
  

  PDataOutputControl( void ) {;}
  
  void initialize_from_config_data( ConfigData& config_data );
  
  void output( OutputManager& outputmgr, int ts );

};
// ------------------------------------------------------------------------
/*!
 *\brief Uniform Initial Conditions class
 * It contains two main classes Bimax, and Maxwe which is current in development.
 * Maxwellian distribution of velocities depend on a random number generator, which is also under development.
 * Current RNG generator is old and maybe slow, also possibly bias.
 * Selection of classes is through Makefile using DIAG_MAXWE or DIAG_BIMAX
 * \todo test for Class Maxwe and RNG are been run , after that delete this comments! l166 hypsi.h
*/
class UniformIC {
public:
  int _npsets;
  vector<int> _ptpflag, _pnalloc_per_node;
  vector<int> _pninit_per_node; //!< active number of particle per node or cpu
  vector<long int> _pninit_total_sim;
  vector<string> _psname;
  vector<double> _pmass;
  vector<double> _pcharge;
  vector<double> _pnden;

  #ifdef DIAG_BIMAX
  vector<double> _pbimax_vthpar;
  vector<double> _pbimax_vthperp;
  #endif

  #ifdef DIAG_MAXWE
  vector<double> _pmaxwe_vthermal;
  #endif

  vector<double> _pbimax_vparshift, _pbimax_vxyzshift;

  xyzVector _Bvec;

  UniformIC( void ) {;}
  
  void read_input_data( ConfigData& config_data );
  void initialize_particle_sets( Hypsim& hypsim );
  void initialize_Bfield( Hypsim& hypsim );
  void initialize_particle_sets_zonerandom( Hypsim& hypsim );

};
// ------------------------------------------------------------------------

/*! \brief Info about global aspects of simulation
*/

class SimulationParameters {
public:

  string output_datafile_root;

  int nxdomain, nydomain, nzdomain; //!< number of cells in xyz domain
  double dxcell, dycell, dzcell;    //!< cell size

  double xdomain, ydomain, zdomain; //!< domain size
  xyzRegion domain_region;          //!< region of total simulation

  int n_zones;                       //!< total number of zones (compute nodes)
  int nx_zones, ny_zones, nz_zones;  //!< number of zones in xyz coords

  int hostzone;  //!< zone index of this zone

  int prng_seed; //!< seed for particle RNG

  int npsets;   //!< Number of particle sets
  
  int ntimesteps; //!< number of time steps for simulation
  
  double dt;     //!< current time step

  int nsubsteps; //!< number of substeps in field solution
  
  double uniform_resis; //!< value for uniform resistivity

  double initial_Te;    //!< initial_electron_temperature

  double electron_gamma;   //!< gamma for adiabatic electron fluid
  
  double smoothfac_EinEBadvance;

  SimulationParameters( void ) : n_zones(0) {;}
  
  void initialize_from_config_data( ConfigData& config_data );
};

// ------------------------------------------------- HypsimCollectnvMsgHandler

class HypsimCollectnvtMsgHandler {

friend class Hypsim;

private:
  int _n_zones;

  int _mpidummy_rq_zonef_recv;

  int* _mpidummy_noptcles_send;
  int  _mpidummy_noptcles_recv;

  bool _nvtmoms_recv_complete;
  bool* _nvtmoms_recv_list;
  int  _nvtmoms_recv_n;

  NVTMoments* _nvtmoms;

  MPI_Request* _noptcls_isend_rq;

  bool* _noptcls_isend_pending;
  bool  _noptcls_isend_all_complete;
  int   _noptcls_isend_pending_n;

  int _n_times_called, _n_times_max_reached;

private:
  HypsimCollectnvtMsgHandler( void );
public:
  HypsimCollectnvtMsgHandler( int ntotal_zones, const Zone& zone )
  {
    _n_zones = ntotal_zones;
    _nvtmoms = new NVTMoments( zone );
    _nvtmoms_recv_list = new bool[ ntotal_zones ];
    _noptcls_isend_pending = new bool[ ntotal_zones ];
    _mpidummy_noptcles_send = new int[ ntotal_zones ];
    _noptcls_isend_rq = new MPI_Request[ ntotal_zones ];
    _n_times_called = 0;
    _n_times_max_reached = 0;
  }

  ~HypsimCollectnvtMsgHandler( void )
  {
    delete _nvtmoms;
    delete[] _nvtmoms_recv_list;
    delete[] _noptcls_isend_pending;
    delete[] _mpidummy_noptcles_send;
    delete[] _noptcls_isend_rq;
  }

  void handle_incoming_messages(
       const string& dstr,
       int max_msg_count,
       NVTMoments* pset_nvtmoms_ptr );

  void initialize( void )
  {
    _n_times_called = 0;
    _n_times_max_reached = 0;
    _noptcls_isend_all_complete = false;
    _noptcls_isend_pending_n = 0;
    _nvtmoms_recv_n = 0;
    _nvtmoms_recv_complete = false;
    for( int iz=0; iz < _n_zones; ++iz )
    {
      _nvtmoms_recv_list[ iz ] = false;
      _noptcls_isend_pending[ iz ] = false;
      _mpidummy_noptcles_send[ iz ] = 123;
    }
  }
};

// ---------------------------------------------------- HypsimPmoveMsgHandler

class HypsimPmoveMsgHandler {
public:

  int n_zones;

  int mpidummy_rq_zonef_recv;

  int* mpidummy_noptcles_send;
  int mpidummy_noptcles_recv;

  MPI_Request* zfrecv_rq_arr;

  int  rq_zonef_answered_n;
  bool* rq_zonef_answered_list;

  bool pzmoms_recv_complete;
  bool* pzmoms_recv_list;
  int  pzmoms_recv_n;

  PmoveZMoments* pzmoms;

  MPI_Request* noptcls_isend_rq;

  bool* noptcls_isend_pending;
  bool noptcls_isend_all_complete;
  int noptcls_isent_pending_n;

  int n_times_called, n_times_max_reached;

private:
  HypsimPmoveMsgHandler( void );
public:
  HypsimPmoveMsgHandler( int ntotal_zones, const Zone& zone )
  {
    n_zones = ntotal_zones;
    pzmoms = new PmoveZMoments(zone);
    rq_zonef_answered_list = new bool[ ntotal_zones ];
    pzmoms_recv_list = new bool[ ntotal_zones ];
    noptcls_isend_pending = new bool[ ntotal_zones ];
    mpidummy_noptcles_send = new int[ ntotal_zones ];
    zfrecv_rq_arr = new MPI_Request[ ntotal_zones ];
    noptcls_isend_rq = new MPI_Request[ ntotal_zones ];
  }

  ~HypsimPmoveMsgHandler( void )
  {
    delete pzmoms;
    delete[] rq_zonef_answered_list;
    delete[] pzmoms_recv_list;
    delete[] noptcls_isend_pending;
    delete[] mpidummy_noptcles_send;
    delete[] zfrecv_rq_arr;
    delete[] noptcls_isend_rq;
  }

  void handle_incoming_messages(
       const string& dstr,
       int max_msg_count,
       PmoveZMoments* pset_pzmoms_ptr,
       ZoneFields* zfields_ptr );

  void initialize( void )
  {
    n_times_called = 0;
    n_times_max_reached = 0;
    noptcls_isend_all_complete = false;
    noptcls_isent_pending_n = 0;
    pzmoms_recv_n = 0;
    pzmoms_recv_complete = false;
    rq_zonef_answered_n = 0;
    for( int iz=0; iz < n_zones; ++iz )
    {
      rq_zonef_answered_list[ iz ] = false;
      pzmoms_recv_list[ iz ] = false;
      noptcls_isend_pending[ iz ] = false;
      mpidummy_noptcles_send[ iz ] = 123;
    }
  }
};

// ------------------------------------------------------------------------

/*! \brief Top level class with universe of the simulation
*/
class Hypsim {
public:

  int ts;

  ConfigData config_data;

  MPIcomm mpi_comm;
  
  Domain domain_info;

  vector<Zone> zones_info;

  Zone zinfo;

  SimulationParameters sim_params;

  RNG prng;

  ZoneFields *zfields;   //!< zfields is the zone fields for E and B
  ZoneFields *zhfields;  //!< zhfields is an half substep fields

  vector<PmoveZMoments*> pset_pzmoms;  //!< moments used in pmove

  vector<NVTMoments*> pset_nvtmoms;  //!< used in collectnv for diagnostics
  
  vector<ParticleSet*> psets;
  
  ParticleBCFunctor* particle_bcf;  //!< boundary condition functor

  QZMoments *mp_qmoms, *ep_qmoms, *ca_qmoms;

  QZMoments *camE_qmoms; //!< used in calc_camE for mixed time level charge moments

  ScalarZarray* pe;
  ScalarZarray* resis;

// ```````````````````````````````````````````````````````````````````

// Message handlers used during PTAFs (particle thread functions)

  HypsimPmoveMsgHandler *pmove_msg_handler;
  HypsimCollectnvtMsgHandler *collectnvt_msg_handler;
  
// ```````````````````````````````````````````````````````````````````
  
  TSOutputControl ts_output_control;
  
  PDataOutputControl pdata_output_control;

// ...................................................................
  
  Hypsim( void ) {;}

  void initialize_config_data( int argc, char** argv )
    { config_data.initialize( argc, argv ); }

  void initialize( int argc, char** argv )
  {
    initialize_config_data( argc, argv );

    sim_params.initialize_from_config_data( config_data );

    sim_params.n_zones = mpi_comm.nnodes;
    sim_params.hostzone = mpi_comm.thisnode;

    if( sim_params.nx_zones * sim_params.ny_zones * sim_params.nz_zones
         != sim_params.n_zones )
      throw HypsiException(
        "compute nodes geometry must match total number of nodes",
        "HypsimTest::initialize" );

    domain_info.initialize(
      sim_params.nx_zones, sim_params.ny_zones, sim_params.nz_zones,
      sim_params.nxdomain, sim_params.nydomain, sim_params.nzdomain,
      sim_params.domain_region );

    initialize_zones_info();

    initialize_rng();

    initialize_zonefields();

    initialize_qmoms();

    ts = 0;

// pmove msg handler functor
    pmove_msg_handler = new HypsimPmoveMsgHandler( sim_params.n_zones, zinfo );

// collect nvtmoms msg handler functor
    collectnvt_msg_handler = new HypsimCollectnvtMsgHandler( 
                                   sim_params.n_zones, zinfo );

  }

  void initialize_zones_info( void );

  void initialize_zonefields( void )
  {
    zfields = new ZoneFields( zinfo );
    zhfields = new ZoneFields( zinfo );
  }

  void initialize_rng( void );
  
  void initialize_qmoms( void )
  {
    mp_qmoms = new QZMoments(zinfo);
    ep_qmoms = new QZMoments(zinfo);
    ca_qmoms = new QZMoments(zinfo);
    camE_qmoms = new QZMoments(zinfo);
  }

  void allocate_pset_nvtmoms( void )
  {
    pset_nvtmoms.resize( sim_params.npsets );
    for( int ipset = 0; ipset < sim_params.npsets; ++ipset )
    {
      pset_nvtmoms[ipset] = new NVTMoments( zinfo );
    }
  }

  void free_pset_nvtmoms( void )
  {
    for( int ipset = 0; ipset < sim_params.npsets; ++ipset )
    {
      delete pset_nvtmoms[ipset];
    }
    pset_nvtmoms.resize( 0 );
  }

  void initialize_pe( double initial_Te )
  {
    pe = new ScalarZarray( zinfo );

    pe->set_uniform( initial_Te ); //!< correct value if uniform density = 1

  }

  void initialize_uniform_resis( double resisval )
  {
    resis = new ScalarZarray( zinfo );
    resis->set_uniform( resisval );
  }

  void initialize_uniform_B( const xyzVector& B ) //!< initialize B and E(0,0,0)
  { // this process fill E with zeros and B acording to the input value B_o
    xyzVector E(0,0,0);
    zfields->set_uniform( E, B );
    zhfields->set_uniform( E, B );
  }

  void initialize_periodic_bc( void )
  {  particle_bcf = new PeriodicBCF( sim_params.domain_region ); }

  void initialize_timestep_output_control( void );

  void pmove( double dt );
  
  void calc_nvq_moments( void );

  void advanceEB_xyzperiodic( ZoneFields& F, ZoneFields& Fh,
                        double dt, int nsubsteps,
                        QZMoments& qzmoms,
                        ScalarZarray& pe, ScalarZarray& resis,
                        double smoothfacE );

  void calc_camE_xyzperiodic( double dt, ZoneFields& F,
                       QZMoments& mp_qzmoms, QZMoments& ep_qzmoms,
                       QZMoments& ca_qzmoms,
                       ScalarZarray& pe, ScalarZarray& resis );

  void calc_pe_adiabatic_xyzperiodic( ScalarZarray& pe,
                               QZMoments& zmoms, double Te, double gamma );

  void collect_nvtmoms( bool collectT );
  void collect_nvtmoms_xyzperiodic( bool collectT );

  void dmsg( string s="" ) const
  { cout << "Z" << sim_params.hostzone << ": " << s << endl; }

};

}  // end namespace HYPSI



#endif // _HYPSI_HYPSI_H_
