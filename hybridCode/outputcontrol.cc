/*! \brief Time step output control
*/ 

#include "hypsi.h"
#include "output.h"
using namespace std;
namespace HYPSI {

void PDataOutputControl::initialize_from_config_data( ConfigData& config_data )
{
  try{
    config_data.get_data( "pdata_output_control/num_samples", _nsamples );
  } catch( KVF::kvfException& e )
  {
    e.diag_cout();
    throw HypsiException(
      "Failed to get num_samples data from file: <" + config_data.filename() + ">",
      "PDataOutputControl::initialize_from_config_data" );
  }
  if( _nsamples < 0 )
    throw HypsiException(
      "Bad value for num_samples in file: <" + config_data.filename() + ">",
      "PDataOutputControl::initialize_from_config_data" );

  if( _nsamples == 0 )
   return;

  vector<int> psetlist;
  vector<double> region_vec;
  int nptcle_cadence;
  vector<int> ts_control_vec;

  _ts_control_list.resize( _nsamples );
  _psets_lists.resize( _nsamples );
  _region_list.resize( _nsamples );
  _nptcle_cadence.resize( _nsamples );

  for( int i=0; i<_nsamples; ++i )
  {
  string tagroot, samplestr;
  stringstream sstr;
  sstr.str(""); sstr<<i;
  tagroot = "pdata_output_control/sample_"+sstr.str()+"/";
  samplestr = "Sample " + sstr.str() + ": ";
  try{
    config_data.get_data( tagroot+"pset_list", psetlist );
    config_data.get_data( tagroot+"region", region_vec );
    config_data.get_data( tagroot+"nth_particle_cadence", nptcle_cadence );
    config_data.get_data( tagroot+"ts_control", ts_control_vec );
  } catch( KVF::kvfException& e )
  {
    e.diag_cout();
    throw HypsiException(
      samplestr+"Failed to get data from file: <" + config_data.filename() + ">",
      "PDataOutputControl::initialize_from_config_data" );
  }
  if( nptcle_cadence < 1 )
    throw HypsiException(
      samplestr+"Bad value for nth_particle_cadence in file: <" + config_data.filename() + ">",
      "PDataOutputControl::initialize_from_config_data" );
  if( psetlist.size() < 1 )
    throw HypsiException(
      samplestr+"Bad size for pset_list in file: <" + config_data.filename() + ">",
      "PDataOutputControl::initialize_from_config_data" );
  if( region_vec.size() != 6 )
    throw HypsiException(
      samplestr+"Incorrect amount of data for region in file: <" + config_data.filename() + ">",
      "PDataOutputControl::initialize_from_config_data" );
  if( ts_control_vec.size() != 3 )
    throw HypsiException(
      samplestr+"Incorrect amount of data for region in file: <" + config_data.filename() + ">",
      "PDataOutputControl::initialize_from_config_data" );

  for( int j=0; j<psetlist.size(); ++j )
    _psets_lists[i].push_back( psetlist[j] );

  _region_list[i].set( region_vec );
  _nptcle_cadence[i] = nptcle_cadence;
  _ts_control_list[i].set( "ParticleData",
    ts_control_vec[0], ts_control_vec[1], ts_control_vec[2] );
 
  } // end loop over samples

} // end PDataOutputControl::initialize_from_config_data

void PDataOutputControl::output( OutputManager& outputmgr, int ts )
{

  for( int i=0; i < _nsamples; ++i )
    if( _ts_control_list[i].test( ts ) )
      outputmgr.output( _ts_control_list[i].get_tag(), ts );

}

// --------------------------------------------------------------------
/*!
 * \brief TSOutputControl::initialize_from_config_data
 * is reading all input from the config file
 * \param config_data is the internal name of the config file
 */

void TSOutputControl::initialize_from_config_data( ConfigData& config_data )
{

  vector<string> taglist;
  vector<int> ts0list;
  vector<int> ts1list;
  vector<int> tscadencelist;

  try{
    //! \warning expected parameters in the config file: \b taglist, \b ts0list, \b ts1list, \b tscadencelist
    config_data.get_data( "ts_output_control/tags", taglist );
    config_data.get_data( "ts_output_control/ts_start", ts0list );
    config_data.get_data( "ts_output_control/end_ts", ts1list );
    config_data.get_data( "ts_output_control/ts_cadence", tscadencelist );
  } catch( KVF::kvfException& e )
  {
    e.diag_cout();
    throw HypsiException(
      "Failed to get initialization data from file: <" + config_data.filename() + ">",
      "TSOutputControl::initialize_from_config_data" );
  }

  _nitems = taglist.size();

  if( ts0list.size() < _nitems || ts1list.size() < _nitems 
      || tscadencelist.size() < _nitems )
    throw HypsiException(
      "Inadequate initialization data (check array sizes!) from file: <" + config_data.filename() + ">",
      "TSOutputControl::initialize_from_config_data" );

  _control_list.resize( _nitems );
  
  for( int i=0; i < _nitems; ++i )
    _control_list[i].set( taglist[i], ts0list[i], ts1list[i], tscadencelist[i] );

}



void TSOutputControl::output( OutputManager& outputmgr, int ts )
{

  for( int i=0; i < _nitems; ++i )
    if( _control_list[i].test( ts ) )
      outputmgr.output( _control_list[i].get_tag(), ts );

}


} // end namespace HYPSI
