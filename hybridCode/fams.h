// file fams.h (fields and moments .. geddit?)

#ifndef _HYPSI_FAMS_H_
#define _HYPSI_FAMS_H_

#include "mpi.h"
#include "zonearray.h"

namespace HYPSI {

// ------------------------------------------------------------------------

class NVTMoments {
public:
  Zone zone;
  int zoid;

  double *F;
  double *Vx, *Vy, *Vz, *dn, *Txx, *Tyy, *Tzz, *Txy, *Txz, *Tyz;

  ScalarZarray Vx_sza, Vy_sza, Vz_sza,
               dn_sza,
               Txx_sza, Tyy_sza, Tzz_sza, Txy_sza, Txz_sza, Tyz_sza;

  int nx, nx1, nx2;
  int ny, ny1, ny2;
  int nz, nz1, nz2;
  int nyz2, nxyz2;
  int nf_nv, nf, fidx_t_start, fidx_t_end;

private:
  NVTMoments( void ) {;}

public:
  NVTMoments( const Zone& zone_in ) : zone( zone_in )
    { 
      nx = zone_in.nx; nx1 = nx+1; nx2 = nx+2;
      ny = zone_in.ny; ny1 = ny+1; ny2 = ny+2;
      nz = zone_in.nz; nz1 = nz+1; nz2 = nz+2;
      nyz2 = ny2*nz2; nxyz2 = nx2 * nyz2;
      zoid = zone_in.izone;
      nf_nv = 4 * nxyz2;
      nf = 10 * nxyz2;
      fidx_t_start =  nf_nv;  // index start of Txx
      fidx_t_end =  nf - 1;   // index end of Tyz

//  cout << "NVTMoments: nx, ny, nz: " << nx << " " << ny << " " << nz << endl;
//  cout << "NVTMoments: about to allocate " << nzf << endl;

      F = new double[ nf ];
      dn = F;
      Vx = F + nxyz2;
      Vy = F + 2*nxyz2;
      Vz = F + 3*nxyz2;
      Txx = F + 4*nxyz2;
      Tyy = F + 5*nxyz2;
      Tzz = F + 6*nxyz2;
      Txy = F + 7*nxyz2;
      Txz = F + 8*nxyz2;
      Tyz = F + 9*nxyz2;
      dn_sza.initialize( zone, dn );
      Vx_sza.initialize( zone, Vx );
      Vy_sza.initialize( zone, Vy );
      Vz_sza.initialize( zone, Vz );
      Txx_sza.initialize( zone, Txx );
      Tyy_sza.initialize( zone, Tyy );
      Tzz_sza.initialize( zone, Tzz );
      Txy_sza.initialize( zone, Txy );
      Txz_sza.initialize( zone, Txz );
      Tyz_sza.initialize( zone, Tyz );
    }
  
  ~NVTMoments( void )
  {
    delete[] F;
  }

  void set_zoneinfo( const Zone& zi )
    { if( !( zi.nx == nx && zi.ny == ny && zi.nz == nz ) )
        throw  HypsiException( "Incommensurate sizes", "NVTMoments::set_zoneinfo()" );
      zone = zi;
      zoid = zi.izone; }

  inline int idx( int ix, int iy, int iz ) const
    { return ix*nyz2 + iy*nz2 + iz; }
  
  void set_zero( void )
    { for( int i=0; i<nf; ++i ) F[i] = 0.0; }
  void set_nv_zero( void )
    { for( int i=0; i<nf_nv; ++i ) F[i] = 0.0; }
  void set_t_zero( void )
    { for( int i=fidx_t_start; i<=fidx_t_end; ++i ) F[i] = 0.0; }

  void addto_nv( const NVTMoments& a )
    { if( !( a.nx == nx && a.ny == ny && a.nz  == nz && a.nf == nf ) )
        throw  HypsiException( "Incommensurate sizes", "NVTMoments::addto_nv()" );
      for( int i=0; i<nf_nv; ++i ) F[i] += a.F[i]; }
  void addto_t( const NVTMoments& a )
    { if( !( a.nx == nx && a.ny == ny && a.nz  == nz && a.nf == nf ) )
        throw  HypsiException( "Incommensurate sizes", "NVTMoments::addto_t()" );
      for( int i=fidx_t_start; i<=fidx_t_end; ++i ) F[i] += a.F[i]; }

  void multiply_nv( double mulfac )
    {  for( int i=0; i<nf_nv; ++i ) F[i] *= mulfac; }
  void multiply_t( double mulfac )
    {  for( int i=fidx_t_start; i<=fidx_t_end; ++i ) F[i] *= mulfac; }
  

  void mpi_send_nv( int dst, int tag );
  void mpi_isend_nv( int dst, int tag, MPI_Request* mpi_request );
  void mpi_recv_nv( int src, int tag );
  void mpi_irecv_nv( int src, int tag, MPI_Request* mpi_request );

  void make_nv_xyzperiodic( void );
};

// ------------------------------------------------------------------------

class PmoveZMoments {
public:
  Zone zinfo;
  int zoid;

  double *F;
  double *Uax, *Uay, *Uaz, *Ubx, *Uby, *Ubz, *dna, *dnb;

  ScalarZarray Uax_sza, Uay_sza, Uaz_sza,
               Ubx_sza, Uby_sza, Ubz_sza, dna_sza, dnb_sza;

  int nx, nx1, nx2;
  int ny, ny1, ny2;
  int nz, nz1, nz2;
  int nyz2, nxyz2, nzf;

private:
  PmoveZMoments( void ) {;}

public:
  PmoveZMoments( const Zone& zi ) : zinfo( zi )
    { nx = zi.nx; nx1 = nx+1; nx2 = nx+2;
      ny = zi.ny; ny1 = ny+1; ny2 = ny+2;
      nz = zi.nz; nz1 = nz+1; nz2 = nz+2;
      nyz2 = ny2*nz2; nxyz2 = nx2 * nyz2;
      zoid = zi.izone;
      nzf = 8 * nxyz2 ;
//  cout << "PmoveZMoments: nx, ny, nz: " << nx << " " << ny << " " << nz << endl;
//  cout << "PmoveZMoments: about to allocate " << nzf << endl;
      F = new double[ nzf ];
      Uax = F;
      Uay = F + nxyz2;
      Uaz = F + 2*nxyz2;
      dna = F + 3*nxyz2;
      Ubx = F + 4*nxyz2;
      Uby = F + 5*nxyz2;
      Ubz = F + 6*nxyz2;
      dnb = F + 7*nxyz2;
      Uax_sza.initialize( zinfo, Uax );
      Uay_sza.initialize( zinfo, Uay );
      Uaz_sza.initialize( zinfo, Uaz );
      Ubx_sza.initialize( zinfo, Ubx );
      Uby_sza.initialize( zinfo, Uby );
      Ubz_sza.initialize( zinfo, Ubz );
      dna_sza.initialize( zinfo, dna );
      dnb_sza.initialize( zinfo, dnb );
    }
  
  ~PmoveZMoments( void )
  {
    delete[] F;
  }

  void set_zoneinfo( const Zone& zi )
    { if( !( zi.nx == nx && zi.ny == ny && zi.nz == nz ) )
        throw  HypsiException( "Incommensurate sizes", "PmoveZMoments::set_zoneinfo()" );
      zinfo = zi;
      zoid = zi.izone; }

  inline int idx( int ix, int iy, int iz ) const
    { return ix*nyz2 + iy*nz2 + iz; }
  
  inline double get_dnb( int ix, int iy, int iz ) const
    { return dnb[ ix*nyz2 + iy*nz2 + iz ]; }

  void set_zero( void )
    { for( int i=0; i<nzf; ++i ) F[i] = 0.0; }

  void add( const PmoveZMoments& a )
    { if( !( a.nx == nx && a.ny == ny && a.nz  == nz && a.nzf == nzf ) )
        throw  HypsiException( "Incommensurate sizes", "PmoveZMoments::add()" );
      for( int i=0; i<nzf; ++i ) F[i] += a.F[i]; }

  void mpi_send( int dst, int tag );
  void mpi_isend( int dst, int tag, MPI_Request* mpi_request );
  void mpi_recv( int src, int tag );
  void mpi_irecv( int src, int tag, MPI_Request* mpi_request );

  void make_xyzperiodic( void );

};

// ------------------------------------------------------------------------
// ------------------------------------------------------------------------

class QZMoments {
public:
  Zone zinfo;
  int zoid;

  double *F;
  double *Ux, *Uy, *Uz, *dn;
  ScalarZarray Ux_sza, Uy_sza, Uz_sza, dn_sza;

  double *smootharr;

  int nx, nx1, nx2;
  int ny, ny1, ny2;
  int nz, nz1, nz2;
  int nyz2, nxyz2, nzf;

private:
  QZMoments( void ) {;}

public:
  QZMoments( const Zone& zi ) : zinfo( zi )
    {
      nx = zi.nx; nx1 = nx+1; nx2 = nx+2;
      ny = zi.ny; ny1 = ny+1; ny2 = ny+2;
      nz = zi.nz; nz1 = nz+1; nz2 = nz+2;
      nyz2 = ny2*nz2; nxyz2 = nx2 * nyz2;
      zoid = zi.izone;
      nzf = 4 * nxyz2 ;

// cout<<"Z"<<zi.izone<< ": QZMoments: nx, ny, nz: " << nx
//      << " " << ny << " " << nz <<" (" << nzf << ")\n";

      F = new double[ nzf ];
      Ux = F;
      Uy = F + nxyz2;
      Uz = F + 2*nxyz2;
      dn = F + 3*nxyz2;
      Ux_sza.initialize( zinfo, Ux );
      Uy_sza.initialize( zinfo, Uy );
      Uz_sza.initialize( zinfo, Uz );
      dn_sza.initialize( zinfo, dn );

// allocate array for smoothing
      smootharr = new double[ nxyz2 ];
    }
  
  ~QZMoments( void )
  {
    delete[] F;
    delete[] smootharr;
  }

  void set_zoneinfo( const Zone& zi )
    { if( !( zi.nx == nx && zi.ny == ny && zi.nz == nz ) )
        throw  HypsiException( "Incommensurate sizes", "QZMoments::set_zoneinfo()" );
      zinfo = zi;
      zoid = zi.izone; }

  inline int idx( int ix, int iy, int iz ) const
    { return ix*nyz2 + iy*nz2 + iz; }
  
  void set_zero( void )
    { for( int i=0; i<nzf; ++i ) F[i] = 0.0; }

  void add( const QZMoments& a )
    { if( !( a.nx == nx && a.ny == ny && a.nz  == nz && a.nzf == nzf ) )
        throw  HypsiException( "Incommensurate sizes", "QZMoments::add()" );
      for( int i=0; i<nzf; ++i ) F[i] += a.F[i]; }

  void addwfac( double fac, const QZMoments& a )
    { if( !( a.nx == nx && a.ny == ny && a.nz  == nz && a.nzf == nzf ) )
        throw  HypsiException( "Incommensurate sizes", "QZMoments::addwfac()" );
      for( int i=0; i<nzf; ++i ) F[i] += fac * a.F[i]; }

  void addwfac_pzmoms_a( double fac, const PmoveZMoments& pzm )
    { if( !( pzm.nx == nx && pzm.ny == ny && pzm.nz  == nz ) )
        throw  HypsiException( "Incommensurate sizes", "QZMoments::addwfac_pzmoms_a()" );
      for( int i=0; i<nzf; ++i ) F[i] += fac * pzm.Uax[i]; }

  void addwfac_pzmoms_b( double fac, const PmoveZMoments& pzm )
    { if( !( pzm.nx == nx && pzm.ny == ny && pzm.nz  == nz ) )
        throw  HypsiException( "Incommensurate sizes", "QZMoments::addwfac_pzmoms_b()" );
      for( int i=0; i<nzf; ++i ) F[i] += fac * pzm.Ubx[i]; }

  void copy_dn_data( const QZMoments& a )
    { if( !( a.nx == nx && a.ny == ny && a.nz  == nz && a.nzf == nzf ) )
        throw  HypsiException( "Incommensurate sizes", "QZMoments::copy_dn_data()" );
      for( int i=0; i<nxyz2; ++i ) dn[i] = a.dn[i]; }

  void copy_U_data( const QZMoments& a )
    { if( !( a.nx == nx && a.ny == ny && a.nz  == nz && a.nzf == nzf ) )
        throw  HypsiException( "Incommensurate sizes", "QZMoments::copy_U_data()" );
      for( int i=0; i<3*nxyz2; ++i ) Ux[i] = a.Ux[i]; }

  void smooth_dn_data( double smoothing )
  { double cntrfac = 1.0 - smoothing; double nneighbfac = smoothing / 6.0;
    for( int i=0; i < nxyz2; ++i )
      smootharr[i] =  dn[i];
    for( int ix = 1; ix <= nx; ++ix )
    for( int iy = 1; iy <= ny; ++iy )
    for( int iz = 1; iz <= nz; ++iz )
    {
      dn[idx(ix,iy,iz)] = cntrfac * smootharr[idx(ix,iy,iz)]
        + nneighbfac * smootharr[idx(ix,iy,iz+1)]
        + nneighbfac * smootharr[idx(ix,iy,iz-1)]
        + nneighbfac * smootharr[idx(ix,iy+1,iz)]
        + nneighbfac * smootharr[idx(ix,iy-1,iz)]
        + nneighbfac * smootharr[idx(ix+1,iy,iz)]
        + nneighbfac * smootharr[idx(ix-1,iy,iz)];
    }
  }

  void smooth_U_data( double smoothing )
  { double cntrfac = 1.0 - smoothing; double nneighbfac = smoothing / 6.0;
    for( int iarr=0; iarr<3; ++iarr )
    {
    double *arrp = Ux + iarr*nxyz2;
    for( int i=0; i < nxyz2; ++i )
      smootharr[i] =  arrp[i];
    for( int ix = 1; ix <= nx; ++ix )
    for( int iy = 1; iy <= ny; ++iy )
    for( int iz = 1; iz <= nz; ++iz )
    {
      arrp[idx(ix,iy,iz)] = cntrfac * smootharr[idx(ix,iy,iz)]
        + nneighbfac * smootharr[idx(ix,iy,iz+1)]
        + nneighbfac * smootharr[idx(ix,iy,iz-1)]
        + nneighbfac * smootharr[idx(ix,iy+1,iz)]
        + nneighbfac * smootharr[idx(ix,iy-1,iz)]
        + nneighbfac * smootharr[idx(ix+1,iy,iz)]
        + nneighbfac * smootharr[idx(ix-1,iy,iz)];
    }
    }
  }

  void mpi_send( int dst, int tag );
  void mpi_isend( int dst, int tag, MPI_Request* mpi_request );
  void mpi_recv( int src, int tag );
  void mpi_irecv( int src, int tag, MPI_Request* mpi_request );

  void make_xyzperiodic( void );

};

// ------------------------------------------------------------------------

class ZoneFieldsStats {
public:
  double Bxyzt_minmax[8];
  double Exyzt_minmax[8];
  double Bxyzt_mean[4];
  double Exyzt_mean[4];
  double Bxyzt_sqsum[4];
  double Exyzt_sqsum[4];
  double Bxyzt_variance[4];
  double Exyzt_variance[4];

};
// ------------------------------------------------------------------------

class ZoneFields {
public:
  Zone zinfo;
  int zoid;

  double *F;
  double *Bx, *By, *Bz, *Ex, *Ey, *Ez;
  ScalarZarray Bx_sza, By_sza, Bz_sza, Ex_sza, Ey_sza, Ez_sza;

  int nx, nx1, nx2;
  int ny, ny1, ny2;
  int nz, nz1, nz2;
  int nxyz, nyz2, nxyz2, nzf;

  double *smootharr;

  double *psi;
  ScalarZarray psi_sza;
  double psi_chsq, psi_cpsq;

  ScalarZarray pe_cv_sza;   //!< pe_cv_sza used in calcE

  ZoneFieldsStats stats;

private:
  ZoneFields( void ) {;}

public:
  ZoneFields( const Zone& zi ) : zinfo( zi )
    { nx = zi.nx; nx1 = nx+1; nx2 = nx+2;
      ny = zi.ny; ny1 = ny+1; ny2 = ny+2;
      nz = zi.nz; nz1 = nz+1; nz2 = nz+2;
      nxyz = nx * ny * nz;
      nyz2 = ny2*nz2; nxyz2 = nx2 * nyz2;
      zoid = zi.izone;
      nzf = 7 * nxyz2;

// cout<<"Z"<<zi.izone<< ": ZoneFields: nx, ny, nz: " << nx
//      << " " << ny << " " << nz <<" (" << nzf << ")\n";

      F = new double[ nzf ];
      Bx = F;
      By = F + nxyz2;
      Bz = F + 2*nxyz2;
      Ex = F + 3*nxyz2;
      Ey = F + 4*nxyz2;
      Ez = F + 5*nxyz2;
      psi = F + 6*nxyz2;

// allocate array for smoothing
      smootharr = new double[ nxyz2 ];

      Bx_sza.initialize( zinfo, Bx );
      By_sza.initialize( zinfo, By );
      Bz_sza.initialize( zinfo, Bz );
      Ex_sza.initialize( zinfo, Ex );
      Ey_sza.initialize( zinfo, Ey );
      Ez_sza.initialize( zinfo, Ez );

      pe_cv_sza.initialize( zinfo );  // let it allocate space

      psi_sza.initialize( zinfo, psi );

      psi_chsq = 0.0;
      psi_cpsq = 0.01;

    }

  ~ZoneFields( void )
  {
    delete[] F;
  }

  void copy_EBdata( const ZoneFields& zf )
    { if( !( zf.nx == nx && zf.ny == ny && zf.nz == nz ) )
        throw  HypsiException( "Incommensurate sizes", "ZoneFields::copy_EBdata()" );
      for( int i=0; i < nzf; ++i )
        F[i] = zf.F[i];  }

  void average_with_EBdata( const ZoneFields& zf )
    { if( !( zf.nx == nx && zf.ny == ny && zf.nz == nz ) )
        throw  HypsiException( "Incommensurate sizes", "ZoneFields::average_with_EBdata()" );
      for( int i=0; i < nzf; ++i )
        F[i] = 0.5*( F[i] + zf.F[i] );  }

  void copy_Edata( const ZoneFields& zf )
    { if( !( zf.nx == nx && zf.ny == ny && zf.nz == nz ) )
        throw  HypsiException( "Incommensurate sizes", "ZoneFields::copy_Edata()" );
      for( int i=0; i < 3*nxyz2; ++i )
        Ex[i] = zf.Ex[i];  }

  void copy_Bdata( const ZoneFields& zf )
    { if( !( zf.nx == nx && zf.ny == ny && zf.nz == nz ) )
        throw  HypsiException( "Incommensurate sizes", "ZoneFields::copy_Bdata()" );
      for( int i=0; i < 3*nxyz2; ++i )
        Bx[i] = zf.Bx[i];  }
  
  void set_zoneinfo( const Zone& zi )
    { if( !( zi.nx == nx && zi.ny == ny && zi.nz == nz ) )
        throw  HypsiException( "Incommensurate sizes", "ZoneFields::set_zoneinfo()" );
      zinfo = zi;
      zoid = zi.izone; }

  inline int idx( int ix, int iy, int iz ) const
    { return ix*nyz2 + iy*nz2 + iz; }
  
  void set_uniform( const xyzVector& E, const xyzVector& B ) //!< fill in the initial values for B and E(0,0,0)
  { for( int ix=0; ix<nx2; ++ix )
    for( int iy=0; iy<ny2; ++iy )
    for( int iz=0; iz<nz2; ++iz )
    { int i=idx(ix,iy,iz);
      Bx[i] = B._x; By[i] = B._y; Bz[i] = B._z;
      Ex[i] = E._x; Ey[i] = E._y; Ez[i] = E._z; } }

  void set_psi_uniform( double psi_value )
  { for( int ix=0; ix<nx2; ++ix )
    for( int iy=0; iy<ny2; ++iy )
    for( int iz=0; iz<nz2; ++iz )
    { int i=idx(ix,iy,iz);
      psi[i] = psi_value; } }
  
  void interpolate( const double* xyzp, xyzVector& E, xyzVector& B,
                    InterpWeights& w );

  void calculate_eweights( const double* xyzp, InterpWeights& w );

  void calcE( QZMoments& qzmoms, ScalarZarray& pe, ScalarZarray& resis );
  void advanceB( double dt );

  void make_E_xyzperiodic( void );
  void make_B_xyzperiodic( void );

  void make_psi_xyzperiodic( void );
  void advance_psi( double dt );

  void calc_stats( void );

  void smooth_Edata( double smoothing )
  { double cntrfac = 1.0 - smoothing; double nneighbfac = smoothing / 6.0;
    for( int iarr=0; iarr<3; ++iarr )
    {
    double *arrp = Ex + iarr*nxyz2;
    for( int i=0; i < nxyz2; ++i )
      smootharr[i] =  arrp[i];
    for( int ix = 1; ix <= nx; ++ix )
    for( int iy = 1; iy <= ny; ++iy )
    for( int iz = 1; iz <= nz; ++iz )
    {
      arrp[idx(ix,iy,iz)] = cntrfac * smootharr[idx(ix,iy,iz)]
        + nneighbfac * smootharr[idx(ix,iy,iz+1)]
        + nneighbfac * smootharr[idx(ix,iy,iz-1)]
        + nneighbfac * smootharr[idx(ix,iy+1,iz)]
        + nneighbfac * smootharr[idx(ix,iy-1,iz)]
        + nneighbfac * smootharr[idx(ix+1,iy,iz)]
        + nneighbfac * smootharr[idx(ix-1,iy,iz)];
    }
    }
  }

  void mpi_send( int dst, int tag );
  void mpi_recv( int src, int tag );
  void mpi_irecv( int src, int tag, MPI_Request* mpi_request );

};

} // end namespace HYPSI

#endif  //   _HYPSI_FAMS_H_
