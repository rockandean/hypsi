#ifndef _HYPSI_HYPSI_H_
#define _HYPSI_HYPSI_H_

#include <iostream>

#include "mpi.h"

#include "kvfDataSource.h"

#include "exception.h"

#include "timer.h"

#include "zonearray.h"

// NB: ptcls.h is included later in this file
// NB: zone.h is included later in this file

namespace HYPSI {

// forward references

class Hypsim;


/*! \brief MPI communications class for initialization and MPI identity
*/
class MPIcomm {

public:
  int nnodes;
  int thisnode, leftnode, rightnode;

public:
  MPIcomm( void ) {;}
  ~MPIcomm( void )
  {
    cout << "~MPIcomm\n";
    MPI_Finalize();
  }

  void initialize( int* argc, char*** argv )
  {
    MPI_Init( argc, argv );
    MPI_Comm_size(MPI_COMM_WORLD,&nnodes);
    MPI_Comm_rank(MPI_COMM_WORLD,&thisnode);
    if( thisnode == 0 )
    {
      leftnode = nnodes - 1;  rightnode = thisnode + 1;
    }
    else if( thisnode == nnodes - 1 )
    {
      leftnode = thisnode - 1;  rightnode = 0;
    } else
    {
      leftnode = thisnode - 1;  rightnode = thisnode + 1;
    }
  }
  
};

// ------------------------------------------------------------------------

class ConfigData : public KVF::kvfDataSource {

  string config_fname;
public:
  ConfigData( void ) {;}

  void initialize( int argc, char** argv );
  string filename(void) { return config_fname; }
};
// ------------------------------------------------------------------------

/*! \brief Info about global aspects of simulation
*/

class SimulationParameters {
public:

  string output_datafile_root;

  double xdomain, ydomain, zdomain;
  int nxdomain, nydomain, nzdomain; //< number of cells in xyz domain
  double dxcell, dycell, dzcell;   //< cell size
  xyzRegion domain_region;  //< region of total simulation

  int n_zones;                         //< total number of zones (compute nodes)
  int nx_zones, ny_zones, nz_zones;  //< number of zones in xyz coords

  int hostzone;  // zone index of this zone

  int prng_seed; //< seed for particle RNG

  int npsets;   //< Number of particle sets
  
  int ntimesteps; //< number of time steps for simulation
  
  double dt;    //< current time step

  int nsubsteps; //< number of substeps in field solution
  
  double uniform_resis; //< value for uniform resistivity

  double initial_Te;   //< initial_electron_temperature

  double electron_gamma;   //< gamma for adiabatic electron fluid
  
  double smoothfac_EinEBadvance;

  SimulationParameters( void ) : n_zones(0) {;}
  
  void initialize_from_config_data( ConfigData& config_data );
};
// ------------------------------------------------------------------------
 

// ------------------------------------------------------------------------

/*! \brief Top level class with universe of the simulation
*/
class HypsimTest {
public:

  int ts;

  ConfigData config_data;

  MPIcomm mpi_comm;
  
  Domain domain_info;

  vector<Zone> zones_info;

  Zone zinfo;

  SimulationParameters sim_params;


  ScalarZarray* scarray;

// ...................................................................
  
  HypsimTest( void ) {;}

  void initialize_config_data( int argc, char** argv )
    { config_data.initialize( argc, argv ); }

  void initialize( int argc, char** argv )
  {

    initialize_config_data( argc, argv );
    sim_params.initialize_from_config_data( config_data );

    sim_params.n_zones = mpi_comm.nnodes;
    sim_params.hostzone = mpi_comm.thisnode;

    if( sim_params.nx_zones * sim_params.ny_zones * sim_params.nz_zones
         != sim_params.n_zones )
      throw HypsiException(
        "compute nodes geometry must match total number of nodes",
        "HypsimTest::initialize" );

    domain_info.initialize(
      sim_params.nx_zones, sim_params.ny_zones, sim_params.ny_zones,
      sim_params.nxdomain, sim_params.nydomain, sim_params.nzdomain,
      sim_params.domain_region );

    initialize_zones_info();

    ts = 0;
  }

  void initialize_zones_info( void );

  void dmsg( string s="" ) const
  { cout << "Z" << sim_params.hostzone << ": " << s << endl; }

};

}  // end namespace HYPSI



#endif // _HYPSI_HYPSI_H_
