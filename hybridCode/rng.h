

#ifndef _HYPSI_RNG_H_
#define _HYPSI_RNG_H_

namespace HYPSI {

class RNG {

 int iseed_initial;
 long idum;

 void psdes(unsigned long *lword, unsigned long *irword);
 float ran2(long *idum);


public:
  RNG( void )
    { int iseed = -2357;
      if( iseed > 0 ) iseed = - iseed;
      iseed_initial = iseed; idum = iseed; ran2( &idum );}

  RNG( int iseed )
    { if( iseed > 0 ) iseed = - iseed;
      iseed_initial = iseed; idum = iseed; ran2( &idum );}
  
  void set_seed( int iseed = -2357 )
    { if( iseed > 0 ) iseed = - iseed;
      iseed_initial = iseed; idum = iseed; ran2( &idum );}

  double operator()( void ) { return ran2( &idum ); }

};


}  // end namespace HYPSI

#endif // _HYPSI_RNG_H_
