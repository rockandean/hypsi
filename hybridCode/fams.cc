
#include <cmath>
#include <iostream>
#include <sstream>

#include "fams.h"

namespace HYPSI {

using namespace std;

// ============================================ Member functions PmoveZMoments

void PmoveZMoments::mpi_recv( int src, int tag )
{
  int mpierr;
  MPI_Status mpi_recv_status;
  mpierr = MPI_Recv( F, nzf, MPI_DOUBLE,
            src,
            tag,
            MPI_COMM_WORLD,
            &mpi_recv_status );
            
  if( mpierr != MPI_SUCCESS )
    throw HypsiMPIException(
    "MPI_Recv failed", "PmoveZMoments::mpi_recv", mpierr);
}

void PmoveZMoments::mpi_irecv( int src, int tag, MPI_Request* mpi_request )
{
  int mpierr;
  mpierr = MPI_Irecv( F, nzf, MPI_DOUBLE,
            src,
            tag,
            MPI_COMM_WORLD,
            mpi_request );
            
  if( mpierr != MPI_SUCCESS )
    throw HypsiMPIException(
    "MPI_Irecv failed", "PmoveZMoments::mpi_irecv", mpierr);
}

void PmoveZMoments::mpi_send( int dst, int tag )
{
  int mpierr;
  mpierr = MPI_Send( F, nzf, MPI_DOUBLE,
            dst,
            tag,
            MPI_COMM_WORLD );

  if( mpierr != MPI_SUCCESS )
    throw HypsiMPIException(
    "MPI_Send failed", "PmoveZMoments::mpi_send", mpierr);
}


void PmoveZMoments::mpi_isend( int dst, int tag, MPI_Request* mpi_request )
{
  int mpierr;
  mpierr = MPI_Isend( F, nzf, MPI_DOUBLE,
            dst,
            tag,
            MPI_COMM_WORLD,
            mpi_request );

  if( mpierr != MPI_SUCCESS )
    throw HypsiMPIException(
    "MPI_Isend failed", "PmoveZMoments::mpi_isend", mpierr);
}

void PmoveZMoments::make_xyzperiodic( void )
{

  Uax_sza.adjust_moment_edges_xyzperiodic();
  Uay_sza.adjust_moment_edges_xyzperiodic();
  Uaz_sza.adjust_moment_edges_xyzperiodic();
  Ubx_sza.adjust_moment_edges_xyzperiodic();
  Uby_sza.adjust_moment_edges_xyzperiodic();
  Ubz_sza.adjust_moment_edges_xyzperiodic();
  dna_sza.adjust_moment_edges_xyzperiodic();
  dnb_sza.adjust_moment_edges_xyzperiodic();

  Uax_sza.make_xyzperiodic();
  Uay_sza.make_xyzperiodic();
  Uaz_sza.make_xyzperiodic();
  Ubx_sza.make_xyzperiodic();
  Uby_sza.make_xyzperiodic();
  Ubz_sza.make_xyzperiodic();
  dna_sza.make_xyzperiodic();
  dnb_sza.make_xyzperiodic();

} // end of PmoveZMoments::make_xyzperiodic

// ================================== Member functions PmoveZMoments  == END

//<><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>

// ============================================= Member functions QZMoments

void QZMoments::mpi_recv( int src, int tag )
{
  int mpierr;
  MPI_Status mpi_recv_status;
  mpierr = MPI_Recv( F, nzf, MPI_DOUBLE,
            src, tag,
            MPI_COMM_WORLD, &mpi_recv_status );
            
  if( mpierr != MPI_SUCCESS )
    throw HypsiMPIException(
    "MPI_Recv failed", "QZMoments::mpi_recv", mpierr);
}

void QZMoments::mpi_irecv( int src, int tag, MPI_Request* mpi_request )
{
  int mpierr;
  mpierr = MPI_Irecv( F, nzf, MPI_DOUBLE,
            src, tag,
            MPI_COMM_WORLD, mpi_request );
            
  if( mpierr != MPI_SUCCESS )
    throw HypsiMPIException(
    "MPI_Irecv failed", "QZMoments::mpi_irecv", mpierr);
}

void QZMoments::mpi_send( int dst, int tag )
{
  int mpierr;
  mpierr = MPI_Send( F, nzf, MPI_DOUBLE,
            dst, tag,
            MPI_COMM_WORLD );

  if( mpierr != MPI_SUCCESS )
    throw HypsiMPIException(
    "MPI_Send failed", "QZMoments::mpi_send", mpierr);
}


void QZMoments::mpi_isend( int dst, int tag, MPI_Request* mpi_request )
{
  int mpierr;
  mpierr = MPI_Isend( F, nzf, MPI_DOUBLE,
            dst, tag,
            MPI_COMM_WORLD, mpi_request );

  if( mpierr != MPI_SUCCESS )
    throw HypsiMPIException(
    "MPI_Isend failed", "QZMoments::mpi_isend", mpierr);
}

//<><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>

void QZMoments::make_xyzperiodic( void )
{

// ATTENTION!!
// this might be imcomplete/half-baked
//
// originally only did the make periodic part, without the
//   edge corrections
// check that it is being used properly!

//cout<<"Z"<<zinfo.izone<<": QZMoments::make_xyzperiodic\n";

  Ux_sza.make_xyzperiodic();
  Uy_sza.make_xyzperiodic();
  Uz_sza.make_xyzperiodic();
  dn_sza.make_xyzperiodic();

} // end of QZMoments::make_xyzperiodic

// ================================== Member functions QZMoments  == END

//<><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>
//<><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>

// ============================================= Member functions NVTMoments

void NVTMoments::mpi_recv_nv( int src, int tag )
{
  int mpierr;
  MPI_Status mpi_recv_status;
  mpierr = MPI_Recv( F, nf_nv, MPI_DOUBLE,
            src, tag,
            MPI_COMM_WORLD, &mpi_recv_status );
            
  if( mpierr != MPI_SUCCESS )
    throw HypsiMPIException(
    "MPI_Recv failed", "NVTMoments::mpi_recv_nv", mpierr);
}

void NVTMoments::mpi_irecv_nv( int src, int tag, MPI_Request* mpi_request )
{
  int mpierr;
  mpierr = MPI_Irecv( F, nf_nv, MPI_DOUBLE,
            src, tag,
            MPI_COMM_WORLD, mpi_request );
            
  if( mpierr != MPI_SUCCESS )
    throw HypsiMPIException(
    "MPI_Irecv failed", "NVTMoments::mpi_irecv_nv", mpierr);
}

void NVTMoments::mpi_send_nv( int dst, int tag )
{
  int mpierr;
  mpierr = MPI_Send( F, nf_nv, MPI_DOUBLE,
            dst, tag,
            MPI_COMM_WORLD );

  if( mpierr != MPI_SUCCESS )
    throw HypsiMPIException(
    "MPI_Send failed", "NVTMoments::mpi_send_nv", mpierr);
}


void NVTMoments::mpi_isend_nv( int dst, int tag, MPI_Request* mpi_request )
{
  int mpierr;
  mpierr = MPI_Isend( F, nf_nv, MPI_DOUBLE,
            dst, tag,
            MPI_COMM_WORLD, mpi_request );

  if( mpierr != MPI_SUCCESS )
    throw HypsiMPIException(
    "MPI_Isend failed", "NVTMoments::mpi_isend_nv", mpierr);
}

//<><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>

void NVTMoments::make_nv_xyzperiodic( void )
{

//cout<<"Z"<<zinfo.izone<<": NVTMoments::make_xyzperiodic\n";

  dn_sza.adjust_moment_edges_xyzperiodic();
  Vx_sza.adjust_moment_edges_xyzperiodic();
  Vy_sza.adjust_moment_edges_xyzperiodic();
  Vz_sza.adjust_moment_edges_xyzperiodic();

  dn_sza.make_xyzperiodic();
  Vx_sza.make_xyzperiodic();
  Vy_sza.make_xyzperiodic();
  Vz_sza.make_xyzperiodic();

} // end of NVTMoments::make_nv_xyzperiodic

// ================================== Member functions NVTMoments  == END

//<><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>

void ZoneFields::mpi_send( int dst, int tag )
{
  int mpierr;
  mpierr = MPI_Send( F, nzf, MPI_DOUBLE,
            dst,
            tag,
            MPI_COMM_WORLD );

  if( mpierr != MPI_SUCCESS )
    throw HypsiMPIException(
    "MPI_Send failed", "ZoneFields::mpi_send", mpierr);
}


void ZoneFields::mpi_recv( int src, int tag )
{
  int mpierr;
  MPI_Status mpi_recv_status;
  mpierr = MPI_Recv( F, nzf, MPI_DOUBLE,
            src,
            tag,
            MPI_COMM_WORLD,
            &mpi_recv_status );
            
  if( mpierr != MPI_SUCCESS )
    throw HypsiMPIException(
    "MPI_Recv failed", "ZoneFields::mpi_recv", mpierr);
}

void ZoneFields::mpi_irecv( int src, int tag, MPI_Request* mpi_request )
{
  int mpierr;
  mpierr = MPI_Irecv( F, nzf, MPI_DOUBLE,
            src,
            tag,
            MPI_COMM_WORLD,
            mpi_request );
            
  if( mpierr != MPI_SUCCESS )
    throw HypsiMPIException(
    "MPI_Irecv failed", "ZoneFields::mpi_irecv", mpierr);
}

//<><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>

void ZoneFields::interpolate( const double* xyzp, xyzVector& E, xyzVector& B,
                              InterpWeights& w )
{
  int ix0, ix1, iy0, iy1, iz0, iz1;
  double wx0, wx1, wy0, wy1, wz0, wz1;

// B grid interpolation
  wx0 = ( xyzp[0] - zinfo.region.x0 )/ zinfo.dx + 1.0;
  ix0 = static_cast<int>( wx0 );
  ix1 = ix0 + 1;
  wx1 = wx0 - ix0;
  wx0 = 1.0 - wx1;
  wy0 = ( xyzp[1] - zinfo.region.y0 )/ zinfo.dy + 1.0;
  iy0 = static_cast<int>( wy0 );
  iy1 = iy0 + 1;
  wy1 = wy0 - iy0;
  wy0 = 1.0 - wy1;
  wz0 = ( xyzp[2] - zinfo.region.z0 )/ zinfo.dz + 1.0;
  iz0 = static_cast<int>( wz0 );
  iz1 = iz0 + 1;
  wz1 = wz0 - iz0;
  wz0 = 1.0 - wz1;
  
  w.bw000 = wx0 * wy0 * wz0;
  w.bw100 = wx1 * wy0 * wz0;
  w.bw010 = wx0 * wy1 * wz0;
  w.bw110 = wx1 * wy1 * wz0;
  w.bw001 = wx0 * wy0 * wz1;
  w.bw101 = wx1 * wy0 * wz1;
  w.bw011 = wx0 * wy1 * wz1;
  w.bw111 = wx1 * wy1 * wz1;

  w.bi000 = idx( ix0, iy0, iz0 );
  w.bi100 = w.bi000 + nyz2;
  w.bi010 = w.bi000 + nz2;
  w.bi110 = w.bi010 + nyz2;
  w.bi001 = w.bi000 + 1;
  w.bi101 = w.bi100 + 1;
  w.bi011 = w.bi010 + 1;
  w.bi111 = w.bi110 + 1;
  
//  cout << ix0 << "  " << ix1 << " "
//  << iy0 << "  " << iy1 << " "
//  << iz0 << "  " << iz1 << endl;
//  
//  cout << i000 << " "
//  << i100 << " "
//  << i010 << " "
//  << i110 << " "
//  << i001 << " "
//  << i101 << " "
//  << i011 << " "
// << i111 << " " << endl;

  if(    w.bi000 < 0 || w.bi000 >= nxyz2 
     ||  w.bi100 < 0 || w.bi100 >= nxyz2
     ||  w.bi010 < 0 || w.bi010 >= nxyz2
     ||  w.bi110 < 0 || w.bi110 >= nxyz2
     ||  w.bi001 < 0 || w.bi001 >= nxyz2
     ||  w.bi101 < 0 || w.bi101 >= nxyz2
     ||  w.bi011 < 0 || w.bi011 >= nxyz2
     ||  w.bi111 < 0 || w.bi111 >= nxyz2 )
   {
     HypsiException e(
      "B Cell index out of bounds", "ZoneFields::interpolate" );
     stringstream s;
     s << "Cell: ix0, iy0, iz0: " << ix0 << ", " << iy0 << ", " << iz0 
       << " x,y,z: " << xyzp[0] << ", " << xyzp[1] << ", "<< xyzp[2];
     e.push_err_msg( s.str() );
 
     throw e;
   }
     

  B._x =  w.bw000 * Bx[ w.bi000 ]
        + w.bw100 * Bx[ w.bi100 ]
        + w.bw010 * Bx[ w.bi010 ]
        + w.bw110 * Bx[ w.bi110 ]
        + w.bw001 * Bx[ w.bi001 ]
        + w.bw101 * Bx[ w.bi101 ]
        + w.bw011 * Bx[ w.bi011 ]
        + w.bw111 * Bx[ w.bi111 ];
  B._y =  w.bw000 * By[ w.bi000 ]
        + w.bw100 * By[ w.bi100 ]
        + w.bw010 * By[ w.bi010 ]
        + w.bw110 * By[ w.bi110 ]
        + w.bw001 * By[ w.bi001 ]
        + w.bw101 * By[ w.bi101 ]
        + w.bw011 * By[ w.bi011 ]
        + w.bw111 * By[ w.bi111 ];
  B._z =  w.bw000 * Bz[ w.bi000 ]
        + w.bw100 * Bz[ w.bi100 ]
        + w.bw010 * Bz[ w.bi010 ]
        + w.bw110 * Bz[ w.bi110 ]
        + w.bw001 * Bz[ w.bi001 ]
        + w.bw101 * Bz[ w.bi101 ]
        + w.bw011 * Bz[ w.bi011 ]
        + w.bw111 * Bz[ w.bi111 ];

// E grid interpolation
  wx0 = ( xyzp[0] - zinfo.region.x0 )/ zinfo.dx + 0.5;
  ix0 = static_cast<int>( wx0 );
  ix1 = ix0 + 1;
  wx1 = wx0 - ix0;
  wx0 = 1.0 - wx1;
  wy0 = ( xyzp[1] - zinfo.region.y0 )/ zinfo.dy + 0.5;
  iy0 = static_cast<int>( wy0 );
  iy1 = iy0 + 1;
  wy1 = wy0 - iy0;
  wy0 = 1.0 - wy1;
  wz0 = ( xyzp[2] - zinfo.region.z0 )/ zinfo.dz + 0.5;
  iz0 = static_cast<int>( wz0 );
  iz1 = iz0 + 1;
  wz1 = wz0 - iz0;
  wz0 = 1.0 - wz1;

  w.ew000 = wx0 * wy0 * wz0;
  w.ew100 = wx1 * wy0 * wz0;
  w.ew010 = wx0 * wy1 * wz0;
  w.ew110 = wx1 * wy1 * wz0;
  w.ew001 = wx0 * wy0 * wz1;
  w.ew101 = wx1 * wy0 * wz1;
  w.ew011 = wx0 * wy1 * wz1;
  w.ew111 = wx1 * wy1 * wz1;

  w.ei000 = idx( ix0, iy0, iz0 );
  w.ei100 = w.ei000 + nyz2;
  w.ei010 = w.ei000 + nz2;
  w.ei110 = w.ei010 + nyz2;
  w.ei001 = w.ei000 + 1;
  w.ei101 = w.ei100 + 1;
  w.ei011 = w.ei010 + 1;
  w.ei111 = w.ei110 + 1;

  if(    w.ei000 < 0 || w.ei000 >= nxyz2 
     ||  w.ei100 < 0 || w.ei100 >= nxyz2
     ||  w.ei010 < 0 || w.ei010 >= nxyz2
     ||  w.ei110 < 0 || w.ei110 >= nxyz2
     ||  w.ei001 < 0 || w.ei001 >= nxyz2
     ||  w.ei101 < 0 || w.ei101 >= nxyz2
     ||  w.ei011 < 0 || w.ei011 >= nxyz2
     ||  w.ei111 < 0 || w.ei111 >= nxyz2 )
     throw HypsiException(
      "E Cell index out of bounds", "ZoneFields::interpolate" );
  
  E._x =  w.ew000 * Ex[ w.ei000 ]
        + w.ew100 * Ex[ w.ei100 ]
        + w.ew010 * Ex[ w.ei010 ]
        + w.ew110 * Ex[ w.ei110 ]
        + w.ew001 * Ex[ w.ei001 ]
        + w.ew101 * Ex[ w.ei101 ]
        + w.ew011 * Ex[ w.ei011 ]
        + w.ew111 * Ex[ w.ei111 ];
  E._y =  w.ew000 * Ey[ w.ei000 ]
        + w.ew100 * Ey[ w.ei100 ]
        + w.ew010 * Ey[ w.ei010 ]
        + w.ew110 * Ey[ w.ei110 ]
        + w.ew001 * Ey[ w.ei001 ]
        + w.ew101 * Ey[ w.ei101 ]
        + w.ew011 * Ey[ w.ei011 ]
        + w.ew111 * Ey[ w.ei111 ];
  E._z =  w.ew000 * Ez[ w.ei000 ]
        + w.ew100 * Ez[ w.ei100 ]
        + w.ew010 * Ez[ w.ei010 ]
        + w.ew110 * Ez[ w.ei110 ]
        + w.ew001 * Ez[ w.ei001 ]
        + w.ew101 * Ez[ w.ei101 ]
        + w.ew011 * Ez[ w.ei011 ]
        + w.ew111 * Ez[ w.ei111 ];

} // end of ZoneFields::interpolate

// ------------------------------------------------- hacked class for testing

class MinMaxatidx
{
public:
  int _ix, _iy, _iz;
  double _v;

  MinMaxatidx(void) : _v(0.0) {;}
  void min( double v, int ix, int iy, int iz )
  { if( v < _v )
    {
      _v = v;
      _ix = ix; _iy = iy; _iz = iz;
    }
  }
  void max( double v, int ix, int iy, int iz )
  { if( v > _v )
    {
      _v = v;
      _ix = ix; _iy = iy; _iz = iz;
    }
  }
};

ostream& operator<<( ostream& os, MinMaxatidx& v )
{ os << v._v << " @(" << v._ix << ","
        << v._iy << "," << v._iz << ")"; return os; }

// -------------------------------------------- END  hacked class for testing

/*


*/
void ZoneFields::calcE( QZMoments& qzmoms,
                        ScalarZarray& pe, ScalarZarray& resis )
{
// cell vertex indices
  int i000, i100, i010, i110, i001, i101, i011, i111;

// B at cell centre
  double Bcc_x, Bcc_y, Bcc_z;

// J cross B 
  double JcB_x, JcB_y, JcB_z;

// grad( pe )
  double gradpe_x, gradpe_y, gradpe_z;

// curl(B)
  double curlB_x, curlB_y, curlB_z;

// curl(B) cross B
  double curlBcB_x, curlBcB_y, curlBcB_z;

//  double min_curlBcB_x=0, max_curlBcB_x=0, min_curlBcB_y=0, max_curlBcB_y=0,
//         min_curlBcB_z=0, max_curlBcB_z=0;
//  MinMaxatidx min_curlB_x, max_curlB_x, min_curlB_y, max_curlB_y,
//         min_curlB_z, max_curlB_z;

// now alloc in ZoneFields object to stop repeated allocation
// electron pressure at cell vertices
//  ScalarZarray pe_cv( zinfo );

  double dx4i = 1.0 / ( 4.0 * zinfo.dx );
  double dy4i = 1.0 / ( 4.0 * zinfo.dy );
  double dz4i = 1.0 / ( 4.0 * zinfo.dz );
  
//cout << "dx4i,dy4i, dz4i  " <<  dx4i << " " << dy4i<< " " << dz4i << "\n";

// interpolate electron pressure to cell vertices
// sets cells with ix: 1 .. nx+1; iy: 1 .. ny+1; iz: 1 .. nz+1
  for( int ix=0; ix <= nx; ++ix )
  for( int iy=0; iy <= ny; ++iy )
  for( int iz=0; iz <= nz; ++iz )
  {
    i000 = idx( ix,iy,iz );
    i100 = i000 + nyz2;
    i010 = i000 + nz2;
    i110 = i010 + nyz2;
    i001 = i000 + 1;
    i101 = i100 + 1;
    i011 = i010 + 1;
    i111 = i110 + 1;

// pe[ ix+1,iy+1,iz+1] = (1/8)(vertex combinations of ix,iy,iz,ix+1,iy+1,iz+1)
    pe_cv_sza.V[ i111 ] 
               = ( pe.V[i000] + pe.V[i100] + pe.V[i010]+ pe.V[i110]
                 + pe.V[i001] + pe.V[i101] + pe.V[i011]+ pe.V[i111] )/ 8.0;
  }

// loop over cells for calculating E
  for( int ix=1; ix <= nx; ++ix )
  for( int iy=1; iy <= ny; ++iy )
  for( int iz=1; iz <= nz; ++iz )
  {
    i000 = idx( ix,iy,iz );
    i100 = i000 + nyz2;
    i010 = i000 + nz2;
    i110 = i010 + nyz2;
    i001 = i000 + 1;
    i101 = i100 + 1;
    i011 = i010 + 1;
    i111 = i110 + 1;

// B at cell centre
    Bcc_x = ( Bx[i000] + Bx[i100] + Bx[i010]+ Bx[i110]
            + Bx[i001] + Bx[i101] + Bx[i011]+ Bx[i111] )/ 8.0;

    Bcc_y = ( By[i000] + By[i100] + By[i010]+ By[i110]
            + By[i001] + By[i101] + By[i011]+ By[i111] )/ 8.0;

    Bcc_z = ( Bz[i000] + Bz[i100] + Bz[i010]+ Bz[i110]
            + Bz[i001] + Bz[i101] + Bz[i011]+ Bz[i111] )/ 8.0;

//cout << "Bcc_x Bcc_y Bcc_z : " << Bcc_x << " "<<Bcc_y << " "<<Bcc_z<< "\n";

// J cross B
    JcB_x = qzmoms.Uy[i000] * Bcc_z  -  qzmoms.Uz[i000] * Bcc_y;
    JcB_y = qzmoms.Uz[i000] * Bcc_x  -  qzmoms.Ux[i000] * Bcc_z;
    JcB_z = qzmoms.Ux[i000] * Bcc_y  -  qzmoms.Uy[i000] * Bcc_x;

// grad pe
    gradpe_x = dx4i* (
            pe_cv_sza.V[i100] + pe_cv_sza.V[i101] + pe_cv_sza.V[i110]  + pe_cv_sza.V[i111]
          - pe_cv_sza.V[i000] - pe_cv_sza.V[i001] - pe_cv_sza.V[i010]  - pe_cv_sza.V[i011] );
    gradpe_y = dy4i* (
            pe_cv_sza.V[i010] + pe_cv_sza.V[i011] + pe_cv_sza.V[i110]  + pe_cv_sza.V[i111]
          - pe_cv_sza.V[i000] - pe_cv_sza.V[i001] - pe_cv_sza.V[i100]  - pe_cv_sza.V[i101] );
    gradpe_z = dz4i* (
            pe_cv_sza.V[i001] + pe_cv_sza.V[i011] + pe_cv_sza.V[i101]  + pe_cv_sza.V[i111]
          - pe_cv_sza.V[i000] - pe_cv_sza.V[i010] - pe_cv_sza.V[i100]  - pe_cv_sza.V[i110] );

// curl B
    curlB_x = 
         dy4i * (  Bz[i010] + Bz[i011] + Bz[i110]  + Bz[i111]
                 - Bz[i000] - Bz[i001] - Bz[i100]  - Bz[i101] )
       - dz4i * (  By[i001] + By[i011] + By[i101]  + By[i111]
                 - By[i000] - By[i010] - By[i100]  - By[i110] );
    curlB_y =
         dz4i * (  Bx[i001] + Bx[i011] + Bx[i101]  + Bx[i111]
                 - Bx[i000] - Bx[i010] - Bx[i100]  - Bx[i110] )
       - dx4i * (  Bz[i100] + Bz[i101] + Bz[i110]  + Bz[i111]
                 - Bz[i000] - Bz[i001] - Bz[i010]  - Bz[i011] );
    curlB_z =
         dx4i * (  By[i100] + By[i101] + By[i110]  + By[i111]
                 - By[i000] - By[i001] - By[i010]  - By[i011] )
       - dy4i * (  Bx[i010] + Bx[i011] + Bx[i110]  + Bx[i111]
                 - Bx[i000] - Bx[i001] - Bx[i100]  - Bx[i101] );

// curl(B) cross B
    curlBcB_x = curlB_y * Bcc_z  - curlB_z * Bcc_y;
    curlBcB_y = curlB_z * Bcc_x  - curlB_x * Bcc_z;
    curlBcB_z = curlB_x * Bcc_y  - curlB_y * Bcc_x;

// TESTING !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
/*
    if( curlBcB_x < min_curlBcB_x ) min_curlBcB_x = curlBcB_x;
    if( curlBcB_x < max_curlBcB_x ) max_curlBcB_x = curlBcB_x;
    if( curlBcB_y < min_curlBcB_y ) min_curlBcB_y = curlBcB_y;
    if( curlBcB_y > max_curlBcB_y ) max_curlBcB_y = curlBcB_y;
    if( curlBcB_z > min_curlBcB_z ) min_curlBcB_z = curlBcB_z;
    if( curlBcB_z > max_curlBcB_z ) max_curlBcB_z = curlBcB_z;

    min_curlB_x.min( curlB_x, ix, iy, iz );
    max_curlB_x.max( curlB_x, ix, iy, iz );
    min_curlB_y.min( curlB_y, ix, iy, iz );
    max_curlB_y.max( curlB_y, ix, iy, iz );
    min_curlB_z.min( curlB_z, ix, iy, iz );
    max_curlB_z.max( curlB_z, ix, iy, iz );
*/
// TESTING !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
/*
    curlBcB_x = 0.0;
    curlBcB_y = 0.0;
    curlBcB_z = 0.0;
    JcB_x = 0.0;
    JcB_y = 0.0;
    JcB_z = 0.0;
*/

// E = (1/rho_c) ( curl(B) cross B - J cross B - grad(pe) + resis curl(B) )
    Ex[i000] = ( curlBcB_x - JcB_x - gradpe_x + resis.V[i000]*curlB_x )
               / qzmoms.dn[i000];
    Ey[i000] = ( curlBcB_y - JcB_y - gradpe_y + resis.V[i000]*curlB_y )
               / qzmoms.dn[i000];
    Ez[i000] = ( curlBcB_z - JcB_z - gradpe_z + resis.V[i000]*curlB_z )
               / qzmoms.dn[i000];

  } //end of loop over E cells

/*
cout << "min_curlBcB_x,min_curlBcB_y, min_curlBcB_z  " << 
min_curlBcB_x << " " << min_curlBcB_y<< " " << min_curlBcB_z <<"\n";
cout << "max_curlBcB_x,max_curlBcB_y, max_curlBcB_z  " << 
max_curlBcB_x << " " << max_curlBcB_y<< " " << max_curlBcB_z <<"\n";

cout << "min_curlB_x,min_curlB_y, min_curlB_z  " << 
min_curlB_x << " " << min_curlB_y << " " << min_curlB_z <<"\n";
cout << "max_curlB_x,max_curlB_y, max_curlB_z  " << 
max_curlB_x << " " << max_curlB_y<< " " << max_curlB_z <<"\n";
*/

} // end of ZoneFields::calcE

// -----------------------------------------------------------------------

void ZoneFields::advance_psi( double dt )
{
// cell vertex indices
  int i000, i100, i010, i110, i001, i101, i011, i111;

  double dx4i = 1.0 / ( 4.0 * zinfo.dx );
  double dy4i = 1.0 / ( 4.0 * zinfo.dy );
  double dz4i = 1.0 / ( 4.0 * zinfo.dz );

  double divB;

  for( int ix=1; ix <= nx; ++ix )
  for( int iy=1; iy <= ny; ++iy )
  for( int iz=1; iz <= nz; ++iz )
  {
    i000 = idx( ix,iy,iz );
    i100 = i000 + nyz2;
    i010 = i000 + nz2;
    i110 = i010 + nyz2;
    i001 = i000 + 1;
    i101 = i100 + 1;
    i011 = i010 + 1;
    i111 = i110 + 1;

    divB = dx4i* (
            Bx[i100] + Bx[i101] + Bx[i110]  + Bx[i111]
          - Bx[i000] - Bx[i001] - Bx[i010]  - Bx[i011] );
    divB += dy4i* (
            By[i010] + By[i011] + By[i110]  + By[i111]
          - By[i000] - By[i001] - By[i100]  - By[i101] );
    divB += dz4i* (
            Bz[i001] + Bz[i011] + Bz[i101]  + Bz[i111]
          - Bz[i000] - Bz[i010] - Bz[i100]  - Bz[i110] );

    psi[i000] -= dt * ( psi_chsq * divB + psi_chsq * psi[i000] / psi_cpsq );
  }
}
/*

*/
void ZoneFields::advanceB( double dt )
{
// cell vertex indices
  int i000, i100, i010, i110, i001, i101, i011, i111;

  double dtdx4i = dt / ( 4.0 * zinfo.dx );
  double dtdy4i = dt / ( 4.0 * zinfo.dy );
  double dtdz4i = dt / ( 4.0 * zinfo.dz );

//cout << "dtdx4i,dtdy4i, dtdz4i  " <<  dtdx4i << " " << dtdy4i<< " " << dtdz4i << "\n";

// loop over cells for calculating B

// Note the offsetting of index into array so that selected
// E cells (cell centred) are centred on B cell (at cell origin)

  for( int ix=1; ix <= nx; ++ix )
  for( int iy=1; iy <= ny; ++iy )
  for( int iz=1; iz <= nz; ++iz )
  {
    i000 = idx( ix-1,iy-1,iz-1 );
    i100 = i000 + nyz2;
    i010 = i000 + nz2;
    i110 = i010 + nyz2;
    i001 = i000 + 1;
    i101 = i100 + 1;
    i011 = i010 + 1;
    i111 = i110 + 1;

// B = B - dt * curl( E )
    Bx[i111] -=
        dtdy4i * (  Ez[i010] + Ez[i011] + Ez[i110]  + Ez[i111]
                  - Ez[i000] - Ez[i001] - Ez[i100]  - Ez[i101] )
      - dtdz4i * (  Ey[i001] + Ey[i011] + Ey[i101]  + Ey[i111]
                  - Ey[i000] - Ey[i010] - Ey[i100]  - Ey[i110] );
    By[i111] -=
        dtdz4i * (  Ex[i001] + Ex[i011] + Ex[i101]  + Ex[i111]
                  - Ex[i000] - Ex[i010] - Ex[i100]  - Ex[i110] )
      - dtdx4i * (  Ez[i100] + Ez[i101] + Ez[i110]  + Ez[i111]
                  - Ez[i000] - Ez[i001] - Ez[i010]  - Ez[i011] );
    Bz[i111] -=
        dtdx4i * (  Ey[i100] + Ey[i101] + Ey[i110]  + Ey[i111]
                  - Ey[i000] - Ey[i001] - Ey[i010]  - Ey[i011]  )
      - dtdy4i * (  Ex[i010] + Ex[i011] + Ex[i110]  + Ex[i111]
                  - Ex[i000] - Ex[i001] - Ex[i100]  - Ex[i101]  );

    Bx[i111] -=
        dtdx4i * (  psi[i100] + psi[i101] + psi[i110]  + psi[i111]
                  - psi[i000] - psi[i001] - psi[i010]  - psi[i011] );
    By[i111] -=
        dtdy4i * (  psi[i010] + psi[i011] + psi[i110]  + psi[i111]
                  - psi[i000] - psi[i001] - psi[i100]  - psi[i101] );
    Bz[i111] -=
        dtdz4i * (  psi[i001] + psi[i011] + psi[i101]  + psi[i111]
                  - psi[i000] - psi[i010] - psi[i100]  - psi[i110] );

  } // end of loop over B cells

} // end of ZoneFields::calcB

// -----------------------------------------------------------------------

void ZoneFields::make_E_xyzperiodic( void )
{

  Ex_sza.make_xyzperiodic();
  Ey_sza.make_xyzperiodic();
  Ez_sza.make_xyzperiodic();

} // end of ZoneFields::make_E_xyzperiodic

// -----------------------------------------------------------------------

void ZoneFields::make_B_xyzperiodic( void )
{

//cout << "ZoneFields::make_B_xyzperiodic X\n";

  Bx_sza.make_xyzperiodic();

//cout << "ZoneFields::make_B_xyzperiodic Y\n";

  By_sza.make_xyzperiodic();

//cout << "ZoneFields::make_B_xyzperiodic Z\n";

  Bz_sza.make_xyzperiodic();

//cout << "ZoneFields::make_B_xyzperiodic  END\n";

} // end of ZoneFields::make_B_xyzperiodic

// -----------------------------------------------------------------------

void ZoneFields::make_psi_xyzperiodic( void )
{

  psi_sza.make_xyzperiodic();

} // end of ZoneFields::make_psi_xyzperiodic

//<><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>

void ZoneFields::calc_stats( void )
{

  for( int i=0 ; i < 4 ; ++i )
  {
    stats.Bxyzt_mean[i] = 0;     stats.Exyzt_mean[i] = 0;
    stats.Bxyzt_sqsum[i] = 0;    stats.Exyzt_sqsum[i] = 0;
    stats.Bxyzt_variance[i] = 0; stats.Exyzt_variance[i] = 0;
  }

  stats.Bxyzt_minmax[0] = 1000000; stats.Bxyzt_minmax[1] = -1000000;
  stats.Bxyzt_minmax[2] = 1000000; stats.Bxyzt_minmax[3] = -1000000;
  stats.Bxyzt_minmax[4] = 1000000; stats.Bxyzt_minmax[5] = -1000000;
  stats.Bxyzt_minmax[6] = 1000000; stats.Bxyzt_minmax[7] = -1000000;
  stats.Exyzt_minmax[0] = 1000000; stats.Exyzt_minmax[1] = -1000000;
  stats.Exyzt_minmax[2] = 1000000; stats.Exyzt_minmax[3] = -1000000;
  stats.Exyzt_minmax[4] = 1000000; stats.Exyzt_minmax[5] = -1000000;
  stats.Exyzt_minmax[6] = 1000000; stats.Exyzt_minmax[7] = -1000000;


  for( int ix=1; ix <= nx; ++ix )
  for( int iy=1; iy <= ny; ++iy )
  for( int iz=1; iz <= nz; ++iz )
  {
    int i = idx( ix, iy, iz );
    double Bt = sqrt( Bx[i]*Bx[i] + By[i]*By[i] + Bz[i]*Bz[i] );
    double Et = sqrt( Ex[i]*Ex[i] + Ey[i]*Ey[i] + Ez[i]*Ez[i] );

    if( Bx[i] < stats.Bxyzt_minmax[0] ) stats.Bxyzt_minmax[0] = Bx[i];
    if( By[i] < stats.Bxyzt_minmax[2] ) stats.Bxyzt_minmax[2] = By[i];
    if( Bz[i] < stats.Bxyzt_minmax[4] ) stats.Bxyzt_minmax[4] = Bz[i];
    if( Bt    < stats.Bxyzt_minmax[6] ) stats.Bxyzt_minmax[6] = Bt;
    if( Bx[i] > stats.Bxyzt_minmax[1] ) stats.Bxyzt_minmax[1] = Bx[i];
    if( By[i] > stats.Bxyzt_minmax[3] ) stats.Bxyzt_minmax[3] = By[i];
    if( Bz[i] > stats.Bxyzt_minmax[5] ) stats.Bxyzt_minmax[5] = Bz[i];
    if( Bt    > stats.Bxyzt_minmax[7] ) stats.Bxyzt_minmax[7] = Bt;
    if( Ex[i] < stats.Exyzt_minmax[0] ) stats.Exyzt_minmax[0] = Ex[i];
    if( Ey[i] < stats.Exyzt_minmax[2] ) stats.Exyzt_minmax[2] = Ey[i];
    if( Ez[i] < stats.Exyzt_minmax[4] ) stats.Exyzt_minmax[4] = Ez[i];
    if( Et    < stats.Exyzt_minmax[6] ) stats.Exyzt_minmax[6] = Et;
    if( Ex[i] > stats.Exyzt_minmax[1] ) stats.Exyzt_minmax[1] = Ex[i];
    if( Ey[i] > stats.Exyzt_minmax[3] ) stats.Exyzt_minmax[3] = Ey[i];
    if( Ez[i] > stats.Exyzt_minmax[5] ) stats.Exyzt_minmax[5] = Ez[i];
    if( Et    > stats.Exyzt_minmax[7] ) stats.Exyzt_minmax[7] = Et;

    stats.Bxyzt_mean[0] += Bx[i];  stats.Exyzt_mean[0] += Ex[i];
    stats.Bxyzt_mean[1] += By[i];  stats.Exyzt_mean[1] += Ey[i];
    stats.Bxyzt_mean[2] += Bz[i];  stats.Exyzt_mean[2] += Ez[i];
    stats.Bxyzt_mean[3] += Bt;     stats.Exyzt_mean[3] += Et;

    stats.Bxyzt_sqsum[0] += Bx[i]*Bx[i];  stats.Exyzt_sqsum[0] += Ex[i]*Ex[i];
    stats.Bxyzt_sqsum[1] += By[i]*By[i];  stats.Exyzt_sqsum[1] += Ey[i]*Ey[i];
    stats.Bxyzt_sqsum[2] += Bz[i]*Bz[i];  stats.Exyzt_sqsum[2] += Ez[i]*Ez[i];
    stats.Bxyzt_sqsum[3] += Bt*Bt;        stats.Exyzt_sqsum[3] += Et*Et;
  }

  stats.Bxyzt_mean[0] /= nxyz;  stats.Exyzt_mean[0] /= nxyz;
  stats.Bxyzt_mean[1] /= nxyz;  stats.Exyzt_mean[1] /= nxyz;
  stats.Bxyzt_mean[2] /= nxyz;  stats.Exyzt_mean[2] /= nxyz;
  stats.Bxyzt_mean[3] /= nxyz;  stats.Exyzt_mean[3] /= nxyz;
  for( int ix=1; ix <= nx; ++ix )
  for( int iy=1; iy <= ny; ++iy )
  for( int iz=1; iz <= nz; ++iz )
  {
    int i = idx( ix, iy, iz );
    double Bt = sqrt( Bx[i]*Bx[i] + By[i]*By[i] + Bz[i]*Bz[i] );
    double Et = sqrt( Ex[i]*Ex[i] + Ey[i]*Ey[i] + Ez[i]*Ez[i] );

    stats.Bxyzt_variance[0] += (Bx[i]-stats.Bxyzt_mean[0])*(Bx[i]-stats.Bxyzt_mean[0]);
    stats.Bxyzt_variance[1] += (By[i]-stats.Bxyzt_mean[1])*(By[i]-stats.Bxyzt_mean[1]);
    stats.Bxyzt_variance[2] += (Bz[i]-stats.Bxyzt_mean[2])*(Bz[i]-stats.Bxyzt_mean[2]);
    stats.Bxyzt_variance[3] += (Bt-stats.Bxyzt_mean[3])*(Bt-stats.Bxyzt_mean[3]);
    stats.Exyzt_variance[0] += (Ex[i]-stats.Exyzt_mean[0])*(Ex[i]-stats.Exyzt_mean[0]);
    stats.Exyzt_variance[1] += (Ey[i]-stats.Exyzt_mean[1])*(Ey[i]-stats.Exyzt_mean[1]);
    stats.Exyzt_variance[2] += (Ez[i]-stats.Exyzt_mean[2])*(Ez[i]-stats.Exyzt_mean[2]);
    stats.Exyzt_variance[3] += (Et-stats.Exyzt_mean[3])*(Et-stats.Exyzt_mean[3]); 
  }
  stats.Bxyzt_variance[0] /= nxyz;  stats.Exyzt_variance[0] /= nxyz;
  stats.Bxyzt_variance[1] /= nxyz;  stats.Exyzt_variance[1] /= nxyz;
  stats.Bxyzt_variance[2] /= nxyz;  stats.Exyzt_variance[2] /= nxyz;
  stats.Bxyzt_variance[3] /= nxyz;  stats.Exyzt_variance[3] /= nxyz;

} // end of ZoneFieldsStats::calc_stats

}  // end namespace HYPSI
