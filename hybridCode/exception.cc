/*! \file exception.cc
 * Declaration of inherit constructor for MPIException class.
 */

#include "exception.h"
#include "mpi.h"

namespace HYPSI {

using namespace std;

//! Declare inherit constructor HypsiMPIException:HypsiException and initialize _mpi_errcode, see exception.h.
HypsiMPIException::HypsiMPIException( 
                                    const string& err_str,
                                    const string fn_str,
                                    int mpi_errcode )
    : HypsiException( err_str, fn_str, 0 ), _mpi_errcode( mpi_errcode )
    { _type_str = "HypsiMPIException::";
        if( mpi_errcode != MPI_SUCCESS )  {
            char error_string[BUFSIZ];
            int length_of_error_string, error_class;

            MPI_Error_class(mpi_errcode, &error_class);
            MPI_Error_string(error_class, error_string, &length_of_error_string);
//          fprintf(stderr, "%3d: %s\n", my_rank, error_string);
            push_err_msg( error_string );

            MPI_Error_string(mpi_errcode, error_string, &length_of_error_string);
//          fprintf(stderr, "%3d: %s\n", my_rank, error_string);
            push_err_msg( error_string );
        }
    }
} // end namespace HYPSI

