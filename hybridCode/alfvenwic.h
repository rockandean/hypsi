#ifndef _HYPSI_ALVENWIC_H_
#define _HYPSI_ALVENWIC_H_

#include <complex>
#include <string>
#include <fstream>
#include <iostream>
#include <sstream>

namespace HYPSI {

typedef std::complex<double> complex;

class AlfvenWaveIC {
public:

  int _mx, _my, _mz;     // wave mode numbers
  
  int _propsign;  //< +1 for B // k, -1 for opposite
  
  complex _aplus_amplitude, _aminus_amplitude;

  xyzVector _kvec;     //< wave vector
  
  xyzVector _Bhatvec;   //< unit vector B direction
  xyzVector _Bperp1vec;
  xyzVector _Bperp2vec;
  
  UniformIC uniform_initial_conditions;

  AlfvenWaveIC( void ) {;}
  
  void read_input_data( ConfigData& config_data );

  void initialize_fields_and_particles( Hypsim& hypsim );

  void output_params( ostream& ostrm, string s="" ) const
  { 
    ostrm<<s<<"AlfvenIC: k vector" << _kvec << " Propagation sign: " << _propsign 
          << " LH: "<< _aplus_amplitude<<" RH: " << _aminus_amplitude << "\n";
    ostrm<<s<<"AlfvenIC: Bpar : " << _Bhatvec << " Bper1: " << _Bperp1vec
       << " Bper2: " << _Bperp2vec << "\n";
  }


};


}   // end namespace HYPSI

#endif
