/*! Tag assignment for MPI world communication
 * This TAGs are using all along within Hypsi code source to send and receive messages
 * Thier value has no consequences with the code
 */

namespace HYPSI {

extern const int TAG_RQ_ZONEFIELDS           = 1;
extern const int TAG_ZONEFIELDS              = 2;
extern const int TAG_PMOVE_ZMOMENTS          = 3;
extern const int TAG_NOPARTICLESINZONE       = 4;
extern const int TAG_COLLECTNV_ZMOMENTS      = 5;
extern const int TAG_COLLECTNV_T_ZMOMENTS    = 6;
extern const int TAG_NNEIGHBOURS_SEND        = 123000;

}
