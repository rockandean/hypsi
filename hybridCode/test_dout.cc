#include <iostream>

using namespace std;

class Doutstream : public ostream {

  ostream& out;

public:
  Doutstream( ostream& o ) : out(o) {;}
  
  template<class T> Doutstream& operator<<( T& s)
  { out << "DOUT: " << s ; return *this; }

};


main()
{

  Doutstream dout( cout );
  
  int i=1234;
  double d=1.23434567;
  
  dout << "hi mondo\n" ;
  dout << "integer: " << i << "\n";
  dout << "double: " << d  << "\n";
  dout << "hi mondo\n" ;


}
