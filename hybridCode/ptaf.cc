#include "hypsi.h"

namespace HYPSI {

using namespace std;

// PTAF_pmove: PARTICLE THREAD ACTION FUNCTOR: PARTICLE MOVE 
// -----------------------------------------------------------------------

PTAF_pmove::PTAF_pmove( const Zone& zoneinfo )
  : PTTHR_ActionFunctor()
{
  _zfields = new ZoneFields( zoneinfo );
  _pzmoms = new PmoveZMoments( zoneinfo );
}

PTAF_pmove::~PTAF_pmove( void )
{
  delete _zfields;
  delete _pzmoms;
}

/*! \brief initialize pointer to PSet for working and timestep

*/
void PTAF_pmove::set_psetp_dt( ParticleSet* psetp, double dt )
{
  _psetp = psetp;
  _dt = dt;
  _dta = _dt * _psetp->q / _psetp->m ;
  _dth = 0.5 * _dta;
}


void PTAF_pmove::action( void )
{
  int np, ipc, ip;
  double* pp;
  double xp, yp, zp, vxp, vyp, vzp;
  double xpn, ypn, zpn, vxpn, vypn, vzpn;
  double vxh, vyh, vzh;
  xyzVector E, B;
  InterpWeights w;

  double f, g;

//  double xpn_mp, ypn_mp, zpn_mp;
  

  try {

  np = _psetp->zoneidx_list[_izone].last - _psetp->zoneidx_list[_izone].first
         + 1;

/*
  cout << "Z" << _hostzone << " PTAF_pmove::action: for ipset: <"
       << _psetp->type()
  << "> zone: " << _izone
  << " np: "  << np
  << " idx: [" 
  << _psetp->zoneidx_list[_izone].first
  << " -> " << _psetp->zoneidx_list[_izone].last << "]\n";
*/


#ifdef DIAG_PTAF_BCF
  cout << "Z" << _hostzone << "PTAF_pmove::action: ";
  _bcfp->print(cout);
#endif

// initialize moment accumulation arrays

// make sure moments object refers to same zone as fields
  _pzmoms->set_zoneinfo( _zfields->zinfo );

// zero moment arrays
  _pzmoms->set_zero();

// loop over particles in this zone    
  for( ipc = _psetp->zoneidx_list[_izone].first ;
       ipc <= _psetp->zoneidx_list[_izone].last ; ++ipc )
  {
    ip = _psetp->plist[ipc];

    pp = _psetp->pdata + ip*6;

    xp = pp[0];
    yp = pp[1];
    zp = pp[2];
    vxp = pp[3];
    vyp = pp[4];
    vzp = pp[5];


/* -------------------------------------------- NOT REQUIRED ANY MORE

now that pfdata used for zone number, there is no need to test
if in domain.

also shouldn't mangle pfdata at this point!

// test to see if particle is in domain
// NOTE: this test is with regard to domain - not zone
// note: this test is via boundary conditions functor

    _psetp->pfdata[ ip ] = _bcfp->test_in_domain( pp );

// if it isn't in domain, then skip it entirely
    
    if( _psetp->pfdata[ ip ] != PF_INDOMAIN )
      continue;

------------------------------------------------------------------- */

// calculate interpolated E and B and interpolation weights

    _zfields->interpolate( pp, E, B, w );

    
// Matthews

/*
    vxh = vxp + _dth * ( E._x + vyp * B._z - vzp * B._y );
    vyh = vyp + _dth * ( E._y + vzp * B._x - vxp * B._z );
    vzh = vzp + _dth * ( E._z + vxp * B._y - vyp * B._x );

    vxpn = vxp + _dta * ( E._x + vyh * B._z - vzh * B._y );
    vypn = vyp + _dta * ( E._y + vzh * B._x - vxh * B._z );
    vzpn = vzp + _dta * ( E._z + vxh * B._y - vyh * B._x );
*/

// From Winske + Leroy tutorial, citing Nielson+Lewis 1976 

    vxh = vxp + _dth * E._x;
    vyh = vyp + _dth * E._y;
    vzh = vzp + _dth * E._z;
    f = 1.0 - _dta * _dth * (  B._x*B._x + B._y*B._y + B._z*B._z );
    g = _dth * (  vxp*B._x + vyp*B._y + vzp*B._z );

    vxpn = f * vxp + _dta * ( E._x + g * B._x + vyh * B._z - vzh * B._y );
    vypn = f * vyp + _dta * ( E._y + g * B._y + vzh * B._x - vxh * B._z );
    vzpn = f * vzp + _dta * ( E._z + g * B._z + vxh * B._y - vyh * B._x );


// back to Matthews algorithm (common with W+L) for position advance

    xpn = xp + _dt * vxpn;
    ypn = yp + _dt * vypn;
    zpn = zp + _dt * vzpn;

// EXPERIMENT estimate of midpoint position
/*
    xpn_mp = xp + 0.25 * _dt * ( vxp + vxpn );
    ypn_mp = yp + 0.25 * _dt * ( vyp + vypn );
    zpn_mp = zp + 0.25 * _dt * ( vyp + vzpn );

// calculate E cell weightings using midpoint particle position

    pp[0] = xpn_mp;
    pp[1] = ypn_mp;
    pp[2] = zpn_mp;

    _zfields->calculate_eweights( pp, w );
*/

/*
    if( ipc - _psetp->zoneidx_list[_izone].first < 5 )
    {
      cout << "old : " << xp << " " << yp << " " << zp << " "
           << vxp << " " << vyp << " " << vzp << endl;
      cout << "E, B: " << E._x << " " << E._z << " " << E._z << " "
           << B._x << " " << B._y << " " << B._z << endl;
      cout << "new : " << xpn << " " << ypn << " " << zpn << " "
           << vxpn << " " << vypn << " " << vzpn << endl;
    
    }
*/

// dna, Uax etc collected using old position E cell weightings

    _pzmoms->dna[ w.ei000 ] += w.ew000;
    _pzmoms->dna[ w.ei100 ] += w.ew100;
    _pzmoms->dna[ w.ei010 ] += w.ew010;
    _pzmoms->dna[ w.ei110 ] += w.ew110;
    _pzmoms->dna[ w.ei001 ] += w.ew001;
    _pzmoms->dna[ w.ei101 ] += w.ew101;
    _pzmoms->dna[ w.ei011 ] += w.ew011;
    _pzmoms->dna[ w.ei111 ] += w.ew111;

    _pzmoms->Uax[ w.ei000 ] += w.ew000 * vxpn;
    _pzmoms->Uax[ w.ei100 ] += w.ew100 * vxpn;
    _pzmoms->Uax[ w.ei010 ] += w.ew010 * vxpn;
    _pzmoms->Uax[ w.ei110 ] += w.ew110 * vxpn;
    _pzmoms->Uax[ w.ei001 ] += w.ew001 * vxpn;
    _pzmoms->Uax[ w.ei101 ] += w.ew101 * vxpn;
    _pzmoms->Uax[ w.ei011 ] += w.ew011 * vxpn;
    _pzmoms->Uax[ w.ei111 ] += w.ew111 * vxpn;

    _pzmoms->Uay[ w.ei000 ] += w.ew000 * vypn;
    _pzmoms->Uay[ w.ei100 ] += w.ew100 * vypn;
    _pzmoms->Uay[ w.ei010 ] += w.ew010 * vypn;
    _pzmoms->Uay[ w.ei110 ] += w.ew110 * vypn;
    _pzmoms->Uay[ w.ei001 ] += w.ew001 * vypn;
    _pzmoms->Uay[ w.ei101 ] += w.ew101 * vypn;
    _pzmoms->Uay[ w.ei011 ] += w.ew011 * vypn;
    _pzmoms->Uay[ w.ei111 ] += w.ew111 * vypn;

    _pzmoms->Uaz[ w.ei000 ] += w.ew000 * vzpn;
    _pzmoms->Uaz[ w.ei100 ] += w.ew100 * vzpn;
    _pzmoms->Uaz[ w.ei010 ] += w.ew010 * vzpn;
    _pzmoms->Uaz[ w.ei110 ] += w.ew110 * vzpn;
    _pzmoms->Uaz[ w.ei001 ] += w.ew001 * vzpn;
    _pzmoms->Uaz[ w.ei101 ] += w.ew101 * vzpn;
    _pzmoms->Uaz[ w.ei011 ] += w.ew011 * vzpn;
    _pzmoms->Uaz[ w.ei111 ] += w.ew111 * vzpn;

// calculate E cell indices and weightings using new position

// assign new position and velocity back to particle data
    pp[0] = xpn;
    pp[1] = ypn;
    pp[2] = zpn;
    pp[3] = vxpn;
    pp[4] = vypn;
    pp[5] = vzpn;

// calculate E cell weightings using new particle position

    _zfields->zinfo.calculate_eweights( pp, w );

// dnb, Ubx etc collected using new position E cell weightings

    _pzmoms->dnb[ w.ei000 ] += w.ew000;
    _pzmoms->dnb[ w.ei100 ] += w.ew100;
    _pzmoms->dnb[ w.ei010 ] += w.ew010;
    _pzmoms->dnb[ w.ei110 ] += w.ew110;
    _pzmoms->dnb[ w.ei001 ] += w.ew001;
    _pzmoms->dnb[ w.ei101 ] += w.ew101;
    _pzmoms->dnb[ w.ei011 ] += w.ew011;
    _pzmoms->dnb[ w.ei111 ] += w.ew111;

    _pzmoms->Ubx[ w.ei000 ] += w.ew000 * vxpn;
    _pzmoms->Ubx[ w.ei100 ] += w.ew100 * vxpn;
    _pzmoms->Ubx[ w.ei010 ] += w.ew010 * vxpn;
    _pzmoms->Ubx[ w.ei110 ] += w.ew110 * vxpn;
    _pzmoms->Ubx[ w.ei001 ] += w.ew001 * vxpn;
    _pzmoms->Ubx[ w.ei101 ] += w.ew101 * vxpn;
    _pzmoms->Ubx[ w.ei011 ] += w.ew011 * vxpn;
    _pzmoms->Ubx[ w.ei111 ] += w.ew111 * vxpn;

    _pzmoms->Uby[ w.ei000 ] += w.ew000 * vypn;
    _pzmoms->Uby[ w.ei100 ] += w.ew100 * vypn;
    _pzmoms->Uby[ w.ei010 ] += w.ew010 * vypn;
    _pzmoms->Uby[ w.ei110 ] += w.ew110 * vypn;
    _pzmoms->Uby[ w.ei001 ] += w.ew001 * vypn;
    _pzmoms->Uby[ w.ei101 ] += w.ew101 * vypn;
    _pzmoms->Uby[ w.ei011 ] += w.ew011 * vypn;
    _pzmoms->Uby[ w.ei111 ] += w.ew111 * vypn;

    _pzmoms->Ubz[ w.ei000 ] += w.ew000 * vzpn;
    _pzmoms->Ubz[ w.ei100 ] += w.ew100 * vzpn;
    _pzmoms->Ubz[ w.ei010 ] += w.ew010 * vzpn;
    _pzmoms->Ubz[ w.ei110 ] += w.ew110 * vzpn;
    _pzmoms->Ubz[ w.ei001 ] += w.ew001 * vzpn;
    _pzmoms->Ubz[ w.ei101 ] += w.ew101 * vzpn;
    _pzmoms->Ubz[ w.ei011 ] += w.ew011 * vzpn;
    _pzmoms->Ubz[ w.ei111 ] += w.ew111 * vzpn;


// apply boundary conditions here
// note: applying boundary conditions can change the zone a particle is in,
//   so is only done after the moment collection.

// currently this means that particles at zone boundaries must only
// move less than 0.5 dx out of the zone

    _bcfp->apply_bc( pp );


  }  // end of loop over particles

// moment factors  

  } catch ( HypsiException& e )
  { 
    cout << "HYPSI Exception in particle thread\n";
    e.diag_cout();
    cout << endl;
    e.push_err_msg("HYPSI Exception in PTAF_pmove::action" );
    throw e;
  }

}


}  // end namespace HYPSI
