namespace HYPSI {


/*! \brief sorting particles over nodes to maximize localization
*/

class MultiNodeSort {

  Hypsim* _hypsim;
  ParticleSet* _psetp;
  int* _pzonedist;
  int* _totalzonedist;
  int* _idealpzonedist;
  
  int _n_zones, _hostzone; // propagated up from hypsim sim_params

public:

  MultiNodeSort( Hypsim* hypsim, int npset )
  {
    _hypsim = hypsim;
    _psetp =  hypsim->psets[ npset ];
    _n_zones = hypsim->sim_params.n_zones;
    _host_zone = hypsim->sim_params.hostzone;
    _pzonedist = new int[ _n_zones ];
    _totalzonedist = new int[ _n_zones ];
    _idealpzonedist = new int[ _n_zones ];
  }

  void accumulate_totalzonedist( void );






}; // end class MultiNodeSort

} // end namespace HYPSI

int* MultiNodeSort::accumulate_totalzonedist( void )
{

// sort the particles by zone:

  _psetp->sort_byzone( _hypsim->domain_info, _hypsim->zones_info );

  for( iz=0; iz<_n_zones; iz++ )
    _pzonedist[ iz ] = _psetp->zoneidx_list[ iz ].num;





} // end MultiNodeSort::accumulate_totalzonedist



