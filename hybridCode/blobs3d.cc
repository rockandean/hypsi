#include "blobs3d.h"
#include "hypsi.h"

namespace HYPSI {

//! start by reading uniform plasma conditions

void Blobs3d::read_input_data( ConfigData& config_data)
{
    try{

    uniform_initial_conditions.read_input_data( config_data );

    } catch (HypsiException& e)
    {
      e.push_err_msg("In Blobs3d::read_input_data while reading UNIFORM PLASMA input data");
      throw e;
    }

    // input parameters
    vector<double> vxyz_blob;
    vector<double> Bxyz_blob;

    // get parameters from file
    try{
        config_data.get_data("blob_initial_condition/vxyz_blob", vxyz_blob);
        config_data.get_data("blob_initial_condition/Bxyz_blob", Bxyz_blob);
    } catch (KVF::kvfException& e)
    {
        e.diag_out();
        throw HypsiException("Failed to get Blob Initialize from data file: <"
                             + config_data.filename()+ ">",
                             "blobs3d::read_input_data config_data.get_data");
    }
    // any error handling on input variables


} // end read_input_data

void Blobs3d::initialize_fields_and_particles(Hypsim& hypsim)
{
    // initialize UNIFORM PLASMA  initial conditions

    hypsim.initialize_uniform_B( uniform_initial_conditions._Bvec);

    hypsim.initialize_uniform_resis( hypsim.sim_params.uniform_resis);

    hypsim.initialize_pe( hypsim.sim_params.initial_Te);

    // cell random
    uniform_initial_conditions.initialize_particle_sets(Hypsim& hypsim);
    
} // end initialize fields and particles


} // end namespace HIPSY
