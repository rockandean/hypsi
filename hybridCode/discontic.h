#ifndef _HYPSI_DISCONTIC_H_
#define _HYPSI_DISCONTIC_H_

#include <complex>
#include <string>
#include <fstream>
#include <iostream>
#include <sstream>

namespace HYPSI {

typedef std::complex<double> complex;

class DiscontIC {
public:

  double _dd_width; // width of the DD
  int _dd_dir; // direction of the DD normal, 0 for x, 1 for y, 2 for z
  xyzVector _Vshift_disc; // V field between the inferfaces
  xyzVector _Bvec_disc; // B field between the interfaces

  UniformIC uniform_initial_conditions;

  DiscontIC( void ) {;}
  
  void read_input_data( ConfigData& config_data );

  void initialize_fields_and_particles( Hypsim& hypsim );

  //void initialize_particle_sets( Hypsim& hypsim );

  void output_params( ostream& ostrm, string s="" ) const
  { 
    ostrm<<s<<"DiscontIC: Width = " << _dd_width  << " Direction: " << _dd_dir << "\n";
    ostrm<<s<<"DiscontIC: Bvec_disc = " << _Bvec_disc << " Vshift_disc = " << _Vshift_disc << "\n";
  }


};


}   // end namespace HYPSI

#endif
