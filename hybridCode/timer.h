

#ifndef _HYPSI_TIMER_H_
#define _HYPSI_TIMER_H_

#include <time.h>
#include <sys/times.h>
#include <string>
#include <iostream>

namespace HYPSI {

class Timer{
  bool running;
  time_t unixtime_start;
  time_t unixtime_end;
  struct timespec timespec_start;
  struct timespec timespec_end;
  struct tms tbuf_start;
  struct tms tbuf_stop;

public:
  Timer(void) : running(false), unixtime_start(0), unixtime_end(0) { }
  
  void start( void )
    { if( !running )
      { clock_gettime( CLOCK_REALTIME, &timespec_start );
        time( &unixtime_start ); times( &tbuf_start ); running=true;} }
  void stop( void )
    { if( running )
      { clock_gettime( CLOCK_REALTIME, &timespec_end );
        time( &unixtime_end ); times( &tbuf_stop ); running=false;} }
  void restart( void )
    { if( !running ){ running=true;} }
  void print( std::ostream& ostrm, std::string s="" ) const
    {
      double clktck = sysconf(_SC_CLK_TCK);
      ostrm << s << "U: "
       << (tbuf_stop.tms_utime - tbuf_start.tms_utime) / clktck 
       << " S: " << (tbuf_stop.tms_stime - tbuf_start.tms_stime) / clktck
       << " W: " << (timespec_end.tv_sec+1.0e-9*timespec_end.tv_nsec )
                  - (timespec_start.tv_sec+1.0e-9*timespec_start.tv_nsec )
       << "\n";
    }
  void test( std::ostream& ostrm )
    {
      ostrm << "Timer test .. will sleep for 5seconds\n";
      start();
      sleep(5);
      stop();
      print( ostrm );
    }
};


}  // end namespace HYPSI



#endif // _HYPSI_TIMER_H_
